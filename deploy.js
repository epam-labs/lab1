#!/usr/bin/env node

const ncp = require('ncp').ncp;
const path = require('path');
const fs = require('fs');
const glob = require('glob');
const {exec} = require('child_process');
const {replace, flatten} = require('lodash');

const tomcatFolder = process.env.CATALINA_HOME;

const appName = "VT";
const libsPath = "libs";

const sourceDirs = [
    "src/main/java",
    "src/model/java"
];

const excludedDirs = [
    "src/main/java/com/gitlab/andrewkuryan/vt1/view",
    "src/main/java/com/gitlab/andrewkuryan/vt1/viewmodel",
    "src/main/java/com/gitlab/andrewkuryan/vt1/dataprovider/csv"
];

let libsStr;

getFilesList(`${libsPath}/**/*.jar`)
    .then((files) => {
        libsStr = files.join(';');
        return copyDirContent("./web", "./deploy")
    })
    .then(() => {
        return Promise.all(
            sourceDirs.map((dir) => getFilesList(
                `${dir}/**/*.java`,
                excludedDirs.map((d) => `${d}/**/*.java`))
            )
        );
    })
    .then((files) => {
        return commandLine(`javac -cp ${libsStr} ${flatten(files).join(" ")} -d deploy/WEB-INF/classes`);
    })
    .then(() => {
        if (tomcatFolder !== undefined) {
            return copyDirContent("./deploy", path.resolve(tomcatFolder, `webapps/${appName}`));
        } else {
            return Promise.resolve();
        }
    })
    .then(() => {
        return commandLine(`jar cfv ${appName}.war -C deploy/ .`);
    })
    .then(() => {
        if (tomcatFolder !== undefined) {
            return copyFile(`./${appName}.war`, path.resolve(tomcatFolder, `webapps/${appName}.war`));
        } else {
            return Promise.resolve();
        }
    })
    .catch((exc) => {
        console.log(exc);
        console.error("Can't deploy web-app");
    });

function getFilesList(pattern, exclude) {
    return new Promise((res, rej) => {
        glob(pattern, {ignore: exclude}, (err, files) => {
            if (err) {
                rej(err);
            } else {
                res(files);
            }
        })
    });
}

function copyDirContent(from, to) {
    return new Promise((res, rej) => {
        ncp(from, to, (err) => {
            if (err) {
                rej(err);
            } else {
                res();
            }
        })
    });
}

function copyFile(from, to) {
    return new Promise((res, rej) => {
        fs.copyFile(from, to, (err) => {
            if (err) {
                rej(err);
            } else {
                res();
            }
        });
    })
}

function commandLine(args) {
    return new Promise((res, rej) => {
        exec(args, (err) => {
            if (err) {
                rej(err);
            } else {
                res();
            }
        })
    });
}