<div class="navbar-fixed">
    <nav class="nav-extended teal lighten-2">
        <div class="nav-wrapper">
            <a href="#" class="brand-logo">Hotel Service</a>
        </div>
        <div class="nav-content">
            <ul class="tabs tabs-transparent">
                <li class="tab"><a href="roomTypes">Room Types</a></li>
                <li class="tab"><a href="availableRoomTypes">Available Room Types</a></li>
                <li class="tab"><a href="requests">Requests</a></li>
            </ul>
        </div>
    </nav>
</div>