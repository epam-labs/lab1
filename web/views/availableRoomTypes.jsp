<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="styles/materialize.min.css" media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="styles/list.css"/>
    <script type="text/javascript">
        console.log("${roomTypes}");
    </script>
    <title>Available Rooms</title>
</head>
<body>
<%@include file="header.jsp" %>
<div class="content">
    <div class="list-header">
        <h3>Available Rooms</h3>
        <form action="/VT/availableRoomTypes" method="get" class="records-per-page-form">
            <div class="records-per-page-form-content">
                <div>
                    <label for="recordsPerPage">Records per Page:</label>
                    <input id="recordsPerPage" name="recordsPerPage" type="number" min="1" max="10"
                           value="${recordsPerPage}"/>
                </div>
                <button class="waves-effect waves-light btn" type="submit">Apply</button>
            </div>
        </form>
    </div>
    <table class="list striped">
        <thead>
        <tr>
            <th>Image</th>
            <th>Type Name</th>
            <th>Free Places</th>
            <th>Places</th>
            <th>Cost</th>
            <th>Area</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="roomType" items="${roomTypes}">
            <tr>
                <td><img class="responsive-img"
                         src="${roomType.getSmallImage().orElse(null) == null ? "images/noRoomType.png" : roomType.getSmallImage().get()}">
                </td>
                <td>${roomType.getTypeName()}</td>
                <td>${roomType.getAvailableRooms()}</td>
                <td>${roomType.getNumOfPlaces()}</td>
                <td>${roomType.getCost()}</td>
                <td>${roomType.getArea()}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <div class="table-info">
        Show ${roomTypes.size()} of ${totalCount}
    </div>
    <ul class="pagination">
        <c:if test="${page == 1}">
            <li class="disabled"><a href="#"><i class="material-icons">chevron_left</i></a></li>
        </c:if>
        <c:if test="${page > 1}">
            <li class="waves"><a href="/VT/availableRoomTypes?page=${page - 1}"><i
                    class="material-icons">chevron_left</i></a></li>
        </c:if>

        <c:forEach var="i" begin="1" end="${totalPages}">
            <c:if test="${i == page}">
                <li class="active teal lighten-2"><a href="/VT/availableRoomTypes?page=${i}">${i}</a></li>
            </c:if>
            <c:if test="${i != page}">
                <li class="waves-effect"><a href="/VT/availableRoomTypes?page=${i}">${i}</a></li>
            </c:if>
        </c:forEach>

        <c:if test="${page == totalPages}">
            <li class="disabled"><a href="#"><i class="material-icons">chevron_right</i></a></li>
        </c:if>
        <c:if test="${page < totalPages}">
            <li class="waves"><a href="/VT/availableRoomTypes?page=${page + 1}"><i
                    class="material-icons">chevron_right</i></a>
            </li>
        </c:if>
    </ul>
</div>
</body>
</html>