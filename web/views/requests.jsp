<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="styles/materialize.min.css" media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="styles/list.css"/>
    <script type="text/javascript">
        console.log("${requests}");
    </script>
    <title>Requests</title>
</head>
<body>
<%@include file="header.jsp" %>
<div class="content">
    <div class="list-header">
        <h3>Requests</h3>
        <form action="/VT/requests" method="get" class="records-per-page-form">
            <div class="records-per-page-form-content">
                <div>
                    <label for="recordsPerPage">Records per Page:</label>
                    <input id="recordsPerPage" name="recordsPerPage" type="number" min="1" max="10"
                           value="${recordsPerPage}"/>
                </div>
                <button class="waves-effect waves-light btn" type="submit">Apply</button>
            </div>
        </form>
    </div>
    <table class="list striped">
        <thead>
        <tr>
            <th>Room Type</th>
            <th>Arrival Date</th>
            <th>Departure Date</th>
            <th>Persons</th>
            <th>Customer</th>
            <th>Status</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="request" items="${requests}">
            <tr>
                <td>${request.getRoomTypeName()}</td>
                <td>${request.getArrivalDate()}</td>
                <td>${request.getDepartureDate()}</td>
                <td>${request.getNumberOfPersons()}</td>
                <td>${request.getCustomer()}</td>
                <td>${request.getStatus()}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <div class="table-info">
        Show ${requests.size()} of ${totalCount}
    </div>
    <ul class="pagination">
        <c:if test="${page == 1}">
            <li class="disabled"><a href="#"><i class="material-icons">chevron_left</i></a></li>
        </c:if>
        <c:if test="${page > 1}">
            <li class="waves"><a href="/VT/requests?page=${page - 1}"><i class="material-icons">chevron_left</i></a>
            </li>
        </c:if>

        <c:forEach var="i" begin="1" end="${totalPages}">
            <c:if test="${i == page}">
                <li class="active teal lighten-2"><a href="/VT/requests?page=${i}">${i}</a></li>
            </c:if>
            <c:if test="${i != page}">
                <li class="waves-effect"><a href="/VT/requests?page=${i}">${i}</a></li>
            </c:if>
        </c:forEach>

        <c:if test="${page == totalPages}">
            <li class="disabled"><a href="#"><i class="material-icons">chevron_right</i></a></li>
        </c:if>
        <c:if test="${page < totalPages}">
            <li class="waves"><a href="/VT/requests?page=${page + 1}"><i class="material-icons">chevron_right</i></a>
            </li>
        </c:if>
    </ul>
</div>
</body>
</html>