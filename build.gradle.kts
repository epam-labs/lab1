buildscript {
    repositories {
        mavenCentral()
    }
    dependencies {
        classpath("org.junit.platform:junit-platform-gradle-plugin:1.1.0")
        classpath("org.junit.platform:junit-platform-launcher:1.1.0")
    }
}

plugins {
    java
    application
}

sourceSets {
    create("model") {
        java.srcDir(file("src/model/java"))
        resources.srcDir(file("src/model/resources"))
        compileClasspath += configurations["runtimeClasspath"]
    }
    create("repository") {
        java.srcDir(file("src/repository/java"))
        resources.srcDir(file("src/repository/resources"))
        compileClasspath += sourceSets["model"].output
        compileClasspath += configurations["runtimeClasspath"]
    }
    getByName("main") {
        compileClasspath += sourceSets["model"].output
        runtimeClasspath += output + compileClasspath
    }
    getByName("test") {
        compileClasspath +=
                sourceSets["model"].output +
                sourceSets["repository"].output
        runtimeClasspath += output + compileClasspath
    }
    create("integTest") {
        java.srcDir(file("src/integTest/java"))
        resources.srcDir(file("src/integTest/resources"))
        compileClasspath += sourceSets["main"].output + sourceSets["model"].output
        compileClasspath += configurations["testCompileClasspath"]
        compileClasspath += sourceSets["test"].output
        runtimeClasspath += output + compileClasspath
    }
}

tasks.register<JavaExec>("integTest") {
    description = "Runs the integration test"
    group = "verification"
    classpath = sourceSets["integTest"].runtimeClasspath
    main = "com.gitlab.andrewkuryan.vt1.IntegrationTest"
}

tasks.register<JavaExec>("transfer") {
    description = "Transfer xml data to DB"
    group = "application"
    classpath = sourceSets["repository"].runtimeClasspath + 
            sourceSets["model"].runtimeClasspath +
            configurations["runtimeClasspath"]
    main = "com.gitlab.andrewkuryan.vt1.Transfer"
}

tasks.register<Exec>("npmInstall") {
    commandLine("npm install")
}

tasks.named("init") {
    dependsOn("npmInstall")
}

tasks.register<Exec>("compileStylus") {
    commandLine("./node_modules/.bin/stylus", "src/main/resources/styles/", "-o", "build/resources/main/styles/")
}

tasks.named("processResources") {
    dependsOn("compileStylus")
}

val fxModules = arrayListOf("base", "graphics", "controls", "fxml")

tasks.withType<JavaCompile>().all {
    doFirst {
        options.compilerArgs = mutableListOf(
            "--module-path", classpath.asPath,
            "--add-modules", fxModules.joinToString(separator = ",") { "javafx.$it" }
        )
    }
}

application {
    tasks.withType<JavaExec>().all {
        doFirst {
            jvmArgs = mutableListOf(
                "--module-path", classpath.asPath,
                "--add-modules", fxModules.joinToString(separator = ",") { "javafx.$it" })
        }
    }
    mainClassName = "com.gitlab.andrewkuryan.vt1.App"
}

val platform = with(org.gradle.internal.os.OperatingSystem.current()) {
    when {
        isWindows -> "win"
        isLinux -> "linux"
        isMacOsX -> "mac"
        else -> "unknown"
    }
}

repositories {
    mavenCentral()
    jcenter()
}

val junitVersion = "5.1.0"
val logVersion = "2.12.0"

dependencies {
    for (module in fxModules) {
        implementation("org.openjfx:javafx-$module:11:$platform")
    }
    implementation("javax.xml.bind:jaxb-api:2.4.0-b180830.0359")
    implementation("com.sun.xml.bind:jaxb-osgi:2.4.0-b180830.0438")
    implementation("io.github.cdimascio:java-dotenv:5.1.1")

    implementation("org.apache.logging.log4j:log4j-api:$logVersion")
    implementation("org.apache.logging.log4j:log4j-core:$logVersion")

    implementation("org.mariadb.jdbc:mariadb-java-client:2.5.2")
    
    testImplementation("org.mockito:mockito-core:2.+")
    testImplementation("org.junit.jupiter:junit-jupiter-api:$junitVersion")
    testRuntime("org.junit.jupiter:junit-jupiter-engine:$junitVersion")
}


tasks.named<Test>("test") {
    useJUnitPlatform()
}
