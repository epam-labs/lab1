package com.gitlab.andrewkuryan.vt1;

import com.gitlab.andrewkuryan.vt1.dataprovider.csv.AppDataProvider;
import com.gitlab.andrewkuryan.vt1.entity.user.User;
import com.gitlab.andrewkuryan.vt1.service.AppService;
import com.gitlab.andrewkuryan.vt1.service.AppViewModel;
import com.gitlab.andrewkuryan.vt1.service.HotelAppService;
import com.gitlab.andrewkuryan.vt1.service.auth.port.LoginViewModel;
import com.gitlab.andrewkuryan.vt1.service.auth.port.RegisterViewModel;
import com.gitlab.andrewkuryan.vt1.service.request.port.RequestsViewModel;
import com.gitlab.andrewkuryan.vt1.service.roomtype.port.AvailableRoomsViewModel;
import com.gitlab.andrewkuryan.vt1.service.roomtype.port.RoomTypeViewModel;
import com.gitlab.andrewkuryan.vt1.view.account.AccToolbarView;
import com.gitlab.andrewkuryan.vt1.view.account.AdminEditView;
import com.gitlab.andrewkuryan.vt1.view.account.ClientEditView;
import com.gitlab.andrewkuryan.vt1.view.account.menu.AdminContextMenu;
import com.gitlab.andrewkuryan.vt1.view.account.menu.ClientContextMenu;
import com.gitlab.andrewkuryan.vt1.view.account.menu.GuestContextMenu;
import com.gitlab.andrewkuryan.vt1.view.auth.AuthView;
import com.gitlab.andrewkuryan.vt1.view.auth.LoginView;
import com.gitlab.andrewkuryan.vt1.view.auth.RegisterView;
import com.gitlab.andrewkuryan.vt1.view.main.LeftNavigationView;
import com.gitlab.andrewkuryan.vt1.view.main.MainView;
import com.gitlab.andrewkuryan.vt1.view.requests.CreateRequestView;
import com.gitlab.andrewkuryan.vt1.view.requests.RequestsView;
import com.gitlab.andrewkuryan.vt1.view.requests.menu.AdminRequestsMenu;
import com.gitlab.andrewkuryan.vt1.view.requests.menu.ClientRequestsMenu;
import com.gitlab.andrewkuryan.vt1.view.roomtype.AvailableRoomsView;
import com.gitlab.andrewkuryan.vt1.view.roomtype.RoomTypeView;
import com.gitlab.andrewkuryan.vt1.view.utils.FXAlertManager;
import com.gitlab.andrewkuryan.vt1.view.utils.FXConfigurationManager;
import com.gitlab.andrewkuryan.vt1.view.utils.FXNavigationManager;
import com.gitlab.andrewkuryan.vt1.viewmodel.FXAppViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.FXViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.account.FXAdminViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.account.FXClientViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.account.FXGuestViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.auth.FXAuthViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.auth.FXLoginViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.auth.FXRegisterViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.auth.port.AuthViewState;
import com.gitlab.andrewkuryan.vt1.viewmodel.auth.port.LoginViewState;
import com.gitlab.andrewkuryan.vt1.viewmodel.auth.port.RegisterViewState;
import com.gitlab.andrewkuryan.vt1.viewmodel.availableroomtype.FXAvailableRoomsViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.availableroomtype.port.AvailableRoomsViewState;
import com.gitlab.andrewkuryan.vt1.viewmodel.main.FXLeftNavigationViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.main.port.LeftNavigationViewState;
import com.gitlab.andrewkuryan.vt1.viewmodel.port.AlertManager;
import com.gitlab.andrewkuryan.vt1.viewmodel.port.ConfigurationManager;
import com.gitlab.andrewkuryan.vt1.viewmodel.port.LateinitView;
import com.gitlab.andrewkuryan.vt1.viewmodel.port.NavigationManager;
import com.gitlab.andrewkuryan.vt1.viewmodel.requests.FXAdminRequestsViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.requests.FXClientRequestsViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.requests.port.AdminRequestsViewState;
import com.gitlab.andrewkuryan.vt1.viewmodel.requests.port.ClientRequestsViewState;
import com.gitlab.andrewkuryan.vt1.viewmodel.roomtype.FXRoomTypeViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.roomtype.port.RoomTypeViewState;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class IntegrationTest extends Application {

	private static final int winWidth = 1000;
	private static final int winHeight = 600;

	private Pane root;
	private NavigationManager navigationManager;
	private AlertManager alertManager;
	private ConfigurationManager configurationManager;

	private AppViewModel appViewModel;
	private AppService appService;

	@Override
	public void init() throws Exception {
		super.init();
		root = new Pane();
		root.getStylesheets().add("/styles/common.css");
		navigationManager = new FXNavigationManager(root);
		alertManager = new FXAlertManager(root);
		configurationManager = new FXConfigurationManager(new ArrayList<>());

		appViewModel = new FXAppViewModel(
				navigationManager,
				alertManager,
				configurationManager
		);

		appService = new HotelAppService(
				new AppDataProvider(Paths.get("teststorage")),
				appViewModel
		);

		initView();
	}

	private void initView() {
		LoginViewState loginViewState =
				new FXLoginViewModel((FXViewModel) appViewModel, appService.getLoginModel());
		appService.getLoginModel().addViewModel((LoginViewModel) loginViewState);

		RegisterViewState registerViewState =
				new FXRegisterViewModel((FXViewModel) appViewModel, appService.getRegisterModel());
		appService.getRegisterModel().addViewModel((RegisterViewModel) registerViewState);

		LoginView loginView = LoginView.newInstance(loginViewState);
		RegisterView registerView = RegisterView.newInstance(registerViewState);

		AuthViewState authViewState = new FXAuthViewModel();
		AuthView authView = AuthView.newInstance(authViewState, loginView, registerView);
		navigationManager.addRoute("auth", authView);



		FXGuestViewModel guestViewModel = new FXGuestViewModel((FXViewModel) appViewModel);
		appService.getAccountModel().addViewModel(guestViewModel);
		LateinitView<AccToolbarView> lateGuestView = () ->
				AccToolbarView.newInstance(guestViewModel, new GuestContextMenu(guestViewModel));

		FXAdminViewModel adminViewModel = new FXAdminViewModel((FXViewModel) appViewModel, appService.getAccountModel());
		appService.getAccountModel().addViewModel(adminViewModel);
		LateinitView<AccToolbarView> lateAdminView = () ->
				AccToolbarView.newInstance(adminViewModel, new AdminContextMenu(adminViewModel));

		FXClientViewModel clientViewModel = new FXClientViewModel((FXViewModel) appViewModel, appService.getAccountModel());
		appService.getAccountModel().addViewModel(clientViewModel);
		LateinitView<AccToolbarView> lateClientView = () ->
				AccToolbarView.newInstance(clientViewModel, new ClientContextMenu(clientViewModel));



		RoomTypeViewState roomTypeViewState = new FXRoomTypeViewModel((FXViewModel) appViewModel, appService.getRoomTypeModel());
		appService.getRoomTypeModel().addViewModel((RoomTypeViewModel) roomTypeViewState);
		RoomTypeView roomTypeView = RoomTypeView.newInstance(roomTypeViewState);
		navigationManager.addRoute("roomtypes", roomTypeView);

		AvailableRoomsViewState availableRoomsViewState = new FXAvailableRoomsViewModel((FXViewModel) appViewModel, appService.getAvailableRoomTypesModel());
		appService.getAvailableRoomTypesModel().addViewModel((AvailableRoomsViewModel) availableRoomsViewState);
		AvailableRoomsView availableRoomsView = AvailableRoomsView.newInstance(availableRoomsViewState);
		navigationManager.addRoute("availableroomtypes", availableRoomsView);



		ClientRequestsViewState clientRequestsViewState = new FXClientRequestsViewModel((FXViewModel) appViewModel, appService.getClientRequestsModel());
		appService.getClientRequestsModel().addViewModel((RequestsViewModel) clientRequestsViewState);
		LateinitView<RequestsView> lateClientRequestsView = () ->
				RequestsView.newInstance(clientRequestsViewState, new ClientRequestsMenu(clientRequestsViewState));
		navigationManager.addLateinitRoute("clientrequests", lateClientRequestsView);

		AdminRequestsViewState adminRequestsViewState = new FXAdminRequestsViewModel((FXViewModel) appViewModel, appService.getAdminRequestsModel());
		appService.getAdminRequestsModel().addViewModel((RequestsViewModel) adminRequestsViewState);
		LateinitView<RequestsView> lateAdminRequestsView = () ->
				RequestsView.newInstance(adminRequestsViewState, new AdminRequestsMenu(adminRequestsViewState));
		navigationManager.addLateinitRoute("adminrequests", lateAdminRequestsView);

		LateinitView<CreateRequestView> lateCreateRequestView = () ->
				CreateRequestView.newInstance(clientRequestsViewState);
		navigationManager.addLateinitRoute("createrequest", lateCreateRequestView);



		LateinitView<ClientEditView> lateClientEditView = () ->
				ClientEditView.newInstance(clientViewModel);
		navigationManager.addLateinitRoute("clientprofile", lateClientEditView);

		LateinitView<AdminEditView> lateAdminEditView = () ->
				AdminEditView.newInstance(adminViewModel);
		navigationManager.addLateinitRoute("adminprofile", lateAdminEditView);



		LeftNavigationViewState leftNavigationViewState = new FXLeftNavigationViewModel((FXViewModel) appViewModel);
		LeftNavigationView leftNavigationView = new LeftNavigationView(leftNavigationViewState, "guest", Map.of(
				"guest", new LinkedHashMap<>() {{
						put("All Room Types", "roomtypes");
						put("Available Room Types", "availableroomtypes");
					}},
				"client", new LinkedHashMap<>() {{
						put("All Room Types", "roomtypes");
						put("Available Room Types", "availableroomtypes");
						put("Requests", "clientrequests");
					}},
				"admin", new LinkedHashMap<>() {{
						put("All Room Types", "roomtypes");
						put("Available Room Types", "availableroomtypes");
						put("Requests", "adminrequests");
					}}
		));
		configurationManager.addConfigurableView(leftNavigationView);

		MainView mainView = MainView.newInstance(
				lateGuestView.init(),
				leftNavigationView,
				Map.of(
						"guest", lateGuestView,
						"admin", lateAdminView,
						"client", lateClientView
				)
		);

		navigationManager.addRoute("main", mainView);
		configurationManager.addConfigurableView(mainView);
	}

	@Override
	public void start(Stage primaryStage) {
		Scene scene = new Scene(root, winWidth, winHeight);

		primaryStage.setScene(scene);
		primaryStage.setTitle("Hotel Service Dev");
		primaryStage.show();

		appService.getAccountModel().setAccount(User.GUEST);
		navigationManager.navigateTo("roomtypes");
	}

	public static void main(String[] args) {
		launch(args);
	}
}
