package com.gitlab.andrewkuryan.vt1;

import com.gitlab.andrewkuryan.vt1.entity.request.Request;
import com.gitlab.andrewkuryan.vt1.entity.roomtype.RoomType;
import com.gitlab.andrewkuryan.vt1.entity.user.User;

public interface Repository {

    void saveUsers(Iterable<User> users);
    void saveRoomTypes(Iterable<RoomType> roomTypes);
    void saveRequests(Iterable<Request> requests);

    Iterable<User> getUsers();
    Iterable<RoomType> getRoomTypes();
    Iterable<Request> getRequests();

}
