package com.gitlab.andrewkuryan.vt1;

import com.gitlab.andrewkuryan.vt1.entity.user.Administrator;

public interface AdministratorDAO extends DAO<Administrator> {}
