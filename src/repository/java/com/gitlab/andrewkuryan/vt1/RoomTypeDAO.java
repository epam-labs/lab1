package com.gitlab.andrewkuryan.vt1;

import com.gitlab.andrewkuryan.vt1.entity.roomtype.RoomType;

public interface RoomTypeDAO extends DAO<RoomType> {
}
