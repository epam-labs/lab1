package com.gitlab.andrewkuryan.vt1;

import com.gitlab.andrewkuryan.vt1.entity.user.User;

public interface UserDAO extends DAO<User> {
}
