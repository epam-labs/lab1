package com.gitlab.andrewkuryan.vt1;

public interface DAO<T> {

    void save(Iterable<T> objects);
    Iterable<T> read();

}
