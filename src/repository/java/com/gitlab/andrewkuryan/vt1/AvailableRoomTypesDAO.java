package com.gitlab.andrewkuryan.vt1;

import com.gitlab.andrewkuryan.vt1.entity.roomtype.AvailableRoomTypes;

public interface AvailableRoomTypesDAO extends DAO<AvailableRoomTypes> {}
