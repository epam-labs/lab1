package com.gitlab.andrewkuryan.vt1;

import com.gitlab.andrewkuryan.vt1.entity.request.Request;

public interface RequestDAO extends DAO<Request> {}
