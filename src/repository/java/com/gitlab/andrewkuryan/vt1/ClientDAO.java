package com.gitlab.andrewkuryan.vt1;

import com.gitlab.andrewkuryan.vt1.entity.user.Client;

public interface ClientDAO extends DAO<Client> {
}
