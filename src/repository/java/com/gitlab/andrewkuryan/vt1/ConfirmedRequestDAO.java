package com.gitlab.andrewkuryan.vt1;

import com.gitlab.andrewkuryan.vt1.entity.request.ConfirmedRequest;

public interface ConfirmedRequestDAO extends DAO<ConfirmedRequest> {
}
