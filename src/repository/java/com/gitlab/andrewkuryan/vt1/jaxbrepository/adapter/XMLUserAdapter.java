package com.gitlab.andrewkuryan.vt1.jaxbrepository.adapter;

import com.gitlab.andrewkuryan.vt1.entity.user.User;
import com.gitlab.andrewkuryan.vt1.jaxbrepository.adapter.entity.XMLUser;

public class XMLUserAdapter {

	public static XMLUser toXml(User user) {
		XMLUser ans = new XMLUser();
		ans.setLogin(user.getLogin());
		ans.setToken(user.getToken());
		return ans;
	}

	public static User toUser(XMLUser user) {
		return new User(user.getLogin(), user.getToken());
	}

}
