package com.gitlab.andrewkuryan.vt1.jaxbrepository.adapter;

import com.gitlab.andrewkuryan.vt1.entity.request.Request;
import com.gitlab.andrewkuryan.vt1.jaxbrepository.adapter.entity.XMLRequest;

import java.net.MalformedURLException;

public class XMLRequestAdapter {

	public static Request toRequest(XMLRequest xmlRequest) throws MalformedURLException {
		return new Request(
				XMLRoomTypeAdapter.toRoomType(xmlRequest.getRoomType()),
				xmlRequest.arrivalDate(),
				xmlRequest.departureDate(),
				xmlRequest.getNumberOfPersons(),
				XMLClientAdapter.toClient(xmlRequest.getCustomer())
		);
	}

	public static XMLRequest toXmlRequest(Request request) {
		XMLRequest ans = new XMLRequest();
		ans.setRoomType(XMLRoomTypeAdapter.toXmlRoomType(request.getRoomType()));
		ans.setArrivalDateAsDate(request.getArrivalDate());
		ans.setDepartureDateAsDate(request.getDepartureDate());
		ans.setCustomer(XMLClientAdapter.toXml(request.getCustomer()));
		ans.setNumberOfPersons(request.getNumberOfPersons());
		return ans;
	}

}
