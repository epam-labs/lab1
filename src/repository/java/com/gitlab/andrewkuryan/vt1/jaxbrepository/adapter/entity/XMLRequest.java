package com.gitlab.andrewkuryan.vt1.jaxbrepository.adapter.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@XmlType(propOrder = {"roomType", "arrivalDate", "departureDate", "numberOfPersons", "customer"})
public class XMLRequest {

	private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:sszzz");

	private XMLRoomType roomType;
	private Date arrivalDate;
	private Date departureDate;
	private int numberOfPersons;
	private XMLClient customer;

	@XmlElement(name = "roomType")
	public XMLRoomType getRoomType() {
		return roomType;
	}

	public void setRoomType(XMLRoomType roomType) {
		this.roomType = roomType;
	}

	@XmlElement(name = "arrivalDate")
	public String getArrivalDate() {
		return dateFormat.format(arrivalDate);
	}

	public Date arrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(String arrivalDate) {
		try {
			this.arrivalDate = dateFormat.parse(arrivalDate);
		} catch (ParseException exc) {
			exc.printStackTrace();
		}
	}

	public void setArrivalDateAsDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	@XmlElement(name = "departureDate")
	public String getDepartureDate() {
		return dateFormat.format(departureDate);
	}

	public Date departureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		try {
			this.departureDate = dateFormat.parse(departureDate);
		} catch (ParseException exc) {
			exc.printStackTrace();
		}
	}

	public void setDepartureDateAsDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	@XmlElement(name = "numberOfPersons")
	public int getNumberOfPersons() {
		return numberOfPersons;
	}

	public void setNumberOfPersons(int numberOfPersons) {
		this.numberOfPersons = numberOfPersons;
	}

	@XmlElement(name = "customer")
	public XMLClient getCustomer() {
		return customer;
	}

	public void setCustomer(XMLClient customer) {
		this.customer = customer;
	}
}
