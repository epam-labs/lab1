package com.gitlab.andrewkuryan.vt1.jaxbrepository.adapter;

import com.gitlab.andrewkuryan.vt1.entity.request.RejectedRequest;
import com.gitlab.andrewkuryan.vt1.jaxbrepository.adapter.entity.XMLRejectedRequest;

import java.net.MalformedURLException;

public class XMLRejectedRequestAdapter {

	public static RejectedRequest toRequest(XMLRejectedRequest xmlRequest) throws MalformedURLException {
		return new RejectedRequest(
				XMLRoomTypeAdapter.toRoomType(xmlRequest.getRoomType()),
				xmlRequest.arrivalDate(),
				xmlRequest.departureDate(),
				xmlRequest.getNumberOfPersons(),
				XMLClientAdapter.toClient(xmlRequest.getCustomer()),
				xmlRequest.getComment()
		);
	}

	public static XMLRejectedRequest toXmlRequest(RejectedRequest request) {
		XMLRejectedRequest ans = new XMLRejectedRequest();
		ans.setRoomType(XMLRoomTypeAdapter.toXmlRoomType(request.getRoomType()));
		ans.setArrivalDateAsDate(request.getArrivalDate());
		ans.setDepartureDateAsDate(request.getDepartureDate());
		ans.setCustomer(XMLClientAdapter.toXml(request.getCustomer()));
		ans.setNumberOfPersons(request.getNumberOfPersons());
		ans.setComment(request.getComment());
		return ans;
	}

}
