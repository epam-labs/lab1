package com.gitlab.andrewkuryan.vt1.jaxbrepository.adapter;

import com.gitlab.andrewkuryan.vt1.entity.request.ConfirmedRequest;
import com.gitlab.andrewkuryan.vt1.jaxbrepository.adapter.entity.XMLConfirmedRequest;

import java.net.MalformedURLException;

public class XMLConfirmedRequestAdapter {

	public static ConfirmedRequest toRequest(XMLConfirmedRequest xmlRequest) throws MalformedURLException {
		return new ConfirmedRequest(
				XMLRoomTypeAdapter.toRoomType(xmlRequest.getRoomType()),
				xmlRequest.arrivalDate(),
				xmlRequest.departureDate(),
				xmlRequest.getNumberOfPersons(),
				XMLClientAdapter.toClient(xmlRequest.getCustomer()),
				xmlRequest.getRoomNumber()
		);
	}

	public static XMLConfirmedRequest toXmlRequest(ConfirmedRequest request) {
		XMLConfirmedRequest ans = new XMLConfirmedRequest();
		ans.setRoomType(XMLRoomTypeAdapter.toXmlRoomType(request.getRoomType()));
		ans.setArrivalDateAsDate(request.getArrivalDate());
		ans.setDepartureDateAsDate(request.getDepartureDate());
		ans.setCustomer(XMLClientAdapter.toXml(request.getCustomer()));
		ans.setNumberOfPersons(request.getNumberOfPersons());
		ans.setRoomNumber(request.getRoomNumber());
		return ans;
	}

}
