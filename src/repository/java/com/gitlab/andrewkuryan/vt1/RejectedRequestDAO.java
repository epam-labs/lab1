package com.gitlab.andrewkuryan.vt1;

import com.gitlab.andrewkuryan.vt1.entity.request.RejectedRequest;

public interface RejectedRequestDAO extends DAO<RejectedRequest> {
}
