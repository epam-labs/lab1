package com.gitlab.andrewkuryan.vt1.dataprovider;

import com.gitlab.andrewkuryan.vt1.dataprovider.xml.XMLDataProvider;
import com.gitlab.andrewkuryan.vt1.entity.request.ConfirmedRequest;
import com.gitlab.andrewkuryan.vt1.entity.request.RejectedRequest;
import com.gitlab.andrewkuryan.vt1.entity.request.Request;
import com.gitlab.andrewkuryan.vt1.entity.user.Administrator;
import com.gitlab.andrewkuryan.vt1.entity.user.Client;
import com.gitlab.andrewkuryan.vt1.service.mocks.DataProviderMock;
import com.gitlab.andrewkuryan.vt1.service.request.RequestsFilter;
import com.gitlab.andrewkuryan.vt1.service.utils.Pagination;
import com.gitlab.andrewkuryan.vt1.utils.Either;
import com.gitlab.andrewkuryan.vt1.utils.Pair;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RequestsDataProviderTest {

	private static XMLDataProvider dataProvider;
	private static Client client;
	private static Administrator admin;

	@BeforeAll
	static void initDataProvider() {
		dataProvider = new XMLDataProvider(Paths.get("xmlstorage"));
		client = new Client("Test", "Test", "test@test.com", "Test", "Test", "");
		admin = new Administrator("admin", "f6fdffe48c908deb0f4c3bd36c032e72", "Admin", "Admin");
	}

	@Test
	@DisplayName("should return all requests for client")
	void testGetAllClientRequests() {
		Either<List<Request>, Exception> result = dataProvider.getAllRequests(client);
		assertTrue(result.isLeft());
		assertEquals(7, result.getLeft().size());
	}

	@Test
	@DisplayName("should return all requests for admin")
	void testGetAllAdminRequests() {
		Either<List<Request>, Exception> result = dataProvider.getAllRequests(admin);
		assertTrue(result.isLeft());
		assertEquals(7, result.getLeft().size());
	}

	@Test
	@DisplayName("should return requests with pagination")
	void getRequestsWithPagination() {
		Either<Pair<List<Request>, Pagination>, Exception> result = dataProvider.getRequests(
				admin,
				new Pagination(3, 3, 0)
		);
		assertTrue(result.isLeft());
		assertEquals(7, result.getLeft().getSecond().getTotalCount());
	}

	@Test
	@DisplayName("should return filtered reqests")
	void getFilteredRequests() {
		RequestsFilter filter = new RequestsFilter();
		filter.setNumberOfPersonsFrom(2);
		filter.setArrivalDateFrom(new GregorianCalendar(2019, Calendar.DECEMBER, 1).getTime());
		filter.setDepartureDateTo(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
		Either<Pair<List<Request>, Pagination>, Exception> result = dataProvider.getRequests(
				admin,
				new Pagination(7, 1, 0),
				filter
		);
		assertTrue(result.isLeft());
		assertEquals(1, result.getLeft().getFirst().size());
	}

	@Test
	@DisplayName("should create new request")
	void createRequestTest() {
		Either<Request, Exception> result = dataProvider.createRequest(
				client,
				DataProviderMock.getTestRoomTypes().get(0),
				new GregorianCalendar(2020, Calendar.FEBRUARY, 2).getTime(),
				new GregorianCalendar(2020, Calendar.FEBRUARY, 10).getTime(),
				2
		);
		assertTrue(result.isLeft());
	}

	@Test
	@DisplayName("should confirm request")
	void confirmRequestTest() {
		Optional<Exception> result = dataProvider.confirmRequest(
				admin,
				new Request(
						DataProviderMock.getTestRoomTypes().get(0),
						new GregorianCalendar(2019, Calendar.DECEMBER, 10).getTime(),
						new GregorianCalendar(2019, Calendar.DECEMBER, 15).getTime(),
						2,
						client
				),
				103
		);
		assertTrue(result.isEmpty());

		RequestsFilter filter = new RequestsFilter();
		filter.setConfirmed(true);
		filter.setArrivalDateFrom(new GregorianCalendar(2019, Calendar.DECEMBER, 10).getTime());
		filter.setArrivalDateTo(new GregorianCalendar(2019, Calendar.DECEMBER, 10).getTime());
		Either<Pair<List<Request>, Pagination>, Exception> newResult = dataProvider.getRequests(
				admin,
				new Pagination(7, 1, 0),
				filter
		);

		assertTrue(newResult.getLeft().getFirst().get(0) instanceof ConfirmedRequest);
		assertEquals(103, ((ConfirmedRequest) newResult.getLeft().getFirst().get(0)).getRoomNumber());
	}

	@Test
	@DisplayName("should reject request")
	void rejectRequestTest() {
		Optional<Exception> result = dataProvider.rejectRequest(
				admin,
				new Request(
						DataProviderMock.getTestRoomTypes().get(1),
						new GregorianCalendar(2019, Calendar.NOVEMBER, 7).getTime(),
						new GregorianCalendar(2019, Calendar.NOVEMBER, 10).getTime(),
						3,
						client
				),
				"Test reason"
		);
		assertTrue(result.isEmpty());

		RequestsFilter filter = new RequestsFilter();
		filter.setRejected(true);
		filter.setArrivalDateFrom(new GregorianCalendar(2019, Calendar.NOVEMBER, 7).getTime());
		filter.setArrivalDateTo(new GregorianCalendar(2019, Calendar.NOVEMBER, 7).getTime());
		Either<Pair<List<Request>, Pagination>, Exception> newResult = dataProvider.getRequests(
				admin,
				new Pagination(7, 1, 0),
				filter
		);

		assertTrue(newResult.getLeft().getFirst().get(0) instanceof RejectedRequest);
		assertEquals("Test reason", ((RejectedRequest) newResult.getLeft().getFirst().get(0)).getComment());
	}

	@Test
	@DisplayName("should cancel request")
	void cancelRequestTest() {
		Optional<Exception> result = dataProvider.cancelRequest(
				client,
				new RejectedRequest(
						DataProviderMock.getTestRoomTypes().get(4),
						new GregorianCalendar(2019, Calendar.DECEMBER, 31).getTime(),
						new GregorianCalendar(2020, Calendar.JANUARY, 5).getTime(),
						2,
						client,
						"Technical reasons"
				)
		);
		assertTrue(result.isEmpty());

		RequestsFilter filter = new RequestsFilter();
		filter.setArrivalDateFrom(new GregorianCalendar(2019, Calendar.DECEMBER, 31).getTime());
		filter.setArrivalDateTo(new GregorianCalendar(2019, Calendar.DECEMBER, 31).getTime());
		Either<Pair<List<Request>, Pagination>, Exception> newResult = dataProvider.getRequests(
				admin,
				new Pagination(7, 1, 0),
				filter
		);

		assertEquals(0, newResult.getLeft().getFirst().size());
	}

	@AfterEach
	void resetFile() {
		try {
			Files.write(
					Paths.get("teststorage/requests/requests.csv"),
					Arrays.asList(
						"processing|Standard 2x|10.12.2019|15.12.2019|2|client|",
						"processing|Standard 3x|07.11.2019|10.11.2019|3|client|",
						"confirmed|Standard 3x|25.11.2019|05.12.2019|2|client2|105",
						"processing|Economy 1x|01.11.2019|02.11.2019|1|client|",
						"processing|Lux 2x|01.01.2020|07.01.2020|2|client2|",
						"processing|Lux 3x|25.12.2019|02.01.2020|3|client|",
						"rejected|Lux 3x|31.12.2019|05.01.2020|2|client|Technical reasons"
					)
			);
		} catch (IOException exc) {
			exc.printStackTrace();
		}
	}
}
