package com.gitlab.andrewkuryan.vt1.dataprovider;

import com.gitlab.andrewkuryan.vt1.dataprovider.xml.XMLDataProvider;
import com.gitlab.andrewkuryan.vt1.entity.roomtype.AvailableRoomTypes;
import com.gitlab.andrewkuryan.vt1.entity.user.User;
import com.gitlab.andrewkuryan.vt1.service.roomtype.AvailableRoomTypesFilter;
import com.gitlab.andrewkuryan.vt1.service.utils.Pagination;
import com.gitlab.andrewkuryan.vt1.utils.Either;
import com.gitlab.andrewkuryan.vt1.utils.Pair;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.nio.file.Paths;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AvailableRoomsDataProviderTest {

	private static XMLDataProvider dataProvider;

	@BeforeAll
	static void initDataProvider() {
		dataProvider = new XMLDataProvider(Paths.get("xmlstorage"));
	}

	@Test
	@DisplayName("should return available room types in range")
	void getAvailableRoomTypes() {
		AvailableRoomTypesFilter filter = new AvailableRoomTypesFilter();
		filter.setTypeName("Standard 3x");
		filter.setArrivalDate(new GregorianCalendar(2019, Calendar.NOVEMBER, 22).getTime());
		filter.setDepartureDate(new GregorianCalendar(2019, Calendar.DECEMBER, 3).getTime());
		Either<Pair<List<AvailableRoomTypes>, Pagination>, Exception> result =
				dataProvider.getAvailableRoomTypes(
						User.GUEST,
						new Pagination(5, 1, 0),
						filter
				);

		assertTrue(result.isLeft());
		assertEquals(2, result.getLeft().getFirst().get(0).getAvailableRooms());
	}
}
