package com.gitlab.andrewkuryan.vt1.dataprovider;

import com.gitlab.andrewkuryan.vt1.dataprovider.parsers.RoomTypesDOMParser;
import com.gitlab.andrewkuryan.vt1.dataprovider.xml.XMLRoomTypesDataProvider;
import com.gitlab.andrewkuryan.vt1.entity.roomtype.RoomType;
import com.gitlab.andrewkuryan.vt1.entity.user.User;
import com.gitlab.andrewkuryan.vt1.service.roomtype.RoomTypeFilter;
import com.gitlab.andrewkuryan.vt1.service.utils.Pagination;
import com.gitlab.andrewkuryan.vt1.utils.Either;
import com.gitlab.andrewkuryan.vt1.utils.Pair;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RoomTypeDataProviderTest {

	private static XMLRoomTypesDataProvider dataProvider;

	@BeforeAll
	static void initDataProvider() {
		dataProvider = new XMLRoomTypesDataProvider(
				Paths.get("xmlstorage"),
				new RoomTypesDOMParser()
		);
	}

	@Test
	@DisplayName("should return all room types")
	void getAllTest() {
		Either<List<RoomType>, Exception> result = dataProvider.getAllRoomTypes(User.GUEST);
		assertTrue(result.isLeft());
		assertEquals(5, result.getLeft().size());
	}

	@Test
	@DisplayName("should return room types with pagination")
	void getRoomTypesWithPagination() {
		Either<Pair<List<RoomType>, Pagination>, Exception> result = dataProvider.getRoomTypes(
				User.GUEST,
				new Pagination(4, 2, 0)
		);
		assertTrue(result.isLeft());
		assertEquals(5, result.getLeft().getSecond().getTotalCount());
	}

	@Test
	@DisplayName("should return filtered room types")
	void getFilteredRoomTypes() {
		RoomTypeFilter filter = new RoomTypeFilter();
		filter.setNumOfPlacesFrom(2);
		filter.setCostTo(2000);
		filter.setServices(Collections.singletonList("Transfer"));
		Either<Pair<List<RoomType>, Pagination>, Exception> result = dataProvider.getRoomTypes(
				User.GUEST,
				new Pagination(5, 1, 0),
				filter
		);
		assertTrue(result.isLeft());
		assertEquals(1, result.getLeft().getFirst().size());
	}

	@Test
	@DisplayName("should return all services")
	void getServicesTest() {
		Either<List<String>, Exception> result = dataProvider.getAvailableServices(User.GUEST);
		assertTrue(result.isLeft());
		assertEquals(4, result.getLeft().size());
	}
}
