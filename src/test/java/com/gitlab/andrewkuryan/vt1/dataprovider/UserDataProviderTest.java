package com.gitlab.andrewkuryan.vt1.dataprovider;

import com.gitlab.andrewkuryan.vt1.dataprovider.parsers.AdminParser;
import com.gitlab.andrewkuryan.vt1.dataprovider.parsers.ClientParser;
import com.gitlab.andrewkuryan.vt1.dataprovider.xml.XMLUsersDataProvider;
import com.gitlab.andrewkuryan.vt1.entity.user.Client;
import com.gitlab.andrewkuryan.vt1.entity.user.User;
import com.gitlab.andrewkuryan.vt1.service.port.exceptions.IncorrectPasswordException;
import com.gitlab.andrewkuryan.vt1.service.port.exceptions.NoSuchUserException;
import com.gitlab.andrewkuryan.vt1.utils.Either;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UserDataProviderTest {

	private static XMLUsersDataProvider dataProvider;

	@BeforeAll
	static void initDataProvider() {
		dataProvider = new XMLUsersDataProvider(
				Paths.get("xmlstorage"),
				new ClientParser(),
				new AdminParser()
		);
	}

	@Test
	@DisplayName("should authorize user")
	void authTest() {
		Either<User, Exception> result = dataProvider.authorize("client", "123");
		assertTrue(result.isLeft());
		assertEquals("client", result.getLeft().getLogin());
	}

	@Test
	@DisplayName("should return error if login not exists")
	void authWithNonexistentLogin() {
		Either<User, Exception> result = dataProvider.authorize("nonexistent", "123");
		assertTrue(result.isRight());
		assertTrue(result.getRight() instanceof NoSuchUserException);
	}

	@Test
	@DisplayName("should return error if password is incorrect")
	void authWithIncorrectPassword() {
		Either<User, Exception> result = dataProvider.authorize("client", "incorrect");
		assertTrue(result.isRight());
		assertTrue(result.getRight() instanceof IncorrectPasswordException);
	}

	@Test
	@DisplayName("should add new user")
	void registerTest() {
		Either<User, Exception> result = dataProvider.register(
				"newClient", "123", "New", "New", "new@new.com", ""
		);
		assertTrue(result.isLeft());
		assertEquals("newClient", result.getLeft().getLogin());
	}

	@Test
	@DisplayName("should update user")
	void updateTest() {
		Either<User, Exception> result = dataProvider.update(
				new User("client", "123"),
				"123",
				"123",
				"Test",
				"Test",
				"updated_test@test.com",
				"(11)1111111"
		);
		assertTrue(result.isLeft());
		Either<User, Exception> userResult = dataProvider.authorize("client", "123");
		assertTrue(userResult.isLeft());
		assertEquals("updated_test@test.com", ((Client) userResult.getLeft()).getEmail());
	}

	@Test
	@DisplayName("should return error if oldPassword != user password")
	void updateTestWithError() {
		Either<User, Exception> result = dataProvider.update(
				new User("client", "123"),
				"incorrect",
				"123",
				"Test",
				"Test",
				"updated_test@test.com",
				"(11)1111111"
		);
		assertTrue(result.isRight());
		assertTrue(result.getRight() instanceof IncorrectPasswordException);
	}

	@AfterAll
	static void resetFile() {
		try {
			Files.write(
					Paths.get("teststorage/users/client.csv"),
					Arrays.asList(
							"client|3677b23baa08f74c28aba07f0cb6554e|Test|Test|test@test.com|(11)1111111",
							"client2|5637c516a9e510124fa260f6a30e57f5|Test2|Test2|test2@test.com|(22)2222222"
					)
			);
		} catch (IOException exc) {
			exc.printStackTrace();
		}
	}
}
