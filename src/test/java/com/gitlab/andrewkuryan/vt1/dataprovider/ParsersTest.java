package com.gitlab.andrewkuryan.vt1.dataprovider;

import com.gitlab.andrewkuryan.vt1.dataprovider.parsers.*;
import com.gitlab.andrewkuryan.vt1.entity.request.ConfirmedRequest;
import com.gitlab.andrewkuryan.vt1.entity.request.RejectedRequest;
import com.gitlab.andrewkuryan.vt1.entity.request.Request;
import com.gitlab.andrewkuryan.vt1.entity.roomtype.RoomType;
import com.gitlab.andrewkuryan.vt1.entity.user.Administrator;
import com.gitlab.andrewkuryan.vt1.entity.user.Client;
import com.gitlab.andrewkuryan.vt1.repository.DefaultData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ParsersTest {

	@Test
	void roomTypesParseTest() {
		var parser = new RoomTypesDOMParser();
		try {
			List<RoomType> roomTypes =
					parser.parse(new FileInputStream("xmlstorage/roomTypes.xml"));
			assertEquals(DefaultData.getRoomTypes(), roomTypes);
		} catch (FileNotFoundException exc) {
			Assertions.fail(exc);
		}
	}

	@Test
	void adminsParseTest() {
		var parser = new AdminParser();
		try {
			List<Administrator> admins =
					parser.parse(new FileInputStream("xmlstorage/admins.xml"));
			System.out.println(admins);
			assertEquals(DefaultData.getAdministrators(), admins);
		} catch (FileNotFoundException exc) {
			Assertions.fail(exc);
		}
	}

	@Test
	void clientsParseTest() {
		var parser = new ClientParser();
		try {
			List<Client> clients =
					parser.parse(new FileInputStream("xmlstorage/clients.xml"));
			System.out.println(clients);
			assertEquals(DefaultData.getClients(), clients);
		} catch (FileNotFoundException exc) {
			Assertions.fail(exc);
		}
	}

	@Test
	void requestsParseTest() {
		var parser = new RequestsParser(new ClientParser(), new RoomTypesDOMParser());
		try {
			List<Request> requests = parser
					.parse(new FileInputStream("xmlstorage/requests.xml"));
			assertEquals(DefaultData.getRequests(), requests);
		} catch (FileNotFoundException exc) {
			Assertions.fail(exc);
		}
	}

	@Test
	void confirmedRequestsParseTest() {
		var parser = new ConfirmedRequestsParser(new ClientParser(), new RoomTypesDOMParser());
		try {
			List<ConfirmedRequest> requests = parser
					.parse(new FileInputStream("xmlstorage/confirmedRequests.xml"));
			System.out.println(requests);
			assertEquals(DefaultData.getConfirmedRequests(), requests);
		} catch (FileNotFoundException exc) {
			Assertions.fail(exc);
		}
	}

	@Test
	void rejectedRequestsParseTest() {
		var parser = new RejectedRequestsParser(new ClientParser(), new RoomTypesDOMParser());
		try {
			List<RejectedRequest> requests = parser
					.parse(new FileInputStream("xmlstorage/rejectedRequests.xml"));
			System.out.println(requests);
			assertEquals(DefaultData.getRejectedRequests(), requests);
		} catch (FileNotFoundException exc) {
			Assertions.fail(exc);
		}
	}
}
