package com.gitlab.andrewkuryan.vt1.service.mocks;

import com.gitlab.andrewkuryan.vt1.service.AppService;
import com.gitlab.andrewkuryan.vt1.service.AppViewModel;
import com.gitlab.andrewkuryan.vt1.service.account.AccountModel;
import com.gitlab.andrewkuryan.vt1.service.auth.LoginModel;
import com.gitlab.andrewkuryan.vt1.service.auth.RegisterModel;
import com.gitlab.andrewkuryan.vt1.service.port.DataProvider;
import com.gitlab.andrewkuryan.vt1.service.request.AdminRequestsModel;
import com.gitlab.andrewkuryan.vt1.service.request.ClientRequestsModel;
import com.gitlab.andrewkuryan.vt1.service.roomtype.AvailableRoomTypesModel;
import com.gitlab.andrewkuryan.vt1.service.roomtype.RoomTypeModel;
import org.mockito.Mockito;

public class AppServiceMock implements AppService {

	@Override
	public DataProvider getDataProvider() {
		return DataProviderMock.getMock();
	}

	@Override
	public AppViewModel getViewModel() {
		return Mockito.mock(AppViewModel.class);
	}

	@Override
	public AccountModel getAccountModel() {
		return AccountModelMock.getMock();
	}

	@Override
	public LoginModel getLoginModel() {
		return Mockito.mock(LoginModel.class);
	}

	@Override
	public RegisterModel getRegisterModel() {
		return Mockito.mock(RegisterModel.class);
	}

	@Override
	public RoomTypeModel getRoomTypeModel() {
		return Mockito.mock(RoomTypeModel.class);
	}

	@Override
	public AvailableRoomTypesModel getAvailableRoomTypesModel() {
		return Mockito.mock(AvailableRoomTypesModel.class);
	}

	@Override
	public ClientRequestsModel getClientRequestsModel() {
		return Mockito.mock(ClientRequestsModel.class);
	}

	@Override
	public AdminRequestsModel getAdminRequestsModel() {
		return Mockito.mock(AdminRequestsModel.class);
	}
}
