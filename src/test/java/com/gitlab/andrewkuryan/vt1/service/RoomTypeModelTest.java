package com.gitlab.andrewkuryan.vt1.service;

import com.gitlab.andrewkuryan.vt1.service.mocks.AccountModelMock;
import com.gitlab.andrewkuryan.vt1.service.mocks.AppServiceMock;
import com.gitlab.andrewkuryan.vt1.service.mocks.DataProviderMock;
import com.gitlab.andrewkuryan.vt1.service.roomtype.AppRoomTypeModel;
import com.gitlab.andrewkuryan.vt1.service.roomtype.RoomTypeModel;
import com.gitlab.andrewkuryan.vt1.service.roomtype.port.RoomTypeViewModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;

class RoomTypeModelTest {

	private RoomTypeModel model;
	private RoomTypeViewModel viewModel;

	@BeforeEach
	void initModel() {
		model = new AppRoomTypeModel(new AppServiceMock());

		viewModel = Mockito.mock(RoomTypeViewModel.class);
		Mockito.when(viewModel.getRecordsPerPage()).thenReturn(2);
		Mockito.when(viewModel.getFilter()).thenReturn(Optional.empty());
		model.addViewModel(viewModel);

		Mockito.clearInvocations(DataProviderMock.getMock(), AccountModelMock.getMock());
	}

	@Test
	@DisplayName("should load room types with 2 records per page")
	void testLoadRoomTypes() {
		model.getRoomTypesOnPage(1, true);

		Mockito.verify(viewModel).setActualRoomTypes(
				DataProviderMock.getTestRoomTypes().subList(0, 2)
		);
		Mockito.verify(viewModel).setPage(1);
		Mockito.verify(viewModel).setTotalCount(5);

		Mockito.verify(DataProviderMock.getMock(), Mockito.times(1))
				.getRoomTypes(Mockito.any(), Mockito.any());

		Mockito.verify(AccountModelMock.getMock(), Mockito.times(1))
				.getAccount();
	}

	@Test
	@DisplayName("should get room types from loaded")
	void testGetRoomTypesFromLoaded() {
		model.getRoomTypesOnPage(1, true);
		model.getRoomTypesOnPage(2, true);
		model.getRoomTypesOnPage(1, true);

		Mockito.verify(viewModel, Mockito.times(3))
				.setActualRoomTypes(Mockito.any());

		Mockito.verify(DataProviderMock.getMock(), Mockito.times(2))
				.getRoomTypes(Mockito.any(), Mockito.any());
	}
}
