package com.gitlab.andrewkuryan.vt1.service.mocks;

import com.gitlab.andrewkuryan.vt1.entity.request.ConfirmedRequest;
import com.gitlab.andrewkuryan.vt1.entity.request.RejectedRequest;
import com.gitlab.andrewkuryan.vt1.entity.request.Request;
import com.gitlab.andrewkuryan.vt1.entity.roomtype.AvailableRoomTypes;
import com.gitlab.andrewkuryan.vt1.entity.roomtype.RoomType;
import com.gitlab.andrewkuryan.vt1.entity.roomtype.RoomTypeBuilder;
import com.gitlab.andrewkuryan.vt1.entity.user.Administrator;
import com.gitlab.andrewkuryan.vt1.entity.user.Client;
import com.gitlab.andrewkuryan.vt1.entity.user.User;
import com.gitlab.andrewkuryan.vt1.service.port.DataProvider;
import com.gitlab.andrewkuryan.vt1.service.utils.Pagination;
import com.gitlab.andrewkuryan.vt1.utils.Either;
import com.gitlab.andrewkuryan.vt1.utils.Pair;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings("unchecked")
public class DataProviderMock {

	private static DataProvider mock;

	static {
		mock = Mockito.mock(DataProvider.class);

		for (int i = 1, p = 1; i <= 5; i += 2, p++) {
			final int page = p;
			var response = Either
					.<Pair<List<RoomType>, Pagination>, Exception>left(
							new Pair<>(
									getTestRoomTypes().subList(i - 1, i + 1 <= 5 ? i + 1 : 5),
									new Pagination(2, page, 5)
							)
					);
			Mockito.when(mock.getRoomTypes(
					Mockito.any(),
					Mockito.argThat(argument -> argument != null && argument.getRecordsPerPage() == 2 && argument.getPage() == page))
			).thenReturn(response);
			Mockito.when(mock.getRoomTypes(
					Mockito.any(),
					Mockito.argThat(argument -> argument != null && argument.getRecordsPerPage() == 2 && argument.getPage() == page),
					Mockito.any())
			).thenReturn(response);
		}
		for (int i = 1; i <= 5; i++) {
			final int index = i;
			var response = Either
					.<Pair<List<RoomType>, Pagination>, Exception>left(
							new Pair<>(
									getTestRoomTypes().subList(i - 1, i),
									new Pagination(1, i, 5)
							)
					);
			Mockito.when(mock.getRoomTypes(
					Mockito.any(),
					Mockito.argThat(argument -> argument != null && argument.getRecordsPerPage() == 1 && argument.getPage() == index))
			).thenReturn(response);
			Mockito.when(mock.getRoomTypes(
					Mockito.any(),
					Mockito.argThat(argument -> argument != null && argument.getRecordsPerPage() == 1 && argument.getPage() == index),
					Mockito.any())
			).thenReturn(response);
		}

		for (int i = 1, p = 1; i <= 5; i += 2, p++) {
			final int page = p;
			var response = Either
					.<Pair<List<AvailableRoomTypes>, Pagination>, Exception>left(
							new Pair<>(
									getTestAvailableRoomTypes().subList(i - 1, i + 1 <= 5 ? i + 1 : 5),
									new Pagination(2, page, 5)
							)
					);
			Mockito.when(mock.getAvailableRoomTypes(
					Mockito.any(),
					Mockito.argThat(argument -> argument != null && argument.getRecordsPerPage() == 2 && argument.getPage() == page))
			).thenReturn(response);
			Mockito.when(mock.getAvailableRoomTypes(
					Mockito.any(),
					Mockito.argThat(argument -> argument != null && argument.getRecordsPerPage() == 2 && argument.getPage() == page),
					Mockito.any())
			).thenReturn(response);
		}
		for (int i = 1; i <= 5; i++) {
			final int index = i;
			var response = Either
					.<Pair<List<AvailableRoomTypes>, Pagination>, Exception>left(
							new Pair<>(
									getTestAvailableRoomTypes().subList(i - 1, i),
									new Pagination(1, i, 5)
							)
					);
			Mockito.when(mock.getAvailableRoomTypes(
					Mockito.any(),
					Mockito.argThat(argument -> argument != null && argument.getRecordsPerPage() == 1 && argument.getPage() == index))
			).thenReturn(response);
			Mockito.when(mock.getAvailableRoomTypes(
					Mockito.any(),
					Mockito.argThat(argument -> argument != null && argument.getRecordsPerPage() == 1 && argument.getPage() == index),
					Mockito.any())
			).thenReturn(response);
		}

		for (int i = 1, p = 1; i <= 7; i += 2, p++) {
			final int page = p;
			var response = Either
					.<Pair<List<Request>, Pagination>, Exception>left(
							new Pair<>(
									getTestRequests().subList(i - 1, i + 1 <= 7 ? i + 1 : 7),
									new Pagination(2, page, 7)
							)
					);
			Mockito.when(mock.getRequests(
					Mockito.any(),
					Mockito.argThat(argument -> argument != null && argument.getRecordsPerPage() == 2 && argument.getPage() == page))
			).thenReturn(response);
			Mockito.when(mock.getRequests(
					Mockito.any(),
					Mockito.argThat(argument -> argument != null && argument.getRecordsPerPage() == 2 && argument.getPage() == page),
					Mockito.any())
			).thenReturn(response);
		}
		for (int i = 1; i <= 7; i++) {
			final int index = i;
			var response = Either
					.<Pair<List<Request>, Pagination>, Exception>left(
							new Pair<>(
									getTestRequests().subList(i - 1, i),
									new Pagination(1, index, 7)
							)
					);
			Mockito.when(mock.getRequests(
					Mockito.any(),
					Mockito.argThat(argument -> argument != null && argument.getRecordsPerPage() == 1 && argument.getPage() == index))
			).thenReturn(response);
			Mockito.when(mock.getRequests(
					Mockito.any(),
					Mockito.argThat(argument -> argument != null && argument.getRecordsPerPage() == 1 && argument.getPage() == index),
					Mockito.any())
			).thenReturn(response);
		}


		Mockito.when(mock.authorize(Mockito.any(), Mockito.any()))
				.thenReturn(Either.<User, Exception>
						left(new Client("Test", "Test", "test@test.com", "Test", "Test"))
				);
		Mockito.when(mock.authorize(ArgumentMatchers.eq("admin"), Mockito.any()))
				.thenReturn(Either.<User, Exception>
						left(new Administrator("Admin", "Admin", "Admin", "Admin")));


		Mockito.when(mock.getAvailableServices(Mockito.any()))
				.thenReturn(Either.<List<String>, Exception>
						left(Arrays.asList("Transfer", "Breakfast", "Parking", "Own kitchen")));
	}

	public static List<RoomType> getTestRoomTypes() {
		try {
			var roomType1 = new RoomTypeBuilder()
					.setTypeName("Standard 2x")
					.setNumOfPlaces(2)
					.setCost(1000)
					.setLargeImages(Collections.singletonList(new URL("https://images5.travelatacdn.ru/upload/2019_01/content_hotel_5c2c8e471ef0e7.62765995.jpg?width=1920&height=1080")))
					.setArea(25.0)
					.setRoomNumbers(Arrays.asList(100, 101, 102))
					.build();
			var roomType2 = new RoomTypeBuilder()
					.setTypeName("Standard 3x")
					.setNumOfPlaces(3)
					.setCost(1500)
					.setArea(35.0)
					.setRoomNumbers(Arrays.asList(103, 104, 105))
					.setServices(Arrays.asList("Transfer", "Parking"))
					.build();
			var roomType3 = new RoomTypeBuilder()
					.setTypeName("Economy 1x")
					.setNumOfPlaces(1)
					.setCost(300)
					.setArea(20.0)
					.setRoomNumbers(Arrays.asList(200, 201, 202, 203, 204))
					.build();
			var roomType4 = new RoomTypeBuilder()
					.setTypeName("Lux 2x")
					.setNumOfPlaces(2)
					.setSmallImage(new URL("https://www.begonija.lv/i/full_size/tourist_hotels/6-10125491_4306.jpg"))
					.setCost(2000)
					.setArea(45.0)
					.setRoomNumbers(Arrays.asList(300, 301, 302))
					.build();
			var roomType5 = new RoomTypeBuilder()
					.setTypeName("Lux 3x")
					.setNumOfPlaces(3)
					.setCost(3000)
					.setArea(55.0)
					.setRoomNumbers(Arrays.asList(400, 401))
					.build();
			return Arrays.asList(roomType1, roomType2, roomType3, roomType4, roomType5);
		} catch (Exception exc) {
			return Collections.emptyList();
		}
	}

	public static List<AvailableRoomTypes> getTestAvailableRoomTypes() {
		return getTestRoomTypes().stream()
				.map(roomType -> new AvailableRoomTypes(
						roomType,
						2
				)).collect(Collectors.toList());
	}

	public static List<Request> getTestRequests() {
		var testClient = new Client("Test", "Test", "test@test.com", "Test", "Test");
		var request1 = new Request(
				getTestRoomTypes().get(0),
				new GregorianCalendar(2019, Calendar.DECEMBER, 10).getTime(),
				new GregorianCalendar(2019, Calendar.DECEMBER, 15).getTime(),
				2,
				testClient
		);
		var request2 = new Request(
				getTestRoomTypes().get(1),
				new GregorianCalendar(2019, Calendar.NOVEMBER, 7).getTime(),
				new GregorianCalendar(2019, Calendar.NOVEMBER, 10).getTime(),
				3,
				testClient
		);
		var confirmedRequest = new ConfirmedRequest(
				getTestRoomTypes().get(1),
				new GregorianCalendar(2019, Calendar.NOVEMBER, 25).getTime(),
				new GregorianCalendar(2019, Calendar.DECEMBER, 5).getTime(),
				2,
				testClient,
				105
		);
		var request3 = new Request(
				getTestRoomTypes().get(2),
				new GregorianCalendar(2019, Calendar.NOVEMBER, 1).getTime(),
				new GregorianCalendar(2019, Calendar.NOVEMBER, 2).getTime(),
				1,
				testClient
		);
		var request4 = new Request(
				getTestRoomTypes().get(3),
				new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(),
				new GregorianCalendar(2020, Calendar.JANUARY, 7).getTime(),
				2,
				testClient
		);
		var request5 = new Request(
				getTestRoomTypes().get(4),
				new GregorianCalendar(2019, Calendar.DECEMBER, 25).getTime(),
				new GregorianCalendar(2020, Calendar.JANUARY, 2).getTime(),
				3,
				testClient
		);
		var rejectedRequest = new RejectedRequest(
				getTestRoomTypes().get(4),
				new GregorianCalendar(2019, Calendar.DECEMBER, 31).getTime(),
				new GregorianCalendar(2020, Calendar.JANUARY, 5).getTime(),
				2,
				testClient,
				"Technical reasons"
		);
		return Arrays.asList(request1, request2, confirmedRequest, request3, request4, request5, rejectedRequest);
	}

	public static DataProvider getMock() {
		return mock;
	}
}
