package com.gitlab.andrewkuryan.vt1.service;

import com.gitlab.andrewkuryan.vt1.service.mocks.AccountModelMock;
import com.gitlab.andrewkuryan.vt1.service.mocks.AppServiceMock;
import com.gitlab.andrewkuryan.vt1.service.mocks.DataProviderMock;
import com.gitlab.andrewkuryan.vt1.service.request.AppClientRequestsModel;
import com.gitlab.andrewkuryan.vt1.service.request.ClientRequestsModel;
import com.gitlab.andrewkuryan.vt1.service.request.port.RequestsViewModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Optional;

class RequestsModelTest {

	private ClientRequestsModel model;
	private RequestsViewModel viewModel;

	@BeforeEach
	void initModel() {
		model = new AppClientRequestsModel(new AppServiceMock());

		viewModel = Mockito.mock(RequestsViewModel.class);
		Mockito.when(viewModel.getRecordsPerPage()).thenReturn(2);
		Mockito.when(viewModel.getFilter()).thenReturn(Optional.empty());
		model.addViewModel(viewModel);

		Mockito.clearInvocations(DataProviderMock.getMock(), AccountModelMock.getMock());
	}

	@Test
	@DisplayName("should load room types with 2 records per page")
	void testLoadRoomTypes() {
		model.getRequestsOnPage(1, true);

		Mockito.verify(viewModel).setActualRequests(
				DataProviderMock.getTestRequests().subList(0, 2)
		);
		Mockito.verify(viewModel).setPage(1);
		Mockito.verify(viewModel).setTotalCount(5);

		Mockito.verify(DataProviderMock.getMock(), Mockito.times(1))
				.getRequests(Mockito.any(), Mockito.any());

		Mockito.verify(AccountModelMock.getMock(), Mockito.times(1))
				.getAccount();
	}

	@Test
	@DisplayName("should get room types from loaded")
	void testGetRoomTypesFromLoaded() {
		model.getRequestsOnPage(1, true);
		model.getRequestsOnPage(2, true);
		model.getRequestsOnPage(1, true);

		Mockito.verify(viewModel, Mockito.times(3))
				.setActualRequests(Mockito.any());

		Mockito.verify(DataProviderMock.getMock(), Mockito.times(2))
				.getRequests(Mockito.any(), Mockito.any());
	}
}
