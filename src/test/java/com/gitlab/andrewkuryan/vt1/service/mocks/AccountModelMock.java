package com.gitlab.andrewkuryan.vt1.service.mocks;

import com.gitlab.andrewkuryan.vt1.entity.user.User;
import com.gitlab.andrewkuryan.vt1.service.account.AccountModel;
import org.mockito.Mockito;

import java.util.Optional;

public class AccountModelMock {

	private static AccountModel mock;

	static {
		mock = Mockito.mock(AccountModel.class);

		User testUser = new User("Test", "Test");
		Mockito.when(mock.getAccount()).thenReturn(Optional.of(testUser));
	}

	public static AccountModel getMock() {
		return mock;
	}
}
