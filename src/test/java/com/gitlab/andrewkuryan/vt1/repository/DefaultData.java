package com.gitlab.andrewkuryan.vt1.repository;

import com.gitlab.andrewkuryan.vt1.entity.request.ConfirmedRequest;
import com.gitlab.andrewkuryan.vt1.entity.request.RejectedRequest;
import com.gitlab.andrewkuryan.vt1.entity.request.Request;
import com.gitlab.andrewkuryan.vt1.entity.roomtype.RoomType;
import com.gitlab.andrewkuryan.vt1.entity.user.Administrator;
import com.gitlab.andrewkuryan.vt1.entity.user.Client;
import com.gitlab.andrewkuryan.vt1.service.mocks.DataProviderMock;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class DefaultData {

	public static List<Administrator> getAdministrators() {
		return Collections.singletonList(
				new Administrator(
						"admin",
						"f6fdffe48c908deb0f4c3bd36c032e72",
						"Admin",
						"Admin"
				)
		);
	}

	public static List<Client> getClients() {
		return Arrays.asList(
				new Client(
						"client",
						"3677b23baa08f74c28aba07f0cb6554e",
						"Test",
						"Test",
						"test@test.com",
						"(11)1111111"
				),
				new Client(
						"client2",
						"5637c516a9e510124fa260f6a30e57f5",
						"Test2",
						"Test2",
						"test2@test.com",
						"(22)2222222"
				)
		);
	}

	public static List<RoomType> getRoomTypes() {
		return DataProviderMock.getTestRoomTypes();
	}

	public static List<Request> getRequests() {
		return DataProviderMock.getTestRequests()
				.stream()
				.filter(request -> !(request instanceof ConfirmedRequest) &&
						!(request instanceof RejectedRequest))
				.collect(Collectors.toList());
	}

	public static List<ConfirmedRequest> getConfirmedRequests() {
		return DataProviderMock.getTestRequests()
				.stream()
				.filter(request -> request instanceof ConfirmedRequest)
				.map(request -> (ConfirmedRequest) request)
				.collect(Collectors.toList());
	}

	public static List<RejectedRequest> getRejectedRequests() {
		return DataProviderMock.getTestRequests()
				.stream()
				.filter(request -> request instanceof RejectedRequest)
				.map(request -> (RejectedRequest) request)
				.collect(Collectors.toList());
	}
}
