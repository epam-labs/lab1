package com.gitlab.andrewkuryan.vt1.repository;

import com.gitlab.andrewkuryan.vt1.entity.request.ConfirmedRequest;
import com.gitlab.andrewkuryan.vt1.entity.request.RejectedRequest;
import com.gitlab.andrewkuryan.vt1.entity.request.Request;
import com.gitlab.andrewkuryan.vt1.entity.roomtype.RoomType;
import com.gitlab.andrewkuryan.vt1.entity.user.Administrator;
import com.gitlab.andrewkuryan.vt1.entity.user.Client;
import com.gitlab.andrewkuryan.vt1.jaxbrepository.XMLDAO;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.assertEquals;

class XMLTests {

	@Test
	void clientMarshallTest() {
		XMLDAO<Client> dao = new XMLDAO<>(new File("xmlstorage/clients.xml"));
		dao.save(DefaultData.getClients());
	}

	@Test
	void clientUnmarshallTest() {
		XMLDAO<Client> dao = new XMLDAO<>(
				new File("xmlstorage/clients.xml"),
				new File("src/repository/resources/xsd/clients.xsd")
		);
		assertEquals(DefaultData.getClients(), dao.read());
	}

	@Test
	void adminsMarshallTest() {
		XMLDAO<Administrator> dao = new XMLDAO<>(new File("xmlstorage/admins.xml"));
		dao.save(DefaultData.getAdministrators());
	}

	@Test
	void adminsUnmarshallTest() {
		XMLDAO<Administrator> dao = new XMLDAO<>(
				new File("xmlstorage/admins.xml"),
				new File("src/repository/resources/xsd/admins.xsd")
		);
		assertEquals(DefaultData.getAdministrators(), dao.read());
	}

	@Test
	void roomTypesMarshallTest() {
		XMLDAO<RoomType> dao = new XMLDAO<>(new File("xmlstorage/roomTypes.xml"));
		dao.save(DefaultData.getRoomTypes());
	}

	@Test
	void roomTypesUnmarshallTest() {
		XMLDAO<RoomType> dao = new XMLDAO<>(
				new File("xmlstorage/roomTypes.xml"),
				new File("src/repository/resources/xsd/roomTypes.xsd")
		);
		assertEquals(DefaultData.getRoomTypes(), dao.read());
	}

	@Test
	void requestsMarshallTest() {
		XMLDAO<Request> dao = new XMLDAO<>(new File("xmlstorage/requests.xml"));
		dao.save(DefaultData.getRequests());
	}

	@Test
	void requestsUnmarshallTest() {
		XMLDAO<Request> dao = new XMLDAO<>(
				new File("xmlstorage/requests.xml"),
				new File("src/repository/resources/xsd/requests.xsd")
		);
		assertEquals(DefaultData.getRequests(), dao.read());
	}

	@Test
	void rejectedRequestsMarshallTest() {
		XMLDAO<RejectedRequest> dao = new XMLDAO<>(new File("xmlstorage/rejectedRequests.xml"));
		dao.save(DefaultData.getRejectedRequests());
	}

	@Test
	void rejectedRequestsUnmarshallTest() {
		XMLDAO<RejectedRequest> dao = new XMLDAO<>(
				new File("xmlstorage/rejectedRequests.xml"),
				new File("src/repository/resources/xsd/rejectedRequests.xsd")
		);
		assertEquals(DefaultData.getRejectedRequests(), dao.read());
	}

	@Test
	void confirmedRequestsMarshallTest() {
		XMLDAO<ConfirmedRequest> dao = new XMLDAO<>(new File("xmlstorage/confirmedRequests.xml"));
		dao.save(DefaultData.getConfirmedRequests());
	}

	@Test
	void confirmedRequestsUnmarshallTest() {
		XMLDAO<ConfirmedRequest> dao = new XMLDAO<>(
				new File("xmlstorage/confirmedRequests.xml"),
				new File("src/repository/resources/xsd/confirmedRequests.xsd")
		);
		assertEquals(DefaultData.getConfirmedRequests(), dao.read());
	}
}
