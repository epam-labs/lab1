package com.gitlab.andrewkuryan.vt1.repository;

import com.gitlab.andrewkuryan.vt1.DAO;
import com.gitlab.andrewkuryan.vt1.entity.user.Client;
import com.gitlab.andrewkuryan.vt1.mysqlrepository.MySqlClientDAO;
import com.gitlab.andrewkuryan.vt1.mysqlrepository.MySqlConfigurationManager;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class MySQLTests {

	@Test
	void userSaveTest() {
		List<Client> userList = new ArrayList<>();
		Client user1 = new Client("login1", "token1",
				"abc@abc.abc", "abc", "abc");
		Client user2 = new Client("login2", "token2",
				"def@def.def", "def", "def");
		userList.add(user1);
		userList.add(user2);
		DAO<Client> dao = new MySqlClientDAO(MySqlConfigurationManager.getInstance());
		dao.save(userList);
	}

	@Test
	void userReadTest() {
		/*List<User> userList = new ArrayList<>();
		User user1 = new User("login1", "token1");
		User user2 = new User("login2", "token2");
		User user3 = new User("login3", "token3");
		userList.add(user1);
		userList.add(user2);
		userList.add(user3);
		DAO<User> dao = new MySqlUserDAO();
		Iterable<User> userList1 = dao.read();
		Assertions.assertIterableEquals(userList, userList1);*/
	}
}

