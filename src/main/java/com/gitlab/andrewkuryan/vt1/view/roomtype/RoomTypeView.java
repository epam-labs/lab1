package com.gitlab.andrewkuryan.vt1.view.roomtype;

import com.gitlab.andrewkuryan.vt1.view.utils.View;
import com.gitlab.andrewkuryan.vt1.view.utils.ViewLoader;
import com.gitlab.andrewkuryan.vt1.viewmodel.roomtype.port.RoomTypeViewState;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class RoomTypeView extends View implements Initializable {

	@FXML
	private RoomTypeViewState viewModel;
	private ListView<AnchorPane> listView;

	@FXML
	private HBox roomTypeRoot;
	@FXML
	private Spinner<Integer> recordsPerPage;
	@FXML
	private Pagination pagination;
	@FXML
	private ScrollPane filterWrapper;

	private RoomTypeFilterView filterView;

	public static RoomTypeView newInstance(RoomTypeViewState viewModel) {
		RoomTypeView view = new RoomTypeView(viewModel);
		ViewLoader.loadView(view, viewModel, RoomTypeViewState.class, "/layout/roomtype/roomtype.fxml");
		return view;
	}

	private RoomTypeView(RoomTypeViewState viewModel) {
		this.viewModel = viewModel;
		filterView = RoomTypeFilterView.newInstance(viewModel);

		listView = new ListView<>();
		this.viewModel.getActualRoomTypes().addListener((observable, oldValue, newValue) ->
				listView.setItems(
						newValue.stream()
								.map(RoomTypeItemView::new)
								.collect(Collectors.toCollection(FXCollections::observableArrayList))
				)
		);
	}

	@Override
	public HBox getRoot() {
		return roomTypeRoot;
	}

	@Override
	public void onMount() {
		viewModel.getRecordsOnPage(viewModel.getPage());
	}

	@FXML
	private void onRefreshClick() {
		viewModel.refresh();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		roomTypeRoot.getStylesheets().add("/styles/roomtype.css");

		filterWrapper.setContent(filterView.getRoot());

		recordsPerPage.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(
				1, 9, viewModel.getRecordsPerPage()
		));
		recordsPerPage.valueProperty().addListener((observable, oldValue, newValue) ->
				viewModel.recordsPerPageProperty().set(newValue)
		);

		viewModel.pageProperty().addListener(((observable, oldValue, newValue) ->
			pagination.setCurrentPageIndex(newValue.intValue() - 1)
		));
		pagination.currentPageIndexProperty().addListener(((observable, oldValue, newValue) ->
			viewModel.getRecordsOnPage(newValue.intValue() + 1)
		));
		pagination.setPageFactory((Integer index) -> listView);
	}
}
