package com.gitlab.andrewkuryan.vt1.view.requests;

import com.gitlab.andrewkuryan.vt1.view.utils.ViewLoader;
import com.gitlab.andrewkuryan.vt1.viewmodel.requests.port.RequestsViewStateFilter;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.ResourceBundle;

public class RequestsFilterView implements Initializable {

	@FXML
	private RequestsViewStateFilter viewModel;

	@FXML
	private VBox requestsFilterRoot;
	@FXML
	private CheckBox isConfirmed;
	@FXML
	private CheckBox isRejected;
	@FXML
	private TextField roomTypeName;
	@FXML
	private TextField customerLogin;
	@FXML
	private TextField numOfPersonsFrom;
	@FXML
	private TextField numOfPersonsTo;
	@FXML
	private DatePicker arrivalDateFrom;
	@FXML
	private DatePicker arrivalDateTo;
	@FXML
	private DatePicker departureDateFrom;
	@FXML
	private DatePicker departureDateTo;

	static RequestsFilterView newInstance(RequestsViewStateFilter viewModel) {
		RequestsFilterView view = new RequestsFilterView(viewModel);
		ViewLoader.loadView(view, viewModel, RequestsViewStateFilter.class, "/layout/requests/requestsfilter.fxml");
		return view;
	}

	private RequestsFilterView(RequestsViewStateFilter viewModel) {
		this.viewModel = viewModel;
	}

	public VBox getRoot() {
		return requestsFilterRoot;
	}

	@FXML
	private void onApplyFilter() {
		viewModel.applyFilter();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		isConfirmed.selectedProperty().bindBidirectional(viewModel.confirmedProperty());
		isRejected.selectedProperty().bindBidirectional(viewModel.rejectedProperty());

		roomTypeName.textProperty().bindBidirectional(viewModel.roomTypeNameProperty());
		customerLogin.textProperty().bindBidirectional(viewModel.customerLoginProperty());
		numOfPersonsFrom.textProperty().bindBidirectional(viewModel.numOfPersonsFromProperty());
		numOfPersonsTo.textProperty().bindBidirectional(viewModel.numOfPersonsToProperty());

		arrivalDateFrom.valueProperty().bindBidirectional(viewModel.arrivalDateFromProperty());
		arrivalDateTo.valueProperty().bindBidirectional(viewModel.arrivalDateToProperty());
		departureDateFrom.valueProperty().bindBidirectional(viewModel.departureDateFromProperty());
		departureDateTo.valueProperty().bindBidirectional(viewModel.departureDateToProperty());
	}
}
