package com.gitlab.andrewkuryan.vt1.view.utils;

import com.gitlab.andrewkuryan.vt1.viewmodel.port.LateinitView;
import com.gitlab.andrewkuryan.vt1.viewmodel.port.NavigationManager;
import javafx.scene.layout.Pane;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FXNavigationManager implements NavigationManager {

	private Pane root;
	private List<String> history;
	private int currentRoutePos;
	private Map<String, View> routes;
	private Map<String, LateinitView<? extends View>> lateinitRoutes;

	public FXNavigationManager(Pane root) {
		this.root = root;
		routes = new HashMap<>();
		lateinitRoutes = new HashMap<>();
		history = new ArrayList<>();
		currentRoutePos = -1;
	}

	@Override
	public void addRoute(String path, View view) {
		routes.put(path, view);
	}

	@Override
	public void addLateinitRoute(String path, LateinitView<? extends View> view) {
		lateinitRoutes.put(path, view);
	}

	@Override
	public void navigateTo(String path) {
		if (currentRoutePos < 0 || !path.equals(history.get(currentRoutePos))) {
			System.out.println("Navigate to: " + path);
			history.add(path);
			currentRoutePos++;
			navigate(path);
		}
	}

	@Override
	public void navigateWithReplace(String path) {
		if (currentRoutePos < 0 || !path.equals(history.get(currentRoutePos))) {
			System.out.println("Navigate with replace: " + path);
			if (root.getChildren().size() > 0) {
				root.getChildren().remove(root.getChildren().size() - 1);
				history.set(history.size() - 1, path);
			} else {
				history.add(path);
			}
			navigate(path);
		}
	}

	@Override
	public void navigateToUpperScene() {
		currentRoutePos--;
		root.getChildren().remove(root.getChildren().size() - 1);
	}

	private void navigate(String path) {
		if (routes.get(path) != null) {
			root.getChildren().add(routes.get(path).getRoot());
			routes.get(path).onMount();
		} else if (lateinitRoutes.get(path) != null) {
			var view = lateinitRoutes.get(path).init();
			routes.put(path, view);
			root.getChildren().add(view.getRoot());
			view.onMount();
		}
	}
}
