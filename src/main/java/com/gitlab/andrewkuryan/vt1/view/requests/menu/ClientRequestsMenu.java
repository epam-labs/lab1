package com.gitlab.andrewkuryan.vt1.view.requests.menu;

import com.gitlab.andrewkuryan.vt1.entity.request.Request;
import com.gitlab.andrewkuryan.vt1.viewmodel.requests.port.ClientRequestsViewState;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;

public class ClientRequestsMenu extends ContextMenu {

	public ClientRequestsMenu(ClientRequestsViewState viewModel) {
		super();
		MenuItem itemCancel = new MenuItem("Cancel");
		itemCancel.setOnAction(e -> viewModel.cancelRequest((Request) getUserData()));

		getItems().addAll(itemCancel);
	}
}
