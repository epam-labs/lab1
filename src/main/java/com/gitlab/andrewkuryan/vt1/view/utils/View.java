package com.gitlab.andrewkuryan.vt1.view.utils;

import javafx.scene.Parent;

public abstract class View {

	public abstract Parent getRoot();

	public void onMount() { }
}
