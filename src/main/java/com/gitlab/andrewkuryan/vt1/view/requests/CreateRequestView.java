package com.gitlab.andrewkuryan.vt1.view.requests;

import com.gitlab.andrewkuryan.vt1.view.utils.View;
import com.gitlab.andrewkuryan.vt1.view.utils.ViewLoader;
import com.gitlab.andrewkuryan.vt1.viewmodel.requests.port.ClientRequestsViewState;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.util.ResourceBundle;

public class CreateRequestView extends View implements Initializable {

	@FXML
	private ClientRequestsViewState viewModel;

	@FXML
	private AnchorPane createRequestRoot;
	@FXML
	private TextField roomTypeName;
	@FXML
	private DatePicker arrivalDate;
	@FXML
	private DatePicker departureDate;
	@FXML
	private Spinner<Integer> numOfPersons;

	public static CreateRequestView newInstance(ClientRequestsViewState viewModel) {
		CreateRequestView view = new CreateRequestView(viewModel);
		ViewLoader.loadView(view, viewModel, ClientRequestsViewState.class, "/layout/requests/createrequest.fxml");
		return view;
	}

	private CreateRequestView(ClientRequestsViewState viewModel) {
		this.viewModel = viewModel;
	}

	@Override
	public Parent getRoot() {
		return createRequestRoot;
	}

	@Override
	public void onMount() {
		viewModel.checkPermissions();
		roomTypeName.setText(viewModel.getSelectedRoomTypeName());
	}

	@FXML
	private void onBackClick() {
		viewModel.goBack();
	}

	@FXML
	private void onCreateClick() {
		viewModel.createRequest();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		createRequestRoot.getStylesheets().add("/styles/createrequest.css");

		arrivalDate.valueProperty().bindBidirectional(viewModel.arrivalDateProperty());
		departureDate.valueProperty().bindBidirectional(viewModel.departureDateProperty());

		numOfPersons.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(
				1, 9, 1
		));
		numOfPersons.valueProperty().addListener((observable, oldValue, newValue) ->
				viewModel.numOfPersonsProperty().set(newValue)
		);
	}
}
