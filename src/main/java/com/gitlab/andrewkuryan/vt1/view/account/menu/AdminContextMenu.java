package com.gitlab.andrewkuryan.vt1.view.account.menu;

import com.gitlab.andrewkuryan.vt1.viewmodel.account.port.AdminViewState;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;

public class AdminContextMenu extends ContextMenu {

	public AdminContextMenu(AdminViewState viewModel) {
		super();
		MenuItem itemLogout = new MenuItem("Logout");
		itemLogout.setOnAction(event -> viewModel.logout());
		MenuItem itemProfile = new MenuItem("Profile");
		itemProfile.setOnAction(event -> viewModel.goToProfile());

		getItems().addAll(itemProfile, itemLogout);
	}
}
