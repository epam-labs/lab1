package com.gitlab.andrewkuryan.vt1.view.auth;

import com.gitlab.andrewkuryan.vt1.view.utils.View;
import com.gitlab.andrewkuryan.vt1.view.utils.ViewLoader;
import com.gitlab.andrewkuryan.vt1.viewmodel.auth.port.AuthViewState;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;

import java.net.URL;
import java.util.ResourceBundle;

public class AuthView extends View implements Initializable {

	@FXML
	private AuthViewState viewModel;

	@FXML
	private FlowPane authRoot;
	@FXML
	private AnchorPane authContent;
	@FXML
	private ToggleGroup modeGroup;
	@FXML
	private ToggleButton toggleSignIn;
	@FXML
	private ToggleButton toggleRegister;

	private LoginView loginView;
	private RegisterView registerView;

	public static AuthView newInstance(AuthViewState viewState, LoginView loginView, RegisterView registerView) {
		AuthView view = new AuthView(loginView, registerView);
		ViewLoader.loadView(view, viewState, AuthViewState.class, "/layout/auth.fxml");
		return view;
	}

	private AuthView(LoginView loginView, RegisterView registerView) {
		this.loginView = loginView;
		this.registerView = registerView;
	}

	@Override
	public FlowPane getRoot() {
		return authRoot;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		authRoot.getStylesheets().add("/styles/auth.css");

		if (viewModel.getRegisterSelected()) {
			authContent.getChildren().add(registerView.getRoot());
		} else if (viewModel.getSignInSelected()) {
			authContent.getChildren().add(loginView.getRoot());
		}

		toggleSignIn.selectedProperty().bindBidirectional(viewModel.signInSelectedProperty());
		toggleRegister.selectedProperty().bindBidirectional(viewModel.registerSelectedProperty());

		modeGroup.selectedToggleProperty().addListener(((observable, oldValue, newValue) -> {
			if (newValue != null) {
				authContent.getChildren().clear();
				switch ((String) newValue.getUserData()) {
					case "signIn":
						authContent.getChildren().add(loginView.getRoot());
						break;
					case "register":
						authContent.getChildren().add(registerView.getRoot());
						break;
				}
			}
		}));
	}
}
