package com.gitlab.andrewkuryan.vt1.view.main;

import com.gitlab.andrewkuryan.vt1.viewmodel.main.port.LeftNavigationViewState;
import com.gitlab.andrewkuryan.vt1.viewmodel.port.ConfigurableView;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

import java.util.LinkedHashMap;
import java.util.Map;

public class LeftNavigationView extends VBox implements ConfigurableView {

	private Map<String, LinkedHashMap<String, String>> configs;
	private LeftNavigationViewState viewModel;

	public LeftNavigationView(LeftNavigationViewState viewModel, String defConfig, Map<String, LinkedHashMap<String, String>> configs) {
		super();
		this.configs = configs;
		this.viewModel = viewModel;
		setId("leftNavigation");

		renderButtons(defConfig);
	}

	@Override
	public void configure(String configName) {
		getChildren().clear();
		renderButtons(configName);
	}

	private void renderButtons(String config) {
		ToggleGroup navGroup = new ToggleGroup();
		configs.get(config).forEach((name, path) -> {
			ToggleButton button = new ToggleButton();
			button.setToggleGroup(navGroup);
			Label  label  = new Label(name);
			label.setRotate(-90);
			button.setGraphic(new Group(label));
			button.setOnAction(event -> viewModel.navigateTo(path));

			AnchorPane wrapper = new AnchorPane();
			wrapper.getChildren().add(button);
			VBox.setVgrow(wrapper, Priority.ALWAYS);

			AnchorPane.setTopAnchor(button, 0.0);
			AnchorPane.setLeftAnchor(button, 0.0);
			AnchorPane.setBottomAnchor(button, 0.0);
			AnchorPane.setRightAnchor(button, 0.0);

			getChildren().add(wrapper);
		});
	}
}
