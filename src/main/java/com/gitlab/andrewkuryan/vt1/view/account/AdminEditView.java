package com.gitlab.andrewkuryan.vt1.view.account;

import com.gitlab.andrewkuryan.vt1.view.utils.View;
import com.gitlab.andrewkuryan.vt1.view.utils.ViewLoader;
import com.gitlab.andrewkuryan.vt1.viewmodel.account.port.AdminViewState;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.util.ResourceBundle;

public class AdminEditView extends View implements Initializable {

	@FXML
	private AdminViewState viewModel;

	@FXML
	private AnchorPane adminEditRoot;
	@FXML
	private TextField login;
	@FXML
	private TextField oldPassword;
	@FXML
	private TextField password;
	@FXML
	private TextField firstName;
	@FXML
	private TextField lastName;

	public static AdminEditView newInstance(AdminViewState viewModel) {
		AdminEditView view = new AdminEditView(viewModel);
		ViewLoader.loadView(view, viewModel, AdminViewState.class, "/layout/profile/adminedit.fxml");
		return view;
	}

	private AdminEditView(AdminViewState viewModel) {
		this.viewModel = viewModel;
	}

	@Override
	public Parent getRoot() {
		return adminEditRoot;
	}

	@FXML
	private void onBackClick() {
		viewModel.goBack();
	}

	@FXML
	private void onSaveClick() {
		viewModel.oldPasswordProperty().set(oldPassword.getText());
		viewModel.passwordProperty().set(password.getText());
		viewModel.firstNameProperty().set(firstName.getText());
		viewModel.lastNameProperty().set(lastName.getText());
		viewModel.update();
	}

	@FXML
	private void onResetClick() {
		oldPassword.setText("");
		password.setText("");
		firstName.setText(viewModel.getFirstName());
		lastName.setText(viewModel.getLastName());
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		adminEditRoot.getStylesheets().add("/styles/clientedit.css");

		login.textProperty().bindBidirectional(viewModel.loginProperty());
		onResetClick();
	}
}
