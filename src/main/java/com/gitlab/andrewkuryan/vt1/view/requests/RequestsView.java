package com.gitlab.andrewkuryan.vt1.view.requests;

import com.gitlab.andrewkuryan.vt1.view.utils.View;
import com.gitlab.andrewkuryan.vt1.view.utils.ViewLoader;
import com.gitlab.andrewkuryan.vt1.viewmodel.requests.port.RequestsViewState;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class RequestsView extends View implements Initializable {

	@FXML
	private RequestsViewState viewModel;
	private ListView<AnchorPane> listView;
	private RequestsFilterView filterView;

	private ContextMenu itemMenu;

	@FXML
	private HBox requestsViewRoot;
	@FXML
	private Spinner<Integer> recordsPerPage;
	@FXML
	private Pagination pagination;
	@FXML
	private ScrollPane filterWrapper;

	public static RequestsView newInstance(RequestsViewState viewModel, ContextMenu itemMenu) {
		RequestsView view = new RequestsView(viewModel, itemMenu);
		ViewLoader.loadView(view, viewModel, RequestsViewState.class, "/layout/requests/requests.fxml");
		return view;
	}

	private RequestsView(RequestsViewState viewModel, ContextMenu itemMenu) {
		this.viewModel = viewModel;
		this.itemMenu = itemMenu;
		filterView = RequestsFilterView.newInstance(viewModel);

		listView = new ListView<>();
		this.viewModel.getActualRequests().addListener((observable, oldValue, newValue) ->
				listView.setItems(
						newValue.stream()
								.map(request -> new RequestItemView(request, itemMenu))
								.collect(Collectors.toCollection(FXCollections::observableArrayList))
				)
		);
	}

	@Override
	public Parent getRoot() {
		return requestsViewRoot;
	}

	@Override
	public void onMount() {
		viewModel.getRecordsOnPage(viewModel.getPage());
	}

	@FXML
	private void onRefreshClick() {
		viewModel.refresh();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		requestsViewRoot.getStylesheets().add("/styles/roomtype.css");
		filterWrapper.setContent(filterView.getRoot());

		recordsPerPage.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(
				1, 9, viewModel.getRecordsPerPage()
		));
		recordsPerPage.valueProperty().addListener((observable, oldValue, newValue) ->
				viewModel.recordsPerPageProperty().set(newValue)
		);

		viewModel.pageProperty().addListener(((observable, oldValue, newValue) ->
				pagination.setCurrentPageIndex(newValue.intValue() - 1)
		));
		pagination.currentPageIndexProperty().addListener(((observable, oldValue, newValue) ->
				viewModel.getRecordsOnPage(newValue.intValue() + 1)
		));
		pagination.setPageFactory((Integer index) -> listView);
	}
}
