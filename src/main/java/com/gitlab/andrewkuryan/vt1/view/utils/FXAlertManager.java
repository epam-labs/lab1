package com.gitlab.andrewkuryan.vt1.view.utils;

import com.gitlab.andrewkuryan.vt1.view.alert.ConfirmAlert;
import com.gitlab.andrewkuryan.vt1.view.alert.InputAlert;
import com.gitlab.andrewkuryan.vt1.viewmodel.port.AlertManager;
import javafx.scene.layout.Pane;

import java.util.function.Consumer;

public class FXAlertManager implements AlertManager {

	private Pane root;

	public FXAlertManager(Pane root) {
		this.root = root;
	}

	@Override
	public void showConfirmAlert(String confirmText, Consumer<Void> onConfirm) {
		root.getChildren().add(ConfirmAlert.newInstance(confirmText, v -> {
			onConfirm.accept(v);
			root.getChildren().remove(root.getChildren().size() - 1);
		}).getRoot());
	}

	@Override
	public void showTextInputAlert(String text, Consumer<String> onConfirm) {
		root.getChildren().add(InputAlert.newInstance(text, s -> {
			onConfirm.accept(s);
			root.getChildren().remove(root.getChildren().size() - 1);
		}).getRoot());
	}
}
