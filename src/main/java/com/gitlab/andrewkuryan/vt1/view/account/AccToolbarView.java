package com.gitlab.andrewkuryan.vt1.view.account;

import com.gitlab.andrewkuryan.vt1.view.utils.ViewLoader;
import com.gitlab.andrewkuryan.vt1.viewmodel.account.port.AccountViewState;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.util.ResourceBundle;

public class AccToolbarView implements Initializable {

	@FXML
	private AccountViewState viewModel;

	@FXML
	private AnchorPane accToolbarRoot;
	@FXML
	private Label username;
	private ContextMenu menu;

	public static AccToolbarView newInstance(AccountViewState viewState, ContextMenu menu) {
		AccToolbarView view = new AccToolbarView(menu);
		ViewLoader.loadView(view, viewState, AccountViewState.class, "/layout/acctoolbar.fxml");
		return view;
	}

	private AccToolbarView(ContextMenu menu) {
		this.menu = menu;
	}

	public AnchorPane getRoot() {
		return accToolbarRoot;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		accToolbarRoot.getStylesheets().add("/styles/acctoolbar.css");
		username.setContextMenu(menu);
	}
}
