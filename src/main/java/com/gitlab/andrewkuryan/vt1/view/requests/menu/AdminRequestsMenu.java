package com.gitlab.andrewkuryan.vt1.view.requests.menu;

import com.gitlab.andrewkuryan.vt1.entity.request.Request;
import com.gitlab.andrewkuryan.vt1.viewmodel.requests.port.AdminRequestsViewState;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;

public class AdminRequestsMenu extends ContextMenu {

	public AdminRequestsMenu(AdminRequestsViewState viewModel) {
		super();
		MenuItem itemConfirm = new MenuItem("Confirm");
		itemConfirm.setOnAction(e -> viewModel.confirmRequest((Request) getUserData()));
		MenuItem itemReject = new MenuItem("Reject");
		itemReject.setOnAction(e -> viewModel.rejectRequest((Request) getUserData()));

		getItems().addAll(itemConfirm, itemReject);
	}
}
