package com.gitlab.andrewkuryan.vt1.view.roomtype;

import com.gitlab.andrewkuryan.vt1.view.utils.ViewLoader;
import com.gitlab.andrewkuryan.vt1.viewmodel.roomtype.port.RoomTypeViewStateFilter;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.ResourceBundle;

public class RoomTypeFilterView implements Initializable {

	private RoomTypeViewStateFilter viewModel;

	@FXML
	private VBox roomTypeFilterRoot;
	@FXML
	private TextField typeName;
	@FXML
	private TextField numOfPlacesFrom;
	@FXML
	private TextField numOfPlacesTo;
	@FXML
	private TextField costFrom;
	@FXML
	private TextField costTo;
	@FXML
	private TextField areaFrom;
	@FXML
	private TextField areaTo;
	@FXML
	private VBox servicesBox;

	static RoomTypeFilterView newInstance(RoomTypeViewStateFilter viewModel) {
		RoomTypeFilterView view = new RoomTypeFilterView(viewModel);
		ViewLoader.loadView(view, viewModel, RoomTypeViewStateFilter.class, "/layout/roomtype/roomtypefilter.fxml");
		return view;
	}

	private RoomTypeFilterView(RoomTypeViewStateFilter viewModel) {
		this.viewModel = viewModel;
	}

	public VBox getRoot() {
		return roomTypeFilterRoot;
	}

	@FXML
	private void onApplyFilter() {
		viewModel.applyFilter();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		viewModel.getAvailableServices().addListener(((observable, oldValue, newValue) ->
				newValue.forEach(service -> {
					var checkBox = new CheckBox(service);
					checkBox.selectedProperty().addListener(((observable1, oldValue1, newValue1) ->
							viewModel.servicesProperty().get().replace(checkBox.getText(), newValue1)
					));
					servicesBox.getChildren().add(checkBox);
				}))
		);

		typeName.textProperty().bindBidirectional(viewModel.typeNameProperty());
		numOfPlacesFrom.textProperty().bindBidirectional(viewModel.numOfPlacesFromProperty());
		numOfPlacesTo.textProperty().bindBidirectional(viewModel.numOfPlacesToProperty());
		costFrom.textProperty().bindBidirectional(viewModel.costFromProperty());
		costTo.textProperty().bindBidirectional(viewModel.costToProperty());
		areaFrom.textProperty().bindBidirectional(viewModel.areaFromProperty());
		areaTo.textProperty().bindBidirectional(viewModel.areaToProperty());
	}
}
