package com.gitlab.andrewkuryan.vt1.view.account;

import com.gitlab.andrewkuryan.vt1.view.utils.View;
import com.gitlab.andrewkuryan.vt1.view.utils.ViewLoader;
import com.gitlab.andrewkuryan.vt1.viewmodel.account.port.ClientViewState;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.util.ResourceBundle;

public class ClientEditView extends View implements Initializable {

	@FXML
	private ClientViewState viewModel;

	@FXML
	private AnchorPane clientEditRoot;

	@FXML
	private TextField login;
	@FXML
	private TextField oldPassword;
	@FXML
	private TextField password;
	@FXML
	private TextField firstName;
	@FXML
	private TextField lastName;
	@FXML
	private TextField email;
	@FXML
	private TextField phoneNumber;

	public static ClientEditView newInstance(ClientViewState viewModel) {
		ClientEditView view = new ClientEditView(viewModel);
		ViewLoader.loadView(view, viewModel, ClientViewState.class, "/layout/profile/clientedit.fxml");
		return view;
	}

	private ClientEditView(ClientViewState viewModel) {
		this.viewModel = viewModel;
	}

	@Override
	public Parent getRoot() {
		return clientEditRoot;
	}

	@FXML
	private void onBackClick() {
		viewModel.goBack();
	}

	@FXML
	private void onSaveClick() {
		viewModel.oldPasswordProperty().set(oldPassword.getText());
		viewModel.passwordProperty().set(password.getText());
		viewModel.firstNameProperty().set(firstName.getText());
		viewModel.lastNameProperty().set(lastName.getText());
		viewModel.emailProperty().set(email.getText());
		viewModel.phoneNumberProperty().set(phoneNumber.getText());
		viewModel.update();
	}

	@FXML
	private void onResetClick() {
		oldPassword.setText("");
		password.setText("");
		firstName.setText(viewModel.getFirstName());
		lastName.setText(viewModel.getLastName());
		email.setText(viewModel.getEmail());
		phoneNumber.setText(viewModel.getPhoneNumber());
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		clientEditRoot.getStylesheets().add("/styles/clientedit.css");

		login.textProperty().bindBidirectional(viewModel.loginProperty());
		onResetClick();
	}
}
