package com.gitlab.andrewkuryan.vt1.view.auth;

import com.gitlab.andrewkuryan.vt1.view.utils.ViewLoader;
import com.gitlab.andrewkuryan.vt1.viewmodel.auth.port.RegisterViewState;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;

import java.net.URL;
import java.util.ResourceBundle;

public class RegisterView implements Initializable {

	@FXML
	private RegisterViewState viewModel;

	@FXML
	private FlowPane registerRoot;
	@FXML
	private TextField login;
	@FXML
	private TextField password;
	@FXML
	private TextField repeatedPassword;
	@FXML
	private TextField email;
	@FXML
	private TextField firstName;
	@FXML
	private TextField lastName;
	@FXML
	private TextField phoneNumber;

	public static RegisterView newInstance(RegisterViewState viewState) {
		RegisterView view = new RegisterView();
		ViewLoader.loadView(view, viewState, RegisterViewState.class, "/layout/register.fxml");
		return view;
	}

	private RegisterView() {

	}

	FlowPane getRoot() {
		return registerRoot;
	}

	@FXML
	private void onRegisterClick() {
		viewModel.register();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		registerRoot.getStylesheets().add("/styles/register.css");

		login.textProperty().bindBidirectional(viewModel.loginProperty());
		password.textProperty().bindBidirectional(viewModel.passwordProperty());
		repeatedPassword.textProperty().bindBidirectional(viewModel.repeatedPasswordProperty());
		email.textProperty().bindBidirectional(viewModel.emailProperty());
		firstName.textProperty().bindBidirectional(viewModel.firstNameProperty());
		lastName.textProperty().bindBidirectional(viewModel.lastNameProperty());
		phoneNumber.textProperty().bindBidirectional(viewModel.phoneNumberProperty());
	}
}
