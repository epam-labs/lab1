package com.gitlab.andrewkuryan.vt1.view.auth;

import com.gitlab.andrewkuryan.vt1.view.utils.ViewLoader;
import com.gitlab.andrewkuryan.vt1.viewmodel.auth.port.LoginViewState;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.ResourceBundle;

public class LoginView implements Initializable {

	@FXML
	private LoginViewState viewModel;

	@FXML
	private VBox loginRoot;
	@FXML
	private TextField login;
	@FXML
	private TextField password;

	public static LoginView newInstance(LoginViewState viewState) {
		LoginView view = new LoginView();
		ViewLoader.loadView(view, viewState, LoginViewState.class, "/layout/login.fxml");
		return view;
	}

	private LoginView() {

	}

	VBox getRoot() {
		return loginRoot;
	}

	@FXML
	private void onSignInClick() {
		viewModel.login();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		loginRoot.getStylesheets().add("/styles/login.css");

		login.textProperty().bindBidirectional(viewModel.loginProperty());
		password.textProperty().bindBidirectional(viewModel.passwordProperty());
	}
}
