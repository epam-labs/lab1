package com.gitlab.andrewkuryan.vt1.view.roomtype;

import com.gitlab.andrewkuryan.vt1.view.utils.View;
import com.gitlab.andrewkuryan.vt1.view.utils.ViewLoader;
import com.gitlab.andrewkuryan.vt1.viewmodel.availableroomtype.port.AvailableRoomsViewState;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class AvailableRoomsView extends View implements Initializable {

	@FXML
	private AvailableRoomsViewState viewModel;
	private ListView<AnchorPane> listView;
	private AvailableRoomsFilterView filterView;

	private ContextMenu itemMenu;

	@FXML
	private HBox availableRoomsRoot;
	@FXML
	private Spinner<Integer> recordsPerPage;
	@FXML
	private Pagination pagination;
	@FXML
	private ScrollPane filterWrapper;

	public static AvailableRoomsView newInstance(AvailableRoomsViewState viewModel) {
		AvailableRoomsView view = new AvailableRoomsView(viewModel);
		ViewLoader.loadView(view, viewModel, AvailableRoomsViewState.class, "/layout/roomtype/availablerooms.fxml");
		return view;
	}

	private AvailableRoomsView(AvailableRoomsViewState viewModel) {
		this.viewModel = viewModel;
		filterView = AvailableRoomsFilterView.newInstance(viewModel);

		itemMenu = new ContextMenu();
		MenuItem itemCreateReq = new MenuItem("Create Request");
		itemCreateReq.setOnAction(e -> viewModel.createRequest());
		itemMenu.getItems().addAll(itemCreateReq);

		listView = new ListView<>();
		listView.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
				if (newValue.intValue() >= 0) {
					System.out.println("New value: " + newValue.intValue());
					this.viewModel.selectedRoomTypeProperty().set(
							this.viewModel.getActualRoomTypes().get().get(newValue.intValue())
					);
				}
		});
		this.viewModel.getActualRoomTypes().addListener((observable, oldValue, newValue) ->
				listView.setItems(
						newValue.stream()
								.map(rt -> new AvailableRoomsItemView(rt, itemMenu))
								.collect(Collectors.toCollection(FXCollections::observableArrayList))
				)
		);
	}

	@Override
	public HBox getRoot() {
		return availableRoomsRoot;
	}

	@Override
	public void onMount() {
		viewModel.getRecordsOnPage(viewModel.getPage());
	}

	@FXML
	private void onRefreshClick() {
		viewModel.refresh();
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		availableRoomsRoot.getStylesheets().add("/styles/roomtype.css");

		filterWrapper.setContent(filterView.getRoot());

		recordsPerPage.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(
				1, 9, viewModel.getRecordsPerPage()
		));
		recordsPerPage.valueProperty().addListener((observable, oldValue, newValue) ->
				viewModel.recordsPerPageProperty().set(newValue)
		);

		viewModel.pageProperty().addListener(((observable, oldValue, newValue) ->
				pagination.setCurrentPageIndex(newValue.intValue() - 1)
		));
		pagination.currentPageIndexProperty().addListener(((observable, oldValue, newValue) ->
				viewModel.getRecordsOnPage(newValue.intValue() + 1)
		));
		pagination.setPageFactory((Integer index) -> listView);
	}
}
