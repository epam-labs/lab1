package com.gitlab.andrewkuryan.vt1.view.requests;

import com.gitlab.andrewkuryan.vt1.entity.request.ConfirmedRequest;
import com.gitlab.andrewkuryan.vt1.entity.request.RejectedRequest;
import com.gitlab.andrewkuryan.vt1.entity.request.Request;
import com.gitlab.andrewkuryan.vt1.view.utils.AbstractItemView;
import javafx.scene.control.ContextMenu;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Map;

class RequestItemView extends AbstractItemView<Request> {

	private static DateFormat dateFormat;

	static {
		dateFormat = new SimpleDateFormat("dd.MM.yyyy");
	}

	@Override
	public Map<String, String> calcValues(Request request) {
		String status = "In processing";
		String additional = "";
		if (request instanceof ConfirmedRequest) {
			status = "Confirmed";
			additional = String.format("Room: %d", ((ConfirmedRequest) request).getRoomNumber());
		} else if (request instanceof RejectedRequest) {
			status = "Rejected";
			additional = String.format("Reason: %s", ((RejectedRequest) request).getComment());
		}
		return Map.of(
				"roomTypeName", request.getRoomType().getTypeName(),
				"arrivalDate", dateFormat.format(request.getArrivalDate()),
				"departureDate", dateFormat.format(request.getDepartureDate()),
				"numOfPersons", Integer.toString(request.getNumberOfPersons()),
				"comment", status,
				"additionalInfo", additional
		);
	}

	@Override
	public String getLayoutPath() {
		return "/layout/requests/requestsitem.fxml";
	}

	@Override
	public String getStylesheetPath() {
		return "/styles/requestitem.css";
	}

	RequestItemView(Request request, ContextMenu menu) {
		super(request);
		menu.setUserData(request);
		setOnContextMenuRequested((event -> menu.show(this, event.getScreenX(), event.getScreenY())));
	}
}
