package com.gitlab.andrewkuryan.vt1.view.utils;

import javafx.fxml.FXMLLoader;
import javafx.fxml.JavaFXBuilderFactory;
import javafx.scene.Parent;

import java.util.Optional;

public class ViewLoader {

	public static <V> Optional<Parent> loadView(Object view, V viewModel, Class<V> viewStateType, String fxml) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setControllerFactory(param -> view);
			loader.setBuilderFactory(type -> {
				if (type.equals(viewStateType)) {
					return () -> viewModel;
				} else {
					return new JavaFXBuilderFactory().getBuilder(type);
				}
			});
			loader.setLocation(ViewLoader.class.getResource(fxml));
			loader.load();
			return Optional.of(loader.getRoot());
		} catch (Exception exc) {
			return Optional.empty();
		}
	}
}
