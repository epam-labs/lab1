package com.gitlab.andrewkuryan.vt1.view.utils;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.AnchorPane;

import java.util.Collections;
import java.util.Enumeration;
import java.util.Map;
import java.util.ResourceBundle;

public abstract class AbstractItemView<T> extends AnchorPane {

	public abstract Map<String, String> calcValues(T object);

	public abstract String getLayoutPath();

	public abstract String getStylesheetPath();

	public AbstractItemView(T object) {
		super();
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource(getLayoutPath()));
			Map<String, String> values = calcValues(object);
			loader.setResources(new ResourceBundle() {
				@Override
				protected Object handleGetObject(String key) {
					return values.get(key);
				}

				@Override
				public Enumeration<String> getKeys() {
					return Collections.enumeration(values.keySet());
				}
			});
			loader.load();

			Parent root = loader.getRoot();
			root.getStylesheets().add(getStylesheetPath());

			AnchorPane.setTopAnchor(root, 0.0);
			AnchorPane.setBottomAnchor(root, 0.0);
			AnchorPane.setLeftAnchor(root, 0.0);
			AnchorPane.setRightAnchor(root, 0.0);
			getChildren().add(root);
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}
}
