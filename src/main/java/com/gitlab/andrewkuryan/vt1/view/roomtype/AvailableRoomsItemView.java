package com.gitlab.andrewkuryan.vt1.view.roomtype;

import com.gitlab.andrewkuryan.vt1.entity.roomtype.AvailableRoomTypes;
import com.gitlab.andrewkuryan.vt1.view.utils.AbstractItemView;
import javafx.scene.control.ContextMenu;

import java.util.Map;

class AvailableRoomsItemView extends AbstractItemView<AvailableRoomTypes> {

	@Override
	public Map<String, String> calcValues(AvailableRoomTypes roomType) {
		return Map.of(
				"typeName", roomType.getTypeName(),
				"numOfPlaces", Integer.toString(roomType.getNumOfPlaces()),
				"cost", Integer.toString(roomType.getCost()),
				"smallImage", roomType.getSmallImage().isPresent() ?
						roomType.getSmallImage().get().toString() :
						getClass().getResource("/images/noRoomType.png").toString(),
				"availablePlaces", Integer.toString((roomType.getAvailableRooms())
		));
	}

	@Override
	public String getLayoutPath() {
		return "/layout/roomtype/availableroomtypeitem.fxml";
	}

	@Override
	public String getStylesheetPath() {
		return "/styles/availableroomtypeitem.css";
	}

	AvailableRoomsItemView(AvailableRoomTypes availableRoomTypes, ContextMenu contextMenu) {
		super(availableRoomTypes);
		setOnContextMenuRequested(e -> contextMenu.show(this, e.getScreenX(), e.getScreenY()));
	}
}
