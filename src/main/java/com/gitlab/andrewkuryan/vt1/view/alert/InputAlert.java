package com.gitlab.andrewkuryan.vt1.view.alert;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.function.Consumer;

public class InputAlert implements Initializable {

	private Consumer<String> onConfirm;
	private String titleText;

	@FXML
	private FlowPane alertMask;
	@FXML
	private Label title;
	@FXML
	private TextField input;
	@FXML
	private Button okButton;

	public static InputAlert newInstance(String title, Consumer<String> onConfirm) {
		InputAlert alert = new InputAlert(title, onConfirm);
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setControllerFactory(param -> alert);
			loader.setLocation(InputAlert.class.getResource("/layout/alerts/inputalert.fxml"));
			loader.load();
		} catch (Exception ignored) {}
		return alert;
	}

	private InputAlert(String title, Consumer<String> onConfirm) {
		this.onConfirm = onConfirm;
		titleText = title;
	}

	public FlowPane getRoot() {
		return alertMask;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		alertMask.getStylesheets().add("/styles/alert.css");

		title.setText(titleText);
		okButton.setOnAction(e -> onConfirm.accept(input.getText()));
	}
}
