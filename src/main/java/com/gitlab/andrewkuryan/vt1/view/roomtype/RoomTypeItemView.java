package com.gitlab.andrewkuryan.vt1.view.roomtype;

import com.gitlab.andrewkuryan.vt1.entity.roomtype.RoomType;
import com.gitlab.andrewkuryan.vt1.view.utils.AbstractItemView;

import java.util.Map;

class RoomTypeItemView extends AbstractItemView<RoomType> {

	@Override
	public Map<String, String> calcValues(RoomType roomType) {
		return Map.of(
				"typeName", roomType.getTypeName(),
				"numOfPlaces", Integer.toString(roomType.getNumOfPlaces()),
				"cost", Integer.toString(roomType.getCost()),
				"smallImage", roomType.getSmallImage().isPresent() ?
						roomType.getSmallImage().get().toString() :
						getClass().getResource("/images/noRoomType.png").toString()
		);
	}

	@Override
	public String getLayoutPath() {
		return "/layout/roomtype/roomtypeitem.fxml";
	}

	@Override
	public String getStylesheetPath() {
		return "/styles/roomtypeitem.css";
	}

	RoomTypeItemView(RoomType roomType) {
		super(roomType);
	}
}
