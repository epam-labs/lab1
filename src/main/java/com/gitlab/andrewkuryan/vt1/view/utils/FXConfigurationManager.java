package com.gitlab.andrewkuryan.vt1.view.utils;

import com.gitlab.andrewkuryan.vt1.viewmodel.port.ConfigurableView;
import com.gitlab.andrewkuryan.vt1.viewmodel.port.ConfigurationManager;

import java.util.List;

public class FXConfigurationManager implements ConfigurationManager {

	private List<ConfigurableView> views;

	public FXConfigurationManager(List<ConfigurableView> views) {
		this.views = views;
	}

	@Override
	public void addConfigurableView(ConfigurableView view) {
		views.add(view);
	}

	@Override
	public void configure(String newConfig) {
		views.forEach(configurableView -> configurableView.configure(newConfig));
	}
}
