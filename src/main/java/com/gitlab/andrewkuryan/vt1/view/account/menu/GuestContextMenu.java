package com.gitlab.andrewkuryan.vt1.view.account.menu;

import com.gitlab.andrewkuryan.vt1.viewmodel.account.port.GuestViewState;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;

public class GuestContextMenu extends ContextMenu {

	public GuestContextMenu(GuestViewState viewModel) {
		super();
		MenuItem itemSignIn = new MenuItem("Sign in");
		itemSignIn.setOnAction(event -> viewModel.signIn());

		getItems().add(itemSignIn);
	}
}
