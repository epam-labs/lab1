package com.gitlab.andrewkuryan.vt1.view.main;

import com.gitlab.andrewkuryan.vt1.view.account.AccToolbarView;
import com.gitlab.andrewkuryan.vt1.view.utils.View;
import com.gitlab.andrewkuryan.vt1.viewmodel.port.ConfigurableView;
import com.gitlab.andrewkuryan.vt1.viewmodel.port.LateinitView;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.layout.BorderPane;

import java.net.URL;
import java.util.Map;
import java.util.ResourceBundle;

public class MainView extends View implements Initializable, ConfigurableView {

	private Map<String, LateinitView<AccToolbarView>> availableToolbars;

	@FXML
	private BorderPane mainRoot;

	private AccToolbarView accToolbarView;
	private LeftNavigationView leftNavigationView;

	public static MainView newInstance(
			AccToolbarView accToolbarView,
			LeftNavigationView leftNavigationView,
			Map<String, LateinitView<AccToolbarView>> configs
	) {
		MainView view = new MainView(accToolbarView, leftNavigationView, configs);
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setControllerFactory(param -> view);
			loader.setLocation(MainView.class.getResource("/layout/main.fxml"));
			loader.load();
		} catch (Exception ignored) {}
		return view;
	}

	private MainView(
			AccToolbarView accToolbarView,
	        LeftNavigationView leftNavigationView,
	        Map<String, LateinitView<AccToolbarView>> configs
	) {
		availableToolbars = configs;
		this.accToolbarView = accToolbarView;
		this.leftNavigationView = leftNavigationView;
	}

	@Override
	public BorderPane getRoot() {
		return mainRoot;
	}

	@Override
	public void configure(String configName) {
		accToolbarView = availableToolbars.get(configName).init();
		mainRoot.setTop(accToolbarView.getRoot());
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		mainRoot.getStylesheets().add("/styles/main.css");

		mainRoot.setTop(accToolbarView.getRoot());
		mainRoot.setLeft(leftNavigationView);
	}
}
