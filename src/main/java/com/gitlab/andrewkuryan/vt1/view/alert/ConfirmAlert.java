package com.gitlab.andrewkuryan.vt1.view.alert;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.function.Consumer;

public class ConfirmAlert implements Initializable {

	private Consumer<Void> onConfirm;
	private String titleText;

	@FXML
	private FlowPane alertMask;
	@FXML
	private Label title;
	@FXML
	private Button okButton;

	public static ConfirmAlert newInstance(String title, Consumer<Void> onConfirm) {
		ConfirmAlert alert = new ConfirmAlert(title, onConfirm);
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setControllerFactory(param -> alert);
			loader.setLocation(ConfirmAlert.class.getResource("/layout/alerts/confirmalert.fxml"));
			loader.load();
		} catch (Exception ignored) {}
		return alert;
	}

	private ConfirmAlert(String title, Consumer<Void> onConfirm) {
		this.onConfirm = onConfirm;
		titleText = title;
	}

	public FlowPane getRoot() {
		return alertMask;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		alertMask.getStylesheets().add("/styles/alert.css");

		title.setText(titleText);
		okButton.setOnAction(e -> onConfirm.accept(null));
	}
}
