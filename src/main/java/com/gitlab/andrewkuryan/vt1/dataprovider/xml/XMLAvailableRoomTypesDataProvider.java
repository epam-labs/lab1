package com.gitlab.andrewkuryan.vt1.dataprovider.xml;

import com.gitlab.andrewkuryan.vt1.dataprovider.parsers.Parser;
import com.gitlab.andrewkuryan.vt1.entity.roomtype.AvailableRoomTypes;
import com.gitlab.andrewkuryan.vt1.entity.roomtype.RoomType;
import com.gitlab.andrewkuryan.vt1.entity.user.User;
import com.gitlab.andrewkuryan.vt1.service.port.roomtype.AvailableRoomTypesApi;
import com.gitlab.andrewkuryan.vt1.service.roomtype.AvailableRoomTypesFilter;
import com.gitlab.andrewkuryan.vt1.service.utils.Pagination;
import com.gitlab.andrewkuryan.vt1.utils.Either;
import com.gitlab.andrewkuryan.vt1.utils.Pair;
import com.gitlab.andrewkuryan.vt1.utils.Range;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("unchecked")
public class XMLAvailableRoomTypesDataProvider implements AvailableRoomTypesApi {

	private Path xmlPath;
	private Parser<RoomType> roomTypeParser;
	private XMLDataProvider dataProvider;

	public XMLAvailableRoomTypesDataProvider(Path xmlPath, Parser<RoomType> roomTypeParser, XMLDataProvider parent) {
		this.xmlPath = xmlPath;
		this.roomTypeParser = roomTypeParser;
		this.dataProvider = parent;
	}

	@Override
	public Either<Pair<List<AvailableRoomTypes>, Pagination>, Exception> getAvailableRoomTypes(User user, Pagination pagination) {
		Either<Pair<List<RoomType>, Pagination>, Exception> roomTypes = dataProvider.getRoomTypes(user, pagination);
		if (roomTypes.isLeft()) {
			List<RoomType> list = roomTypes.getLeft().getFirst();
			Pagination pagination1 = roomTypes.getLeft().getSecond();
			List<AvailableRoomTypes> actualList = list.stream()
					.map(rt -> new AvailableRoomTypes(
							rt, (int) rt.getRoomNumbers().count())
					)
					.collect(Collectors.toList());
			return Either.left(new Pair<>(actualList, pagination1));
		} else return Either.right(roomTypes.getRight());
	}

	@Override
	public Either<Pair<List<AvailableRoomTypes>, Pagination>, Exception> getAvailableRoomTypes(User user, Pagination pagination, AvailableRoomTypesFilter filter) {
		try (InputStream is = Files.newInputStream(xmlPath.resolve("roomTypes.xml"))) {
			List<RoomType> totalList = roomTypeParser.parse(is);
			List<String> filterServices = filter.getServices().collect(Collectors.toList());
			List<RoomType> filteredResult = totalList.stream()
					.filter(rt -> filter.getTypeName().isEmpty() || rt.getTypeName().equals(filter.getTypeName().get()))
					.filter(rt -> filter.getNumOfPlaces().contains(rt.getNumOfPlaces()))
					.filter(rt -> filter.getCost().contains(rt.getCost()))
					.filter(rt -> filter.getArea().contains(rt.getArea()))
					.filter(rt -> filterServices.isEmpty() || rt.getServices().collect(Collectors.toList()).containsAll(filterServices))
					.collect(Collectors.toList());

			List<AvailableRoomTypes> actualResult = filteredResult.stream()
					.map(rt -> {
						int requestsCount = dataProvider.getXmlRequestsDataProvider()
								.getConfirmedRequestsForRoomType(new Range<>(
										filter.getArrivalDate().orElse(null),
										filter.getDepartureDate().orElse(null)
								), rt.getTypeName()).orElse(Collections.emptyList()).size();
						return new AvailableRoomTypes(rt, (int) rt.getRoomNumbers().count() - requestsCount);
					})
					.collect(Collectors.toList());

			List<AvailableRoomTypes> subList = actualResult
					.subList(
							(pagination.getPage() - 1) * pagination.getRecordsPerPage(),
							(pagination.getPage() - 1) * pagination.getRecordsPerPage() +
									pagination.getRecordsPerPage() <= filteredResult.size() ?
									(pagination.getPage() - 1) * pagination.getRecordsPerPage() +
											pagination.getRecordsPerPage() : filteredResult.size()
					);
			return Either.left(new Pair(
					subList,
					new Pagination(pagination.getRecordsPerPage(), pagination.getPage(), actualResult.size())
			));
		} catch (IOException exc) {
			return Either.right(new Exception("Can't read storage"));
		}
	}
}
