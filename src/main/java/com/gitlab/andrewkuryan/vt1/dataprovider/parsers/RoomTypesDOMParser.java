package com.gitlab.andrewkuryan.vt1.dataprovider.parsers;

import com.gitlab.andrewkuryan.vt1.entity.roomtype.RoomType;
import com.gitlab.andrewkuryan.vt1.entity.roomtype.RoomTypeBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class RoomTypesDOMParser implements Parser<RoomType> {

	@Override
	public List<RoomType> parse(InputStream stream) {
		List<RoomType> list = new ArrayList<>();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(stream);
			document.getDocumentElement().normalize();

			NodeList items = document.getElementsByTagName("item");
			for (int i = 0; i < items.getLength(); i++) {
				Element item = (Element) items.item(i);
				RoomTypeBuilder roomTypeBuilder = new RoomTypeBuilder()
						.setTypeName(getStringField(item, "typeName"))
						.setNumOfPlaces(getIntField(item, "numOfPlaces"))
						.setCost(getIntField(item, "cost"))
						.setArea(getDoubleField(item, "area"));

				String smallImage = getStringField(item, "smallImage");
				if (!smallImage.isEmpty()) {
					roomTypeBuilder.setSmallImage(new URL(smallImage));
				}
				roomTypeBuilder.setLargeImages(
						getListField(item, "largeImages", "image").stream()
								.map(str -> {
									try {
										return new URL(str);
									} catch (MalformedURLException exc) {
										return null;
									}
								})
								.filter(Objects::nonNull)
								.collect(Collectors.toList())
				);
				roomTypeBuilder.setRoomNumbers(
						getListField(item, "roomNumbers", "number").stream()
								.map(Integer::parseInt)
								.collect(Collectors.toList())
				);
				roomTypeBuilder.setServices(getListField(item, "services", "service"));

				list.add(roomTypeBuilder.build());
			}
		} catch (Exception ignored) {
			ignored.printStackTrace();
		}
		return list;
	}

	private String getStringField(Element element, String fieldName) {
		return element
				.getElementsByTagName(fieldName)
				.item(0)
				.getTextContent();
	}

	private int getIntField(Element element, String fieldName) {
		return Integer.parseInt(element
				.getElementsByTagName(fieldName)
				.item(0)
				.getTextContent());
	}

	private double getDoubleField(Element element, String fieldName) {
		return Double.parseDouble(element
				.getElementsByTagName(fieldName)
				.item(0)
				.getTextContent());
	}

	private List<String> getListField(Element element, String fieldName, String itemName) {
		NodeList itemsList = ((Element) element.getElementsByTagName(fieldName)
				.item(0))
				.getElementsByTagName(itemName);
		List<String> list = new ArrayList<>();
		for (int f = 0; f < itemsList.getLength(); f++) {
			String textContent = itemsList.item(f).getTextContent();
			if (!textContent.isEmpty()) {
				list.add(textContent);
			}
		}
		return list;
	}
}
