package com.gitlab.andrewkuryan.vt1.dataprovider.parsers;

import com.gitlab.andrewkuryan.vt1.entity.user.Client;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class ClientParser implements Parser<Client> {

	private class ClientBuilder {

		private String login;
		private String token;
		private String email;
		private String firstName;
		private String lastName;
		private String phoneNumber;

		public ClientBuilder setLogin(String login) {
			this.login = login;
			return this;
		}

		public ClientBuilder setToken(String token) {
			this.token = token;
			return this;
		}

		public ClientBuilder setEmail(String email) {
			this.email = email;
			return this;
		}

		public ClientBuilder setFirstName(String firstName) {
			this.firstName = firstName;
			return this;
		}

		public ClientBuilder setLastName(String lastName) {
			this.lastName = lastName;
			return this;
		}

		public ClientBuilder setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
			return this;
		}

		public Client build() {
			return new Client(login, token, email, firstName, lastName, phoneNumber);
		}
	}

	private class ClientSAXParser extends DefaultHandler {

		private List<Client> clients;
		private ClientBuilder clientBuilder;

		private String qName = "";

		public List<Client> getClients() {
			return clients;
		}

		@Override
		public void startDocument() {
			clients = new ArrayList<>();
		}

		@Override
		public void startElement(String namespaceURI, String localName, String qName, Attributes atts) {
			this.qName = qName;
			if (qName.equals("item")) {
				clientBuilder = new ClientBuilder();
			}
		}

		@Override
		public void endElement(String namespaceURI, String localName, String qName) {
			if (qName.equals("item")) {
				if (clientBuilder != null) {
					clients.add(clientBuilder.build());
					clientBuilder = null;
				}
			}
			this.qName = "";
		}

		@Override
		public void characters(char[] ch, int start, int length) {
			switch (qName) {
				case "login":
					clientBuilder.setLogin(new String(ch, start, length));
					break;
				case "token":
					clientBuilder.setToken(new String(ch, start, length));
					break;
				case "email":
					clientBuilder.setEmail(new String(ch, start, length));
					break;
				case "firstName":
					clientBuilder.setFirstName(new String(ch, start, length));
					break;
				case "lastName":
					clientBuilder.setLastName(new String(ch, start, length));
					break;
				case "phoneNumber":
					clientBuilder.setPhoneNumber(new String(ch, start, length));
					break;
			}
		}
	}

	@Override
	public List<Client> parse(InputStream stream) {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		ClientSAXParser saxParser = new ClientSAXParser();
		try {
			SAXParser parser = factory.newSAXParser();
			parser.parse(stream, saxParser);
		} catch (Exception ignored) {
		}
		return saxParser.getClients();
	}
}
