package com.gitlab.andrewkuryan.vt1.dataprovider.xml;

import com.gitlab.andrewkuryan.vt1.dataprovider.parsers.Parser;
import com.gitlab.andrewkuryan.vt1.entity.request.ConfirmedRequest;
import com.gitlab.andrewkuryan.vt1.entity.request.RejectedRequest;
import com.gitlab.andrewkuryan.vt1.entity.request.Request;
import com.gitlab.andrewkuryan.vt1.entity.roomtype.RoomType;
import com.gitlab.andrewkuryan.vt1.entity.user.Administrator;
import com.gitlab.andrewkuryan.vt1.entity.user.Client;
import com.gitlab.andrewkuryan.vt1.entity.user.User;
import com.gitlab.andrewkuryan.vt1.service.port.RequestsApi;
import com.gitlab.andrewkuryan.vt1.service.request.RequestsFilter;
import com.gitlab.andrewkuryan.vt1.service.utils.Pagination;
import com.gitlab.andrewkuryan.vt1.utils.Either;
import com.gitlab.andrewkuryan.vt1.utils.Pair;
import com.gitlab.andrewkuryan.vt1.utils.Range;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@SuppressWarnings("unchecked")
public class XMLRequestsDataProvider implements RequestsApi {

	private Path xmlPath;
	private Parser<Request> requestParser;
	private Parser<ConfirmedRequest> confirmedRequestParser;
	private Parser<RejectedRequest> rejectedRequestParser;

	public XMLRequestsDataProvider(
			Path xmlPath,
			Parser<Request> requestParser,
			Parser<ConfirmedRequest> confirmedRequestParser,
			Parser<RejectedRequest> rejectedRequestParser
	) {
		this.xmlPath = xmlPath;
		this.requestParser = requestParser;
		this.confirmedRequestParser = confirmedRequestParser;
		this.rejectedRequestParser = rejectedRequestParser;
	}

	public Optional<List<Request>> getConfirmedRequestsForRoomType(Range<Date> range, String roomTypeName) {
		try (InputStream is = Files.newInputStream(xmlPath.resolve("confirmedRequests.xml"))) {
			List<ConfirmedRequest> totalList = confirmedRequestParser.parse(is);
			List<Request> filteredResult = totalList.stream()
					.filter(r -> r.getRoomType().getTypeName().equals(roomTypeName))
					.filter(r -> {
						Range<Date> requestRange = new Range<>(r.getArrivalDate(), r.getDepartureDate());
						return (range.getFrom() != null && requestRange.contains(range.getFrom()))
								|| (range.getTo() != null && requestRange.contains(range.getTo()));
					})
					.collect(Collectors.toList());

			return Optional.of(filteredResult);
		} catch (IOException exc) {
			return Optional.empty();
		}
	}

	@Override
	public Either<List<Request>, Exception> getAllRequests(User user) {
		if (user instanceof Administrator || user instanceof Client) {
			try (InputStream is = Files.newInputStream(xmlPath.resolve("requests.xml"));
			     InputStream isC = Files.newInputStream(xmlPath.resolve("confirmedRequests.xml"));
			     InputStream isR = Files.newInputStream(xmlPath.resolve("rejectedRequests.xml"))) {
				List<Request> requestList = requestParser.parse(is);
				List<ConfirmedRequest> confirmedRequestList = confirmedRequestParser.parse(isC);
				List<RejectedRequest> rejectedRequestList = rejectedRequestParser.parse(isR);
				List<Request> totalList = new ArrayList<>();
				totalList.addAll(requestList);
				totalList.addAll(confirmedRequestList);
				totalList.addAll(rejectedRequestList);

				if (!(user instanceof Administrator)) {
					totalList = totalList.stream()
							.filter(r -> r.getCustomer().getLogin().equals(user.getLogin()))
							.collect(Collectors.toList());
				}
				return Either.left(totalList);
			} catch (IOException exc) {
				return Either.right(new Exception("Can't read storage"));
			}
		} else return Either.right(new Exception("Not authorized"));
	}

	@Override
	public Either<Pair<List<Request>, Pagination>, Exception> getRequests(User user, Pagination pagination) {
		return getRequests(user, pagination, new RequestsFilter());
	}

	@Override
	public Either<Pair<List<Request>, Pagination>, Exception> getRequests(User user, Pagination pagination, RequestsFilter filter) {
		if (user instanceof Client || user instanceof Administrator) {
			try (InputStream is = Files.newInputStream(xmlPath.resolve("requests.xml"));
			     InputStream isC = Files.newInputStream(xmlPath.resolve("confirmedRequests.xml"));
			     InputStream isR = Files.newInputStream(xmlPath.resolve("rejectedRequests.xml"))) {
				List<Request> requestList = requestParser.parse(is);
				List<ConfirmedRequest> confirmedRequestList = confirmedRequestParser.parse(isC);
				List<RejectedRequest> rejectedRequestList = rejectedRequestParser.parse(isR);
				List<Request> totalList = new ArrayList<>();
				totalList.addAll(requestList);
				totalList.addAll(confirmedRequestList);
				totalList.addAll(rejectedRequestList);

				if (!(user instanceof Administrator)) {
					totalList = totalList.stream()
							.filter(r -> r.getCustomer().getLogin().equals(user.getLogin()))
							.collect(Collectors.toList());
				}
				List<Request> filteredResult = totalList.stream()
						.filter(r -> !filter.isConfirmed() || r instanceof ConfirmedRequest)
						.filter(r -> !filter.isRejected() || r instanceof RejectedRequest)
						.filter(r -> filter.getRoomTypeName().isEmpty() || r.getRoomType().getTypeName().equals(filter.getRoomTypeName().get()))
						.filter(r -> filter.getCustomerLogin().isEmpty() || r.getCustomer().getLogin().equals(filter.getCustomerLogin().get()))
						.filter(r -> filter.getNumberOfPersons().contains(r.getNumberOfPersons()))
						.filter(r -> filter.getArrivalDate().contains(r.getArrivalDate()))
						.filter(r -> filter.getDepartureDate().contains(r.getDepartureDate()))
						.collect(Collectors.toList());
				List<Request> subList = filteredResult
						.subList(
								(pagination.getPage() - 1) * pagination.getRecordsPerPage(),
								(pagination.getPage() - 1) * pagination.getRecordsPerPage() +
										pagination.getRecordsPerPage() <= filteredResult.size() ?
										(pagination.getPage() - 1) * pagination.getRecordsPerPage() +
												pagination.getRecordsPerPage() : filteredResult.size()
						);
				return Either.left(new Pair(
						subList,
						new Pagination(pagination.getRecordsPerPage(), pagination.getPage(), filteredResult.size())
				));
			} catch (IOException exc) {
				return Either.right(new Exception("Can't read storage"));
			}
		} else return Either.right(new Exception("Not authorized"));
	}

	@Override
	public Either<Request, Exception> createRequest(User user, RoomType roomType, Date arrivalDate, Date departureDate, int numberOfPersons) {
		return null;
	}

	@Override
	public Optional<Exception> confirmRequest(User user, Request request, int roomNumber) {
		return Optional.empty();
	}

	@Override
	public Optional<Exception> cancelRequest(User user, Request request) {
		return Optional.empty();
	}

	@Override
	public Optional<Exception> rejectRequest(User user, Request request, String comment) {
		return Optional.empty();
	}
}
