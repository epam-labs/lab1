package com.gitlab.andrewkuryan.vt1.dataprovider.parsers;

import com.gitlab.andrewkuryan.vt1.entity.request.RejectedRequest;

public class RejectedRequestBuilder extends RequestBuilder {

	private String comment;

	public RejectedRequestBuilder setComment(String comment) {
		this.comment = comment;
		return this;
	}

	@Override
	public RejectedRequest build() {
		return new RejectedRequest(super.build(), comment);
	}
}
