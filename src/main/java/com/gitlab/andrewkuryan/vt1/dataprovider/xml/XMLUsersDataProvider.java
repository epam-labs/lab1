package com.gitlab.andrewkuryan.vt1.dataprovider.xml;

import com.gitlab.andrewkuryan.vt1.dataprovider.parsers.Parser;
import com.gitlab.andrewkuryan.vt1.entity.user.Administrator;
import com.gitlab.andrewkuryan.vt1.entity.user.Client;
import com.gitlab.andrewkuryan.vt1.entity.user.User;
import com.gitlab.andrewkuryan.vt1.service.port.UsersApi;
import com.gitlab.andrewkuryan.vt1.service.port.exceptions.IncorrectPasswordException;
import com.gitlab.andrewkuryan.vt1.service.port.exceptions.NoSuchUserException;
import com.gitlab.andrewkuryan.vt1.utils.Either;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@SuppressWarnings("unchecked")
public class XMLUsersDataProvider implements UsersApi {

	private Path xmlPath;
	private Parser<Client> clientParser;
	private Parser<Administrator> adminParser;

	public XMLUsersDataProvider(Path xmlPath, Parser<Client> clientParser, Parser<Administrator> adminParser) {
		this.xmlPath = xmlPath;
		this.clientParser = clientParser;
		this.adminParser = adminParser;
	}

	@Override
	public Either<User, Exception> register(String login, String password, String firstName, String lastName, String email, String phoneNumber) {
		return null;
	}

	@Override
	public Either<User, Exception> authorize(String login, String password) {
		String receivedToken = md5(login + password);
		try (InputStream adminIs = Files.newInputStream(xmlPath.resolve("admins.xml"))) {
			Administrator administrator = adminParser.parse(adminIs).stream()
					.filter(admin -> admin.getLogin().equals(login))
					.findFirst()
					.orElse(null);
			if (administrator == null) {
				try (InputStream clientIs = Files.newInputStream(xmlPath.resolve("clients.xml"))) {
					Client client = clientParser.parse(clientIs).stream()
							.filter(cl -> cl.getLogin().equals(login))
							.findFirst()
							.orElse(null);
					if (client == null) {
						return Either.right(new NoSuchUserException());
					} else {
						if (client.getToken().equals(receivedToken)) {
							return Either.left(client);
						} else {
							return Either.right(new IncorrectPasswordException());
						}
					}
				}
			} else {
				if (administrator.getToken().equals(receivedToken)) {
					return Either.left(administrator);
				} else {
					return Either.right(new IncorrectPasswordException());
				}
			}
		} catch (IOException exc) {
			return Either.right(new Exception("Can't read storage"));
		}
	}

	@Override
	public Either<User, Exception> update(User user, String oldPassword, String password, String firstName, String lastName, String email, String phoneNumber) {
		return null;
	}

	private String md5(String st) {
		byte[] digest = new byte[0];

		try {
			MessageDigest messageDigest = MessageDigest.getInstance("MD5");
			messageDigest.reset();
			messageDigest.update(st.getBytes());
			digest = messageDigest.digest();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		BigInteger bigInt = new BigInteger(1, digest);
		StringBuilder md5Hex = new StringBuilder(bigInt.toString(16));

		while (md5Hex.length() < 32) {
			md5Hex.insert(0, "0");
		}

		return md5Hex.toString();
	}
}
