package com.gitlab.andrewkuryan.vt1.dataprovider.parsers;

import com.gitlab.andrewkuryan.vt1.entity.request.Request;
import com.gitlab.andrewkuryan.vt1.entity.roomtype.RoomType;
import com.gitlab.andrewkuryan.vt1.entity.user.Client;

import java.util.Date;

public class RequestBuilder {

	private RoomType roomType;
	private Date arrivalDate;
	private Date departureDate;
	private int numberOfPersons;
	private Client customer;

	public RequestBuilder setRoomType(RoomType roomType) {
		this.roomType = roomType;
		return this;
	}

	public RequestBuilder setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
		return this;
	}

	public RequestBuilder setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
		return this;
	}

	public RequestBuilder setNumberOfPersons(int numberOfPersons) {
		this.numberOfPersons = numberOfPersons;
		return this;
	}

	public RequestBuilder setCustomer(Client customer) {
		this.customer = customer;
		return this;
	}

	public Request build() {
		return new Request(roomType, arrivalDate, departureDate, numberOfPersons, customer);
	}
}
