package com.gitlab.andrewkuryan.vt1.dataprovider.parsers;

import com.gitlab.andrewkuryan.vt1.entity.request.ConfirmedRequest;

public class ConfirmedRequestBuilder extends RequestBuilder {

	private int roomNumber;

	public ConfirmedRequestBuilder setRoomNumber(int roomNumber) {
		this.roomNumber = roomNumber;
		return this;
	}

	@Override
	public ConfirmedRequest build() {
		return new ConfirmedRequest(super.build(), roomNumber);
	}
}
