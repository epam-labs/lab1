package com.gitlab.andrewkuryan.vt1.dataprovider.xml;

import com.gitlab.andrewkuryan.vt1.dataprovider.parsers.*;
import com.gitlab.andrewkuryan.vt1.entity.request.Request;
import com.gitlab.andrewkuryan.vt1.entity.roomtype.AvailableRoomTypes;
import com.gitlab.andrewkuryan.vt1.entity.roomtype.RoomType;
import com.gitlab.andrewkuryan.vt1.entity.user.User;
import com.gitlab.andrewkuryan.vt1.service.port.DataProvider;
import com.gitlab.andrewkuryan.vt1.service.request.RequestsFilter;
import com.gitlab.andrewkuryan.vt1.service.roomtype.AvailableRoomTypesFilter;
import com.gitlab.andrewkuryan.vt1.service.roomtype.RoomTypeFilter;
import com.gitlab.andrewkuryan.vt1.service.utils.Pagination;
import com.gitlab.andrewkuryan.vt1.utils.Either;
import com.gitlab.andrewkuryan.vt1.utils.Pair;

import java.nio.file.Path;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class XMLDataProvider implements DataProvider {

	private XMLUsersDataProvider xmlUsersDataProvider;
	private XMLRoomTypesDataProvider xmlRoomTypesDataProvider;
	private XMLAvailableRoomTypesDataProvider xmlAvailableRoomTypesDataProvider;
	private XMLRequestsDataProvider xmlRequestsDataProvider;

	public XMLDataProvider(Path rootPath) {
		var clientParser = new ClientParser();
		var adminParser = new AdminParser();
		var roomTypeParser = new RoomTypesDOMParser();

		xmlUsersDataProvider = new XMLUsersDataProvider(rootPath, clientParser, adminParser);
		xmlRoomTypesDataProvider = new XMLRoomTypesDataProvider(rootPath, roomTypeParser);
		xmlAvailableRoomTypesDataProvider = new XMLAvailableRoomTypesDataProvider(rootPath, roomTypeParser, this);
		xmlRequestsDataProvider = new XMLRequestsDataProvider(
				rootPath,
				new RequestsParser(clientParser, roomTypeParser),
				new ConfirmedRequestsParser(clientParser, roomTypeParser),
				new RejectedRequestsParser(clientParser, roomTypeParser)
		);
	}

	public XMLUsersDataProvider getXmlUsersDataProvider() {
		return xmlUsersDataProvider;
	}

	public XMLRoomTypesDataProvider getXmlRoomTypesDataProvider() {
		return xmlRoomTypesDataProvider;
	}

	public XMLAvailableRoomTypesDataProvider getXmlAvailableRoomTypesDataProvider() {
		return xmlAvailableRoomTypesDataProvider;
	}

	public XMLRequestsDataProvider getXmlRequestsDataProvider() {
		return xmlRequestsDataProvider;
	}

	/**
	 * Requests
	 */

	@Override
	public Either<List<Request>, Exception> getAllRequests(User user) {
		return xmlRequestsDataProvider.getAllRequests(user);
	}

	@Override
	public Either<Pair<List<Request>, Pagination>, Exception> getRequests(User user, Pagination pagination) {
		return xmlRequestsDataProvider.getRequests(user, pagination);
	}

	@Override
	public Either<Pair<List<Request>, Pagination>, Exception> getRequests(User user, Pagination pagination, RequestsFilter filter) {
		return xmlRequestsDataProvider.getRequests(user, pagination, filter);
	}

	@Override
	public Either<Request, Exception> createRequest(User user, RoomType roomType, Date arrivalDate, Date departureDate, int numberOfPersons) {
		return xmlRequestsDataProvider.createRequest(user, roomType, arrivalDate, departureDate, numberOfPersons);
	}

	@Override
	public Optional<Exception> confirmRequest(User user, Request request, int roomNumber) {
		return xmlRequestsDataProvider.confirmRequest(user, request, roomNumber);
	}

	@Override
	public Optional<Exception> cancelRequest(User user, Request request) {
		return xmlRequestsDataProvider.cancelRequest(user, request);
	}

	@Override
	public Optional<Exception> rejectRequest(User user, Request request, String comment) {
		return xmlRequestsDataProvider.rejectRequest(user, request, comment);
	}


	/**
	 * User
	 */

	@Override
	public Either<User, Exception> register(String login, String password, String firstName, String lastName, String email, String phoneNumber) {
		return xmlUsersDataProvider.register(login, password, firstName, lastName, email, phoneNumber);
	}

	@Override
	public Either<User, Exception> authorize(String login, String password) {
		return xmlUsersDataProvider.authorize(login, password);
	}

	@Override
	public Either<User, Exception> update(User user, String oldPassword, String password, String firstName, String lastName, String email, String phoneNumber) {
		return xmlUsersDataProvider.update(user, oldPassword, password, firstName, lastName, email, phoneNumber);
	}


	/**
	 * Available Room Types
	 */

	@Override
	public Either<Pair<List<AvailableRoomTypes>, Pagination>, Exception> getAvailableRoomTypes(User user, Pagination pagination) {
		return xmlAvailableRoomTypesDataProvider.getAvailableRoomTypes(user, pagination);
	}

	@Override
	public Either<Pair<List<AvailableRoomTypes>, Pagination>, Exception> getAvailableRoomTypes(User user, Pagination pagination, AvailableRoomTypesFilter filter) {
		return xmlAvailableRoomTypesDataProvider.getAvailableRoomTypes(user, pagination, filter);
	}


	/**
	 * Room Types
	 */

	@Override
	public Either<List<RoomType>, Exception> getAllRoomTypes(User user) {
		return xmlRoomTypesDataProvider.getAllRoomTypes(user);
	}

	@Override
	public Either<Pair<List<RoomType>, Pagination>, Exception> getRoomTypes(User user, Pagination pagination) {
		return xmlRoomTypesDataProvider.getRoomTypes(user, pagination);
	}

	@Override
	public Either<Pair<List<RoomType>, Pagination>, Exception> getRoomTypes(User user, Pagination pagination, RoomTypeFilter filter) {
		return xmlRoomTypesDataProvider.getRoomTypes(user, pagination, filter);
	}

	@Override
	public Either<List<String>, Exception> getAvailableServices(User user) {
		return xmlRoomTypesDataProvider.getAvailableServices(user);
	}
}
