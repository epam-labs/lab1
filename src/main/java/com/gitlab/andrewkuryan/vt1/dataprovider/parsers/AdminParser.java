package com.gitlab.andrewkuryan.vt1.dataprovider.parsers;

import com.gitlab.andrewkuryan.vt1.entity.user.Administrator;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class AdminParser implements Parser<Administrator> {

	private class AdminBuilder {

		private String login;
		private String token;
		private String firstName;
		private String lastName;

		public AdminBuilder setLogin(String login) {
			this.login = login;
			return this;
		}

		public AdminBuilder setToken(String token) {
			this.token = token;
			return this;
		}

		public AdminBuilder setFirstName(String firstName) {
			this.firstName = firstName;
			return this;
		}

		public AdminBuilder setLastName(String lastName) {
			this.lastName = lastName;
			return this;
		}

		public Administrator build() {
			return new Administrator(login, token, firstName, lastName);
		}
	}

	private class AdminSAXParser extends DefaultHandler {

		private List<Administrator> administrators;
		private AdminBuilder adminBuilder;

		private String qName = "";

		public List<Administrator> getAdministrators() {
			return administrators;
		}

		@Override
		public void startDocument() {
			administrators = new ArrayList<>();
		}

		@Override
		public void startElement(String namespaceURI, String localName, String qName, Attributes atts) {
			this.qName = qName;
			if (qName.equals("item")) {
				adminBuilder = new AdminBuilder();
			}
		}

		@Override
		public void endElement(String namespaceURI, String localName, String qName) {
			if (qName.equals("item")) {
				if (adminBuilder != null) {
					administrators.add(adminBuilder.build());
					adminBuilder = null;
				}
			}
			this.qName = "";
		}

		@Override
		public void characters(char[] ch, int start, int length) {
			switch (qName) {
				case "login":
					adminBuilder.setLogin(new String(ch, start, length));
					break;
				case "token":
					adminBuilder.setToken(new String(ch, start, length));
					break;
				case "firstName":
					adminBuilder.setFirstName(new String(ch, start, length));
					break;
				case "lastName":
					adminBuilder.setLastName(new String(ch, start, length));
					break;
			}
		}
	}

	@Override
	public List<Administrator> parse(InputStream stream) {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		AdminSAXParser saxParser = new AdminSAXParser();
		try {
			SAXParser parser = factory.newSAXParser();
			parser.parse(stream, saxParser);
		} catch (Exception ignored) {
		}
		return saxParser.getAdministrators();
	}
}
