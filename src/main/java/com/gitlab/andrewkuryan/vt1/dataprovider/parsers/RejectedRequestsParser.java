package com.gitlab.andrewkuryan.vt1.dataprovider.parsers;

import com.gitlab.andrewkuryan.vt1.entity.request.RejectedRequest;
import com.gitlab.andrewkuryan.vt1.entity.roomtype.RoomType;
import com.gitlab.andrewkuryan.vt1.entity.user.Client;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class RejectedRequestsParser implements Parser<RejectedRequest> {

	private Parser<Client> clientParser;
	private Parser<RoomType> roomTypeParser;
	private DateFormat dateFormat;

	public RejectedRequestsParser(Parser<Client> clientParser, Parser<RoomType> roomTypeParser) {
		this.clientParser = clientParser;
		this.roomTypeParser = roomTypeParser;
		dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:sszzz");
	}

	@Override
	public List<RejectedRequest> parse(InputStream stream) {
		List<RejectedRequest> list = new ArrayList<>();
		XMLInputFactory factory = XMLInputFactory.newInstance();
		XMLEventReader reader = null;
		RejectedRequestBuilder requestBuilder = null;

		StringBuilder roomTypeStr = null;
		StringBuilder customerStr = null;
		try {
			reader = factory.createXMLEventReader(stream);
			while (reader.hasNext()) {
				XMLEvent event = reader.nextEvent();
				if (event.isStartElement()) {
					StartElement startElement = event.asStartElement();
					switch (startElement.getName().getLocalPart()) {
						case "item":
							requestBuilder = new RejectedRequestBuilder();
							break;
						case "roomType":
							roomTypeStr = new StringBuilder();
							roomTypeStr.append(startElement.toString());
							break;
						case "customer":
							customerStr = new StringBuilder();
							customerStr.append(startElement.toString());
							break;
						case "arrivalDate":
							event = reader.nextEvent();
							if (requestBuilder != null) {
								requestBuilder.setArrivalDate(dateFormat.parse(event.asCharacters().getData()));
							}
							break;
						case "departureDate":
							event = reader.nextEvent();
							if (requestBuilder != null) {
								requestBuilder.setDepartureDate(dateFormat.parse(event.asCharacters().getData()));
							}
							break;
						case "numberOfPersons":
							event = reader.nextEvent();
							if (requestBuilder != null) {
								requestBuilder.setNumberOfPersons(Integer.parseInt(event.asCharacters().getData()));
							}
							break;
						case "comment":
							event = reader.nextEvent();
							if (requestBuilder != null) {
								requestBuilder.setComment(event.asCharacters().getData());
							}
							break;
						default:
							if (roomTypeStr != null) {
								roomTypeStr.append(startElement.toString());
							} else if (customerStr != null) {
								customerStr.append(startElement.toString());
							}
					}
				} else if (event.isEndElement()) {
					EndElement endElement = event.asEndElement();
					switch (endElement.getName().getLocalPart()) {
						case "item":
							if (requestBuilder != null) {
								list.add(requestBuilder.build());
							}
							break;
						case "roomType":
							if (roomTypeStr != null && requestBuilder != null) {
								roomTypeStr.append(endElement.toString());
								List<RoomType> roomTypes = roomTypeParser
										.parse(elementToStream(roomTypeStr.toString()));
								requestBuilder.setRoomType(roomTypes.get(0));
								roomTypeStr = null;
							}
							break;
						case "customer":
							if (customerStr != null && requestBuilder != null) {
								customerStr.append(endElement.toString());
								List<Client> clients = clientParser
										.parse(elementToStream(customerStr.toString()));
								requestBuilder.setCustomer(clients.get(0));
								customerStr = null;
							}
							break;
						default:
							if (roomTypeStr != null) {
								roomTypeStr.append(endElement.toString());
							} else if (customerStr != null) {
								customerStr.append(endElement.toString());
							}
							break;
					}
				} else if (event.isCharacters()) {
					if (roomTypeStr != null) {
						roomTypeStr.append(event.asCharacters().getData());
					} else if (customerStr != null) {
						customerStr.append(event.asCharacters().getData());
					}
				}
			}
		} catch (Exception ignored) {
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (Exception ignored) {
				}
			}
		}
		return list;
	}

	private InputStream elementToStream(String element) {
		return new ByteArrayInputStream(
				"<item>"
						.concat(element.replace("&", "&amp;"))
						.concat("</item>")
						.getBytes(StandardCharsets.UTF_8)
		);
	}
}
