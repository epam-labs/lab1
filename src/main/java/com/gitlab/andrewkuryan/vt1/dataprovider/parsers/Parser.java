package com.gitlab.andrewkuryan.vt1.dataprovider.parsers;

import java.io.InputStream;
import java.util.List;

public interface Parser<T> {

	List<T> parse(InputStream stream);
}
