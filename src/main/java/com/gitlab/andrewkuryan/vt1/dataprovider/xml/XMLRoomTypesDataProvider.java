package com.gitlab.andrewkuryan.vt1.dataprovider.xml;

import com.gitlab.andrewkuryan.vt1.dataprovider.parsers.Parser;
import com.gitlab.andrewkuryan.vt1.entity.roomtype.RoomType;
import com.gitlab.andrewkuryan.vt1.entity.user.User;
import com.gitlab.andrewkuryan.vt1.service.port.roomtype.RoomTypeApi;
import com.gitlab.andrewkuryan.vt1.service.roomtype.RoomTypeFilter;
import com.gitlab.andrewkuryan.vt1.service.utils.Pagination;
import com.gitlab.andrewkuryan.vt1.utils.Either;
import com.gitlab.andrewkuryan.vt1.utils.Pair;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("unchecked")
public class XMLRoomTypesDataProvider implements RoomTypeApi {

	private Path xmlPath;
	private Parser<RoomType> roomTypeParser;

	public XMLRoomTypesDataProvider(Path xmlPath, Parser<RoomType> roomTypeParser) {
		this.xmlPath = xmlPath;
		this.roomTypeParser = roomTypeParser;
	}

	@Override
	public Either<List<RoomType>, Exception> getAllRoomTypes(User user) {
		try (InputStream is = Files.newInputStream(xmlPath.resolve("roomTypes.xml"))) {
			List<RoomType> result = roomTypeParser.parse(is);
			return Either.left(result);
		} catch (IOException exc) {
			return Either.right(new Exception("Can't read storage"));
		}
	}

	@Override
	public Either<Pair<List<RoomType>, Pagination>, Exception> getRoomTypes(User user, Pagination pagination) {
		return getRoomTypes(user, pagination, new RoomTypeFilter());
	}

	@Override
	public Either<Pair<List<RoomType>, Pagination>, Exception> getRoomTypes(User user, Pagination pagination, RoomTypeFilter filter) {
		try (InputStream is = Files.newInputStream(xmlPath.resolve("roomTypes.xml"))) {
			List<RoomType> totalList = roomTypeParser.parse(is);
			List<String> filterServices = filter.getServices().collect(Collectors.toList());
			List<RoomType> filteredResult = totalList.stream()
					.filter(rt -> filter.getTypeName().isEmpty() || rt.getTypeName().equals(filter.getTypeName().get()))
					.filter(rt -> filter.getNumOfPlaces().contains(rt.getNumOfPlaces()))
					.filter(rt -> filter.getCost().contains(rt.getCost()))
					.filter(rt -> filter.getArea().contains(rt.getArea()))
					.filter(rt -> filterServices.isEmpty() || rt.getServices().collect(Collectors.toList()).containsAll(filterServices))
					.collect(Collectors.toList());
			List<RoomType> subList = filteredResult
					.subList(
							(pagination.getPage() - 1) * pagination.getRecordsPerPage(),
							(pagination.getPage() - 1) * pagination.getRecordsPerPage() +
									pagination.getRecordsPerPage() <= filteredResult.size() ?
									(pagination.getPage() - 1) * pagination.getRecordsPerPage() +
											pagination.getRecordsPerPage() : filteredResult.size()
					);
			return Either.left(new Pair(
					subList,
					new Pagination(pagination.getRecordsPerPage(), pagination.getPage(), filteredResult.size())
			));
		} catch (IOException exc) {
			return Either.right(new Exception("Can't read storage"));
		}
	}

	@Override
	public Either<List<String>, Exception> getAvailableServices(User user) {
		return null;
	}
}
