package com.gitlab.andrewkuryan.vt1.servlets;

import com.gitlab.andrewkuryan.vt1.entity.roomtype.AvailableRoomTypes;
import com.gitlab.andrewkuryan.vt1.service.roomtype.AvailableRoomTypesFilter;
import com.gitlab.andrewkuryan.vt1.service.roomtype.AvailableRoomTypesModel;
import com.gitlab.andrewkuryan.vt1.service.roomtype.port.AvailableRoomsViewModel;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class AvailableRoomTypesServlet extends HttpServlet implements AvailableRoomsViewModel {

	private AvailableRoomTypesModel model;

	private int page;
	private int totalCount;
	private int recordsPerPage;
	private List<AvailableRoomTypes> actualRoomTypes;

	public AvailableRoomTypesServlet() {
		model = WebApp.getInstance().getAppService().getAvailableRoomTypesModel();
		model.addViewModel(this);
		recordsPerPage = 2;
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int index = Integer.parseInt(req.getParameter("page") == null ? "1" : req.getParameter("page"));
		if (req.getParameter("recordsPerPage") != null) {
			recordsPerPage = Integer.parseInt(req.getParameter("recordsPerPage"));
		}

		model.getRoomTypesOnPage(index, false);

		req.setAttribute("roomTypes", actualRoomTypes);
		req.setAttribute("page", page);
		req.setAttribute("recordsPerPage", recordsPerPage);
		req.setAttribute("totalCount", totalCount);
		req.setAttribute("totalPages", Math.ceil((double) totalCount / recordsPerPage));

		RequestDispatcher requestDispatcher = req.getRequestDispatcher("views/availableRoomTypes.jsp");
		requestDispatcher.forward(req, resp);
	}

	@Override
	public void setPage(int page) {
		this.page = page;
	}

	@Override
	public void setTotalCount(int count) {
		this.totalCount = count;
	}

	@Override
	public Optional<AvailableRoomTypesFilter> getFilter() {
		return Optional.empty();
	}

	@Override
	public void setAvailableServices(List<String> services) {

	}

	@Override
	public int getRecordsPerPage() {
		return recordsPerPage;
	}

	@Override
	public AvailableRoomTypes getSelectedRoomType() {
		return null;
	}

	@Override
	public void setActualAvailableRoomTypes(List<AvailableRoomTypes> roomTypes) {
		this.actualRoomTypes = roomTypes;
	}
}
