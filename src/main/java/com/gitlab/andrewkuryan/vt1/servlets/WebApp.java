package com.gitlab.andrewkuryan.vt1.servlets;

import com.gitlab.andrewkuryan.vt1.dataprovider.xml.XMLDataProvider;
import com.gitlab.andrewkuryan.vt1.entity.user.Administrator;
import com.gitlab.andrewkuryan.vt1.entity.user.User;
import com.gitlab.andrewkuryan.vt1.service.AppService;
import com.gitlab.andrewkuryan.vt1.service.HotelAppService;
import com.gitlab.andrewkuryan.vt1.service.account.port.AccountViewModel;

import java.nio.file.Paths;
import java.util.Optional;

public class WebApp {

	private static volatile WebApp INSTANCE = null;
	private AppService appService;

	private WebApp() {
		appService = new HotelAppService(
				new XMLDataProvider(Paths.get("/home/andrew/Документы/Java-projects/BSUIR/VT/xmlstorage")),
				new ServletViewModel()
		);
		appService.getAccountModel().addViewModel(
				new AccountViewModel() {
					private User account;

					@Override
					public void setAccount(User account) {
						this.account = account;
					}

					@Override
					public Optional<User> getAccount() {
						return Optional.ofNullable(account);
					}
				}
		);
		appService.getAccountModel().setAccount(new Administrator(
				"admin",
				"f6fdffe48c908deb0f4c3bd36c032e72",
				"Admin",
				"Admin"
		));
	}

	public AppService getAppService() {
		return appService;
	}

	public static WebApp getInstance() {
		if (INSTANCE == null) {
			synchronized (WebApp.class) {
				INSTANCE = new WebApp();
			}
		}
		return INSTANCE;
	}
}
