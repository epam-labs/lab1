package com.gitlab.andrewkuryan.vt1.servlets;

import com.gitlab.andrewkuryan.vt1.entity.request.ConfirmedRequest;
import com.gitlab.andrewkuryan.vt1.entity.request.RejectedRequest;
import com.gitlab.andrewkuryan.vt1.entity.request.Request;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class FormattedRequest {

	private String roomTypeName;
	private String arrivalDate;
	private String departureDate;
	private int numberOfPersons;
	private String customer;
	private String status;

	public FormattedRequest(Request request) {
		DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
		roomTypeName = request.getRoomType().getTypeName();
		arrivalDate = dateFormat.format(request.getArrivalDate());
		departureDate = dateFormat.format(request.getDepartureDate());
		numberOfPersons = request.getNumberOfPersons();
		customer = request.getCustomer().getLogin();
		status = request instanceof ConfirmedRequest ? "Confirmed" : request instanceof RejectedRequest ? "Rejected" : "Processing";
	}

	public String getRoomTypeName() {
		return roomTypeName;
	}

	public String getArrivalDate() {
		return arrivalDate;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public int getNumberOfPersons() {
		return numberOfPersons;
	}

	public String getCustomer() {
		return customer;
	}

	public String getStatus() {
		return status;
	}
}
