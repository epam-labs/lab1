package com.gitlab.andrewkuryan.vt1.servlets;

import com.gitlab.andrewkuryan.vt1.entity.request.Request;
import com.gitlab.andrewkuryan.vt1.service.request.RequestsFilter;
import com.gitlab.andrewkuryan.vt1.service.request.RequestsModel;
import com.gitlab.andrewkuryan.vt1.service.request.port.RequestsViewModel;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class RequestsServlet extends HttpServlet implements RequestsViewModel {

	private RequestsModel model;

	private int page;
	private int totalCount;
	private int recordsPerPage;
	private List<Request> actualRequests;

	public RequestsServlet() {
		model = WebApp.getInstance().getAppService().getAdminRequestsModel();
		model.addViewModel(this);
		recordsPerPage = 2;
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		int index = Integer.parseInt(req.getParameter("page") == null ? "1" : req.getParameter("page"));
		if (req.getParameter("recordsPerPage") != null) {
			recordsPerPage = Integer.parseInt(req.getParameter("recordsPerPage"));
		}

		model.getRequestsOnPage(index, false);

		req.setAttribute("requests", actualRequests.stream()
				.map(FormattedRequest::new)
				.collect(Collectors.toList()));
		req.setAttribute("page", page);
		req.setAttribute("recordsPerPage", recordsPerPage);
		req.setAttribute("totalCount", totalCount);
		req.setAttribute("totalPages", Math.ceil((double) totalCount / recordsPerPage));

		RequestDispatcher requestDispatcher = req.getRequestDispatcher("views/requests.jsp");
		requestDispatcher.forward(req, resp);
	}

	@Override
	public void setPage(int page) {
		this.page = page;
	}

	@Override
	public void setTotalCount(int count) {
		this.totalCount = count;
	}

	@Override
	public Optional<RequestsFilter> getFilter() {
		return Optional.empty();
	}

	@Override
	public int getRecordsPerPage() {
		return recordsPerPage;
	}

	@Override
	public void setActualRequests(List<Request> requests) {
		this.actualRequests = requests;
	}
}
