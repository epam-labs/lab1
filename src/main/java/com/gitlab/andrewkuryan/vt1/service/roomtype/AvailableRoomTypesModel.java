package com.gitlab.andrewkuryan.vt1.service.roomtype;

import com.gitlab.andrewkuryan.vt1.entity.roomtype.AvailableRoomTypes;
import com.gitlab.andrewkuryan.vt1.service.roomtype.port.AvailableRoomsViewModel;

public interface AvailableRoomTypesModel {

	void addViewModel(AvailableRoomsViewModel viewModel);

	void getRoomTypesOnPage(int page, boolean useCache);

	void getAvailableServices();

	AvailableRoomTypes getSelectedRoomType();
}
