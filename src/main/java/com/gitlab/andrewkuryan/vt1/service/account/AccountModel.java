package com.gitlab.andrewkuryan.vt1.service.account;

import com.gitlab.andrewkuryan.vt1.entity.user.User;
import com.gitlab.andrewkuryan.vt1.service.account.port.AccountViewModel;

import java.util.Optional;

public interface AccountModel {

	void addViewModel(AccountViewModel viewModel);

	Optional<User> getAccount();

	void setAccount(User user);

	void update(String oldPassword, String password, String firstName, String lastName, String email, String phoneNumber);
}
