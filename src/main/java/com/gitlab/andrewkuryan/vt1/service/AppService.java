package com.gitlab.andrewkuryan.vt1.service;

import com.gitlab.andrewkuryan.vt1.service.account.AccountModel;
import com.gitlab.andrewkuryan.vt1.service.auth.LoginModel;
import com.gitlab.andrewkuryan.vt1.service.auth.RegisterModel;
import com.gitlab.andrewkuryan.vt1.service.port.DataProvider;
import com.gitlab.andrewkuryan.vt1.service.request.AdminRequestsModel;
import com.gitlab.andrewkuryan.vt1.service.request.ClientRequestsModel;
import com.gitlab.andrewkuryan.vt1.service.roomtype.AvailableRoomTypesModel;
import com.gitlab.andrewkuryan.vt1.service.roomtype.RoomTypeModel;

public interface AppService {

	DataProvider getDataProvider();
	AppViewModel getViewModel();

	AccountModel getAccountModel();
	LoginModel getLoginModel();
	RegisterModel getRegisterModel();
	RoomTypeModel getRoomTypeModel();
	AvailableRoomTypesModel getAvailableRoomTypesModel();
	ClientRequestsModel getClientRequestsModel();
	AdminRequestsModel getAdminRequestsModel();
}
