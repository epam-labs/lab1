package com.gitlab.andrewkuryan.vt1.viewmodel;

import com.gitlab.andrewkuryan.vt1.service.AppViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.port.AlertManager;
import com.gitlab.andrewkuryan.vt1.viewmodel.port.ConfigurationManager;
import com.gitlab.andrewkuryan.vt1.viewmodel.port.NavigationManager;

public class FXAppViewModel implements AppViewModel, FXViewModel {

	private NavigationManager navigationManager;
	private AlertManager alertManager;
	private ConfigurationManager configurationManager;

	public FXAppViewModel(
			NavigationManager navigationManager,
			AlertManager alertManager,
			ConfigurationManager configurationManager
	) {
		this.navigationManager = navigationManager;
		this.alertManager = alertManager;
		this.configurationManager = configurationManager;
	}

	@Override
	public NavigationManager getNavigationManager() {
		return navigationManager;
	}

	@Override
	public AlertManager getAlertManager() {
		return alertManager;
	}

	@Override
	public ConfigurationManager getConfigurationManager() {
		return configurationManager;
	}

	@Override
	public void setError(String error) {
		alertManager.showConfirmAlert(error, t -> {});
	}

	@Override
	public void showInfo(String info) {
		System.out.println(info);
		// TODO: Show info alert
	}
}
