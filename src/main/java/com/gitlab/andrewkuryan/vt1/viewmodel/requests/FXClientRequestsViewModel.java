package com.gitlab.andrewkuryan.vt1.viewmodel.requests;

import com.gitlab.andrewkuryan.vt1.entity.request.Request;
import com.gitlab.andrewkuryan.vt1.entity.user.User;
import com.gitlab.andrewkuryan.vt1.service.request.ClientRequestsModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.FXViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.requests.port.ClientRequestsViewState;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class FXClientRequestsViewModel extends FXRequestsViewModel implements ClientRequestsViewState {

	private ClientRequestsModel model;

	private ObjectProperty<LocalDate> arrivalDate;
	private ObjectProperty<LocalDate> departureDate;
	private IntegerProperty numOfPersons;

	public FXClientRequestsViewModel(FXViewModel appViewModel, ClientRequestsModel model) {
		super(appViewModel, model);
		this.model = model;
		arrivalDate = new SimpleObjectProperty<>(null);
		departureDate = new SimpleObjectProperty<>(null);
		numOfPersons = new SimpleIntegerProperty(1);
	}

	@Override
	public String getSelectedRoomTypeName() {
		return model.getSelectedRoomType().getTypeName();
	}

	@Override
	public LocalDate getArrivalDate() {
		return arrivalDate.get();
	}

	@Override
	public ObjectProperty<LocalDate> arrivalDateProperty() {
		return arrivalDate;
	}

	@Override
	public LocalDate getDepartureDate() {
		return departureDate.get();
	}

	@Override
	public ObjectProperty<LocalDate> departureDateProperty() {
		return departureDate;
	}

	@Override
	public int getNumOfPersons() {
		return numOfPersons.get();
	}

	@Override
	public IntegerProperty numOfPersonsProperty() {
		return numOfPersons;
	}

	@Override
	public void cancelRequest(Request request) {
		model.cancelRequest(request);
	}

	@Override
	public void createRequest() {
		// TODO: Validation
		if (arrivalDate != null && departureDate != null) {
			model.createRequest(
					Date.from(arrivalDate.get()
							.atStartOfDay()
							.atZone(ZoneId.systemDefault())
							.toInstant()),
					Date.from(departureDate.get()
							.atStartOfDay()
							.atZone(ZoneId.systemDefault())
							.toInstant()),
					numOfPersons.get()
			);
		}
	}

	@Override
	public void goBack() {
		appViewModel.getNavigationManager().navigateToUpperScene();
	}

	@Override
	public void checkPermissions() {
		if (model.getAccount().isEmpty() || model.getAccount().get().equals(User.GUEST)) {
			appViewModel.getNavigationManager().navigateTo("auth");
		}
	}
}
