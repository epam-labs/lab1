package com.gitlab.andrewkuryan.vt1.viewmodel.roomtype.port;

import com.gitlab.andrewkuryan.vt1.entity.roomtype.RoomType;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;

import java.util.ArrayList;

public interface RoomTypeViewState extends RoomTypeViewStateFilter {

	int getPage();
	IntegerProperty pageProperty();

	int getTotalPages();
	IntegerProperty totalPagesProperty();

	int getRecordsPerPage();
	IntegerProperty recordsPerPageProperty();

	ObjectProperty<ArrayList<RoomType>> getActualRoomTypes();

	void getRecordsOnPage(int page);
	void refresh();
}
