package com.gitlab.andrewkuryan.vt1.viewmodel.account;

import com.gitlab.andrewkuryan.vt1.entity.user.Administrator;
import com.gitlab.andrewkuryan.vt1.entity.user.User;
import com.gitlab.andrewkuryan.vt1.service.account.AccountModel;
import com.gitlab.andrewkuryan.vt1.service.account.port.AccountViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.FXViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.account.port.AdminViewState;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.Optional;

public class FXAdminViewModel implements AccountViewModel, AdminViewState {

	private FXViewModel appViewModel;
	private AccountModel model;

	private StringProperty login;
	private StringProperty oldPassword;
	private StringProperty password;
	private StringProperty firstName;
	private StringProperty lastName;

	private Administrator admin;
	private User actualUser;

	public FXAdminViewModel(FXViewModel appViewModel, AccountModel model) {
		this.appViewModel = appViewModel;
		this.model = model;

		login = new SimpleStringProperty("");
		oldPassword = new SimpleStringProperty("");
		password = new SimpleStringProperty("");
		firstName = new SimpleStringProperty("");
		lastName = new SimpleStringProperty("");
	}

	@Override
	public Optional<User> getAccount() {
		return Optional.ofNullable(actualUser);
	}

	@Override
	public void setAccount(User account) {
		actualUser = account;
		if (account instanceof Administrator) {
			System.out.println("Admin: " + account);
			admin = (Administrator) account;
			login.set(admin.getLogin());
			firstName.set(admin.getFirstName());
			lastName.set(admin.getLastName());

			appViewModel.getConfigurationManager().configure("admin");
			appViewModel.getNavigationManager().navigateToUpperScene();
		}
	}

	@Override
	public String getLogin() {
		return login.get();
	}

	@Override
	public StringProperty loginProperty() {
		return login;
	}

	@Override
	public String getOldPassword() {
		return oldPassword.get();
	}

	@Override
	public StringProperty oldPasswordProperty() {
		return oldPassword;
	}

	@Override
	public String getPassword() {
		return password.get();
	}

	@Override
	public StringProperty passwordProperty() {
		return password;
	}

	@Override
	public String getFirstName() {
		return firstName.get();
	}

	@Override
	public StringProperty firstNameProperty() {
		return firstName;
	}

	@Override
	public String getLastName() {
		return lastName.get();
	}

	@Override
	public StringProperty lastNameProperty() {
		return lastName;
	}

	@Override
	public void update() {
		// TODO: Validate
		model.update(oldPassword.get(), password.get(), firstName.get(), lastName.get(),
				"", "");
	}

	@Override
	public void logout() {
		model.setAccount(User.GUEST);
	}

	@Override
	public void showProfile() {
		// TODO: Navigate to profile
	}

	@Override
	public void goBack() {
		appViewModel.getNavigationManager().navigateToUpperScene();
	}

	@Override
	public void goToProfile() {
		appViewModel.getNavigationManager().navigateTo("adminprofile");
	}
}
