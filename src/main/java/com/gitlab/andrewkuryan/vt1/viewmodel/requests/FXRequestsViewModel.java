package com.gitlab.andrewkuryan.vt1.viewmodel.requests;

import com.gitlab.andrewkuryan.vt1.entity.request.Request;
import com.gitlab.andrewkuryan.vt1.service.request.RequestsFilter;
import com.gitlab.andrewkuryan.vt1.service.request.RequestsModel;
import com.gitlab.andrewkuryan.vt1.service.request.port.RequestsViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.FXViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.requests.port.RequestsViewState;
import javafx.beans.property.*;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class FXRequestsViewModel implements RequestsViewModel, RequestsViewState {

	FXViewModel appViewModel;
	private RequestsModel requestsModel;
	private RequestsFilter filter;

	private IntegerProperty page;
	private IntegerProperty totalPages;
	private IntegerProperty totalCount;
	private IntegerProperty recordsPerPage;
	private ObjectProperty<ArrayList<Request>> actualRequests;

	private BooleanProperty isConfirmed;
	private BooleanProperty isRejected;
	private StringProperty roomTypeName;
	private StringProperty customerLogin;
	private StringProperty numOfPersonsFrom;
	private StringProperty numOfPersonsTo;
	private ObjectProperty<LocalDate> arrivalDateFrom;
	private ObjectProperty<LocalDate> arrivalDateTo;
	private ObjectProperty<LocalDate> departureDateFrom;
	private ObjectProperty<LocalDate> departureDateTo;

	FXRequestsViewModel(FXViewModel appViewModel, RequestsModel model) {
		this.appViewModel = appViewModel;
		requestsModel = model;
		filter = new RequestsFilter();

		page = new SimpleIntegerProperty(1);
		totalPages = new SimpleIntegerProperty(0);
		totalCount = new SimpleIntegerProperty(0);
		recordsPerPage = new SimpleIntegerProperty(2);
		actualRequests = new SimpleObjectProperty<>(new ArrayList<>());

		isConfirmed = new SimpleBooleanProperty(false);
		isRejected = new SimpleBooleanProperty(false);
		roomTypeName = new SimpleStringProperty("");
		customerLogin = new SimpleStringProperty("");
		numOfPersonsFrom = new SimpleStringProperty("");
		numOfPersonsTo = new SimpleStringProperty("");
		arrivalDateFrom = new SimpleObjectProperty<>(null);
		arrivalDateTo = new SimpleObjectProperty<>(null);
		departureDateFrom = new SimpleObjectProperty<>(null);
		departureDateTo = new SimpleObjectProperty<>(null);
	}

	/**Overrides from RequestsViewModel*/

	@Override
	public void setPage(int page) {
		this.page.set(page);
	}

	@Override
	public void setTotalCount(int count) {
		totalCount.set(count);
		totalPages.set((int) Math.ceil((double) count / recordsPerPage.get()));
	}

	@Override
	public Optional<RequestsFilter> getFilter() {
		return Optional.of(filter);
	}

	@Override
	public int getRecordsPerPage() {
		return recordsPerPage.get();
	}

	@Override
	public void setActualRequests(List<Request> requests) {
		actualRequests.set(new ArrayList<>(requests));
	}



	/**Overrides from RequestsViewState*/

	@Override
	public int getPage() {
		return page.get();
	}

	@Override
	public IntegerProperty pageProperty() {
		return page;
	}

	@Override
	public int getTotalPages() {
		return totalPages.get();
	}

	@Override
	public IntegerProperty totalPagesProperty() {
		return totalPages;
	}

	@Override
	public IntegerProperty recordsPerPageProperty() {
		return recordsPerPage;
	}

	@Override
	public ObjectProperty<ArrayList<Request>> getActualRequests() {
		return actualRequests;
	}

	@Override
	public void getRecordsOnPage(int page) {
		int actualTotalPages = (int) Math.ceil((double) totalCount.get() / recordsPerPage.get());
		actualTotalPages = actualTotalPages > 0 ? actualTotalPages : 1;
		if (page <= actualTotalPages) {
			requestsModel.getRequestsOnPage(page, true);
		} else {
			requestsModel.getRequestsOnPage(actualTotalPages, true);
		}
	}

	@Override
	public void refresh() {
		int actualTotalPages = (int) Math.ceil((double) totalCount.get() / recordsPerPage.get());
		actualTotalPages = actualTotalPages > 0 ? actualTotalPages : 1;
		if (page.get() <= actualTotalPages) {
			requestsModel.getRequestsOnPage(page.get(), false);
		} else {
			requestsModel.getRequestsOnPage(actualTotalPages, false);
		}
	}



	/**Overrides from RequestsViewStateFilter*/

	@Override
	public boolean getConfirmed() {
		return isConfirmed.get();
	}

	@Override
	public BooleanProperty confirmedProperty() {
		return isConfirmed;
	}

	@Override
	public boolean getRejected() {
		return isRejected.get();
	}

	@Override
	public BooleanProperty rejectedProperty() {
		return isRejected;
	}

	@Override
	public String getRoomTypeName() {
		return roomTypeName.get();
	}

	@Override
	public StringProperty roomTypeNameProperty() {
		return roomTypeName;
	}

	@Override
	public String getCustomerLogin() {
		return customerLogin.get();
	}

	@Override
	public StringProperty customerLoginProperty() {
		return customerLogin;
	}

	@Override
	public String getNumOfPersonsFrom() {
		return numOfPersonsFrom.get();
	}

	@Override
	public StringProperty numOfPersonsFromProperty() {
		return numOfPersonsFrom;
	}

	@Override
	public String getNumOfPersonsTo() {
		return numOfPersonsTo.get();
	}

	@Override
	public StringProperty numOfPersonsToProperty() {
		return numOfPersonsTo;
	}

	@Override
	public LocalDate getArrivalDateFrom() {
		return arrivalDateFrom.get();
	}

	@Override
	public ObjectProperty<LocalDate> arrivalDateFromProperty() {
		return arrivalDateFrom;
	}

	@Override
	public LocalDate getArrivalDateTo() {
		return arrivalDateTo.get();
	}

	@Override
	public ObjectProperty<LocalDate> arrivalDateToProperty() {
		return arrivalDateTo;
	}

	@Override
	public LocalDate getDepartureDateFrom() {
		return departureDateFrom.get();
	}

	@Override
	public ObjectProperty<LocalDate> departureDateFromProperty() {
		return departureDateFrom;
	}

	@Override
	public LocalDate getDepartureDateTo() {
		return departureDateTo.get();
	}

	@Override
	public ObjectProperty<LocalDate> departureDateToProperty() {
		return departureDateTo;
	}

	@Override
	public void applyFilter() {
		try {
			filter.setConfirmed(isConfirmed.get());
			filter.setRejected(isRejected.get());
			filter.setRoomTypeName(roomTypeName.get().isEmpty() ? null : roomTypeName.get());
			filter.setCustomerLogin(customerLogin.get().isEmpty() ? null : customerLogin.get());
			filter.setNumberOfPersonsFrom(numOfPersonsFrom.get().isEmpty() ? null : Integer.parseInt(numOfPersonsFrom.get()));
			filter.setNumberOfPersonsTo(numOfPersonsTo.get().isEmpty() ? null : Integer.parseInt(numOfPersonsTo.get()));
			filter.setArrivalDateFrom(arrivalDateFrom.get() == null ? null : Date.from(arrivalDateFrom.get()
					.atStartOfDay()
					.atZone(ZoneId.systemDefault())
					.toInstant()));
			filter.setArrivalDateTo(arrivalDateTo.get() == null ? null : Date.from(arrivalDateTo.get()
					.atStartOfDay()
					.atZone(ZoneId.systemDefault())
					.toInstant()));
			filter.setDepartureDateFrom(departureDateFrom.get() == null ? null : Date.from(departureDateFrom.get()
					.atStartOfDay()
					.atZone(ZoneId.systemDefault())
					.toInstant()));
			filter.setDepartureDateTo(departureDateTo.get() == null ? null : Date.from(departureDateTo.get()
					.atStartOfDay()
					.atZone(ZoneId.systemDefault())
					.toInstant()));
			System.out.println(filter);
			refresh();
		} catch (Exception exc) {
			exc.printStackTrace();
		}
	}
}
