package com.gitlab.andrewkuryan.vt1.viewmodel.auth;

import com.gitlab.andrewkuryan.vt1.service.auth.RegisterModel;
import com.gitlab.andrewkuryan.vt1.service.auth.port.RegisterViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.FXViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.auth.port.RegisterViewState;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class FXRegisterViewModel implements RegisterViewModel, RegisterViewState {

	private FXViewModel appViewModel;
	private RegisterModel model;

	private StringProperty login;
	private StringProperty loginError;
	private StringProperty password;
	private StringProperty passwordError;
	private StringProperty repeatedPassword;
	private StringProperty email;
	private StringProperty firstName;
	private StringProperty lastName;
	private StringProperty phoneNumber;

	public FXRegisterViewModel(FXViewModel appViewModel, RegisterModel model) {
		this.appViewModel = appViewModel;
		this.model = model;

		login = new SimpleStringProperty("");
		loginError = new SimpleStringProperty("");
		password = new SimpleStringProperty("");
		passwordError = new SimpleStringProperty("");
		repeatedPassword = new SimpleStringProperty("");
		email = new SimpleStringProperty("");
		firstName = new SimpleStringProperty("");
		lastName = new SimpleStringProperty("");
		phoneNumber = new SimpleStringProperty("");
	}

	@Override
	public void setLogin(String login) {
		this.login.set(login);
	}

	@Override
	public void setLoginError(String error) {
		loginError.set(error);
	}

	@Override
	public void setPassword(String password) {
		this.password.set(password);
	}

	@Override
	public void setEmail(String email) {
		this.email.set(email);
	}

	@Override
	public void setFirstName(String firstName) {
		this.firstName.set(firstName);
	}

	@Override
	public void setLastName(String lastName) {
		this.lastName.set(lastName);
	}

	@Override
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber.set(phoneNumber);
	}


	@Override
	public String getLogin() {
		return login.get();
	}

	@Override
	public StringProperty loginProperty() {
		return login;
	}

	@Override
	public String getLoginError() {
		return loginError.get();
	}

	@Override
	public StringProperty loginErrorProperty() {
		return loginError;
	}

	@Override
	public String getPassword() {
		return password.get();
	}

	@Override
	public StringProperty passwordProperty() {
		return password;
	}

	@Override
	public String getPasswordError() {
		return passwordError.get();
	}

	@Override
	public StringProperty passwordErrorProperty() {
		return passwordError;
	}

	@Override
	public String getRepeatedPassword() {
		return repeatedPassword.get();
	}

	@Override
	public StringProperty repeatedPasswordProperty() {
		return repeatedPassword;
	}

	@Override
	public String getEmail() {
		return email.get();
	}

	@Override
	public StringProperty emailProperty() {
		return email;
	}

	@Override
	public String getFirstName() {
		return firstName.get();
	}

	@Override
	public StringProperty firstNameProperty() {
		return firstName;
	}

	@Override
	public String getLastName() {
		return lastName.get();
	}

	@Override
	public StringProperty lastNameProperty() {
		return lastName;
	}

	@Override
	public String getPhoneNumber() {
		return phoneNumber.get();
	}

	@Override
	public StringProperty phoneNumberProperty() {
		return phoneNumber;
	}

	@Override
	public void register() {
		// TODO: Validation
		if (password.get().equals(repeatedPassword.get())) {
			model.register(login.get(), password.get(),
					firstName.get(), lastName.get(), email.get(), phoneNumber.get());
		} else {
			repeatedPassword.set("");
			passwordError.set("Passwords do not match");
		}
	}
}
