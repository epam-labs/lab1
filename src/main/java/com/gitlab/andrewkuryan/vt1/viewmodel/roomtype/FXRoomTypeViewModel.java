package com.gitlab.andrewkuryan.vt1.viewmodel.roomtype;

import com.gitlab.andrewkuryan.vt1.entity.roomtype.RoomType;
import com.gitlab.andrewkuryan.vt1.service.roomtype.RoomTypeFilter;
import com.gitlab.andrewkuryan.vt1.service.roomtype.RoomTypeModel;
import com.gitlab.andrewkuryan.vt1.service.roomtype.port.RoomTypeViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.FXViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.roomtype.port.RoomTypeViewState;
import javafx.beans.property.*;
import javafx.collections.FXCollections;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class FXRoomTypeViewModel implements RoomTypeViewModel, RoomTypeViewState {

	private IntegerProperty page;
	private IntegerProperty totalPages;
	private IntegerProperty totalCount;
	private IntegerProperty recordsPerPage;
	private ObjectProperty<ArrayList<RoomType>> actualRoomTypes;
	private ObjectProperty<ArrayList<String>> availableServices;

	private StringProperty typeName;
	private StringProperty numOfPlacesFrom;
	private StringProperty numOfPlacesTo;
	private StringProperty costFrom;
	private StringProperty costTo;
	private StringProperty areaFrom;
	private StringProperty areaTo;
	private MapProperty<String, Boolean> services;

	private FXViewModel appViewModel;
	private RoomTypeModel model;
	private RoomTypeFilter filter;

	public FXRoomTypeViewModel(FXViewModel appViewModel, RoomTypeModel model) {
		this.appViewModel = appViewModel;
		this.model = model;
		filter = new RoomTypeFilter();

		page = new SimpleIntegerProperty(1);
		totalCount = new SimpleIntegerProperty(0);
		totalPages = new SimpleIntegerProperty(0);
		recordsPerPage = new SimpleIntegerProperty(2);
		actualRoomTypes = new SimpleObjectProperty<>(new ArrayList<>());
		availableServices = new SimpleObjectProperty<>(new ArrayList<>());

		typeName = new SimpleStringProperty("");
		numOfPlacesFrom = new SimpleStringProperty("");
		numOfPlacesTo = new SimpleStringProperty("");
		costFrom = new SimpleStringProperty("");
		costTo = new SimpleStringProperty("");
		areaFrom = new SimpleStringProperty("");
		areaTo = new SimpleStringProperty("");
		services = new SimpleMapProperty<>(FXCollections.observableHashMap());
	}

	/**
	 * Overrides from RoomTypeViewState
	 */

	@Override
	public int getPage() {
		return page.get();
	}

	@Override
	public IntegerProperty pageProperty() {
		return page;
	}

	@Override
	public int getTotalPages() {
		return totalPages.get();
	}

	@Override
	public IntegerProperty totalPagesProperty() {
		return totalPages;
	}

	@Override
	public IntegerProperty recordsPerPageProperty() {
		return recordsPerPage;
	}

	@Override
	public ObjectProperty<ArrayList<RoomType>> getActualRoomTypes() {
		return actualRoomTypes;
	}

	@Override
	public ObjectProperty<ArrayList<String>> getAvailableServices() {
		return availableServices;
	}

	@Override
	public void getRecordsOnPage(int page) {
		int actualTotalPages = (int) Math.ceil((double) totalCount.get() / recordsPerPage.get());
		actualTotalPages = actualTotalPages > 0 ? actualTotalPages : 1;
		if (page <= actualTotalPages) {
			model.getRoomTypesOnPage(page, true);
		} else {
			model.getRoomTypesOnPage(actualTotalPages, true);
		}
	}

	@Override
	public void refresh() {
		int actualTotalPages = (int) Math.ceil((double) totalCount.get() / recordsPerPage.get());
		actualTotalPages = actualTotalPages > 0 ? actualTotalPages : 1;
		if (page.get() <= actualTotalPages) {
			model.getRoomTypesOnPage(page.get(), false);
		} else {
			model.getRoomTypesOnPage(actualTotalPages, false);
		}
	}


	/**
	 * Overrides from RoomTypeViewModel
	 */

	@Override
	public void setPage(int page) {
		this.page.set(page);
	}

	public void setTotalCount(int totalCount) {
		this.totalCount.set(totalCount);
		totalPages.set((int) Math.ceil((double) totalCount / recordsPerPage.get()));
	}

	@Override
	public Optional<RoomTypeFilter> getFilter() {
		return Optional.of(filter);
	}

	@Override
	public void setAvailableServices(List<String> services) {
		availableServices.set(new ArrayList<>(services));
		services.forEach(service -> this.services.put(service, false));
	}

	@Override
	public int getRecordsPerPage() {
		return recordsPerPage.get();
	}

	@Override
	public void setActualRoomTypes(List<RoomType> roomTypes) {
		actualRoomTypes.set(new ArrayList<>(roomTypes));
	}


	/**
	 * Overrides from RoomTypeViewStateFilter
	 */

	@Override
	public String getTypeName() {
		return typeName.get();
	}

	@Override
	public StringProperty typeNameProperty() {
		return typeName;
	}

	@Override
	public String getNumOfPlacesFrom() {
		return numOfPlacesFrom.get();
	}

	@Override
	public StringProperty numOfPlacesFromProperty() {
		return numOfPlacesFrom;
	}

	@Override
	public String getNumOfPlacesTo() {
		return numOfPlacesTo.get();
	}

	@Override
	public StringProperty numOfPlacesToProperty() {
		return numOfPlacesTo;
	}

	@Override
	public String getCostFrom() {
		return costFrom.get();
	}

	@Override
	public StringProperty costFromProperty() {
		return costFrom;
	}

	@Override
	public String getCostTo() {
		return costTo.get();
	}

	@Override
	public StringProperty costToProperty() {
		return costTo;
	}

	@Override
	public String getAreaFrom() {
		return areaFrom.get();
	}

	@Override
	public StringProperty areaFromProperty() {
		return areaFrom;
	}

	@Override
	public String getAreaTo() {
		return areaTo.get();
	}

	@Override
	public StringProperty areaToProperty() {
		return areaTo;
	}

	@Override
	public Map<String, Boolean> getServices() {
		return services.get();
	}

	@Override
	public MapProperty<String, Boolean> servicesProperty() {
		return services;
	}

	@Override
	public void applyFilter() {
		try {
			filter.setTypeName(typeName.get().isEmpty() ? null : typeName.get());
			filter.setNumOfPlacesFrom(numOfPlacesFrom.get().isEmpty() ? null : Integer.parseInt(numOfPlacesFrom.get()));
			filter.setNumOfPlacesTo(numOfPlacesTo.get().isEmpty() ? null : Integer.parseInt(numOfPlacesTo.get()));
			filter.setCostFrom(costFrom.get().isEmpty() ? null : Integer.parseInt(costFrom.get()));
			filter.setCostTo(costTo.get().isEmpty() ? null : Integer.parseInt(costTo.get()));
			filter.setAreaFrom(areaFrom.get().isEmpty() ? null : Double.parseDouble(areaFrom.get()));
			filter.setAreaTo(areaTo.get().isEmpty() ? null : Double.parseDouble(areaTo.get()));
			filter.setServices(
					services.get().entrySet()
							.stream()
							.filter(Map.Entry::getValue)
							.map(Map.Entry::getKey)
							.collect(Collectors.toList())
			);
			System.out.println(filter);
		} catch (Exception ignored) {}
	}
}
