package com.gitlab.andrewkuryan.vt1.viewmodel.availableroomtype.port;

import com.gitlab.andrewkuryan.vt1.viewmodel.roomtype.port.RoomTypeViewStateFilter;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;

import java.time.LocalDate;

public interface AvailableRoomsViewStateFilter extends RoomTypeViewStateFilter {

	String getAvailablePlacesFrom();
	StringProperty availablePlacesFromProperty();

	String getAvailablePlacesTo();
	StringProperty availablePlacesToProperty();

	LocalDate getArrivalDate();
	ObjectProperty<LocalDate> arrivalDateProperty();

	LocalDate getDepartureDate();
	ObjectProperty<LocalDate> departureDateProperty();
}
