package com.gitlab.andrewkuryan.vt1.viewmodel.account.port;

public interface AdminViewState extends AccountViewState, AuthorizedViewState {

	void goToProfile();
}
