package com.gitlab.andrewkuryan.vt1.viewmodel.port;

import java.util.function.Consumer;

public interface AlertManager {

	void showConfirmAlert(String confirmText, Consumer<Void> onConfirm);

	void showTextInputAlert(String text, Consumer<String> onConfirm);
}
