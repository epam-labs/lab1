package com.gitlab.andrewkuryan.vt1.viewmodel.roomtype.port;

import javafx.beans.property.MapProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;

import java.util.ArrayList;
import java.util.Map;

public interface RoomTypeViewStateFilter {

	String getTypeName();
	StringProperty typeNameProperty();

	String getNumOfPlacesFrom();
	StringProperty numOfPlacesFromProperty();

	String getNumOfPlacesTo();
	StringProperty numOfPlacesToProperty();

	String getCostFrom();
	StringProperty costFromProperty();

	String getCostTo();
	StringProperty costToProperty();

	String getAreaFrom();
	StringProperty areaFromProperty();

	String getAreaTo();
	StringProperty areaToProperty();

	Map<String, Boolean> getServices();
	MapProperty<String, Boolean> servicesProperty();

	ObjectProperty<ArrayList<String>> getAvailableServices();

	void applyFilter();
}
