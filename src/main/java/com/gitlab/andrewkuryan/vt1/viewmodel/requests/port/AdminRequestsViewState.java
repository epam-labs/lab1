package com.gitlab.andrewkuryan.vt1.viewmodel.requests.port;

import com.gitlab.andrewkuryan.vt1.entity.request.Request;

public interface AdminRequestsViewState extends RequestsViewState {

	void confirmRequest(Request request);

	void rejectRequest(Request request);
}
