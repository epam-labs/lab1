package com.gitlab.andrewkuryan.vt1.viewmodel.account.port;

import javafx.beans.property.StringProperty;

public interface AccountViewState {

	String getLogin();
	StringProperty loginProperty();
}
