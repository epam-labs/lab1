package com.gitlab.andrewkuryan.vt1.viewmodel.requests;

import com.gitlab.andrewkuryan.vt1.entity.request.Request;
import com.gitlab.andrewkuryan.vt1.service.request.AdminRequestsModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.FXViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.requests.port.AdminRequestsViewState;

public class FXAdminRequestsViewModel extends FXRequestsViewModel implements AdminRequestsViewState {

	private AdminRequestsModel model;

	public FXAdminRequestsViewModel(FXViewModel appViewModel, AdminRequestsModel model) {
		super(appViewModel, model);
		this.model = model;
	}

	@Override
	public void confirmRequest(Request request) {
		appViewModel.getAlertManager()
				.showTextInputAlert("Enter the room number:", s ->
						model.confirmRequest(request, Integer.parseInt(s))
				);
	}

	@Override
	public void rejectRequest(Request request) {
		appViewModel.getAlertManager()
				.showTextInputAlert("Enter the reason for reject:", s ->
						model.rejectRequest(request, s)
				);
	}
}
