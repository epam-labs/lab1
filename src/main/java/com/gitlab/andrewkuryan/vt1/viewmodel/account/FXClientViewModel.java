package com.gitlab.andrewkuryan.vt1.viewmodel.account;

import com.gitlab.andrewkuryan.vt1.entity.user.Client;
import com.gitlab.andrewkuryan.vt1.entity.user.User;
import com.gitlab.andrewkuryan.vt1.service.account.AccountModel;
import com.gitlab.andrewkuryan.vt1.service.account.port.AccountViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.FXViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.account.port.ClientViewState;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.Optional;

public class FXClientViewModel implements AccountViewModel, ClientViewState {

	private FXViewModel appViewModel;
	private AccountModel model;

	private StringProperty login;
	private StringProperty oldPassword;
	private StringProperty password;
	private StringProperty firstName;
	private StringProperty lastName;
	private StringProperty email;
	private StringProperty phoneNumber;

	private Client client;
	private User actualUser;

	public FXClientViewModel(FXViewModel appViewModel, AccountModel model) {
		this.appViewModel = appViewModel;
		this.model = model;

		login = new SimpleStringProperty("");
		oldPassword = new SimpleStringProperty("");
		password = new SimpleStringProperty("");
		firstName = new SimpleStringProperty("");
		lastName = new SimpleStringProperty("");
		email = new SimpleStringProperty("");
		phoneNumber = new SimpleStringProperty("");
	}

	@Override
	public Optional<User> getAccount() {
		return Optional.ofNullable(actualUser);
	}

	@Override
	public void setAccount(User account) {
		actualUser = account;
		if (account instanceof Client) {
			System.out.println("Client: " + account);
			client = (Client) account;
			login.set(client.getLogin());
			firstName.set(client.getFirstName());
			lastName.set(client.getLastName());
			email.set(client.getEmail());
			phoneNumber.set(client.getPhoneNumber());

			appViewModel.getConfigurationManager().configure("client");
			appViewModel.getNavigationManager().navigateToUpperScene();
		}
	}

	@Override
	public String getLogin() {
		return login.get();
	}

	@Override
	public StringProperty loginProperty() {
		return login;
	}

	@Override
	public String getOldPassword() {
		return oldPassword.get();
	}

	@Override
	public StringProperty oldPasswordProperty() {
		return oldPassword;
	}

	@Override
	public String getPassword() {
		return password.get();
	}

	@Override
	public StringProperty passwordProperty() {
		return password;
	}

	@Override
	public String getFirstName() {
		return firstName.get();
	}

	@Override
	public StringProperty firstNameProperty() {
		return firstName;
	}

	@Override
	public String getLastName() {
		return lastName.get();
	}

	@Override
	public StringProperty lastNameProperty() {
		return lastName;
	}

	@Override
	public String getEmail() {
		return email.get();
	}

	@Override
	public StringProperty emailProperty() {
		return email;
	}

	@Override
	public String getPhoneNumber() {
		return phoneNumber.get();
	}

	@Override
	public StringProperty phoneNumberProperty() {
		return phoneNumber;
	}

	@Override
	public void update() {
		// TODO: Validate
		model.update(oldPassword.get(), password.get(), firstName.get(), lastName.get(),
				email.get(), phoneNumber.get());
	}

	@Override
	public void logout() {
		model.setAccount(User.GUEST);
	}

	@Override
	public void showProfile() {
		// TODO: Navigate to profile
	}

	@Override
	public void goBack() {
		appViewModel.getNavigationManager().navigateToUpperScene();
	}

	@Override
	public void goToProfile() {
		appViewModel.getNavigationManager().navigateTo("clientprofile");
	}
}
