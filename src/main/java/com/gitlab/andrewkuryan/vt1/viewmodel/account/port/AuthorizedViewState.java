package com.gitlab.andrewkuryan.vt1.viewmodel.account.port;

import javafx.beans.property.StringProperty;

public interface AuthorizedViewState extends AccountViewState {

	String getOldPassword();
	StringProperty oldPasswordProperty();

	String getPassword();
	StringProperty passwordProperty();

	String getFirstName();
	StringProperty firstNameProperty();

	String getLastName();
	StringProperty lastNameProperty();

	void update();
	void logout();
	void showProfile();
	void goBack();
}
