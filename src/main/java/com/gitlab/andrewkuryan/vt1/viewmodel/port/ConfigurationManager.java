package com.gitlab.andrewkuryan.vt1.viewmodel.port;

public interface ConfigurationManager {

	void addConfigurableView(ConfigurableView view);

	void configure(String newConfig);
}
