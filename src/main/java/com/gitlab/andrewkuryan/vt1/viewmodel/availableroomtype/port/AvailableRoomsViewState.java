package com.gitlab.andrewkuryan.vt1.viewmodel.availableroomtype.port;

import com.gitlab.andrewkuryan.vt1.entity.roomtype.AvailableRoomTypes;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;

import java.util.ArrayList;

public interface AvailableRoomsViewState extends AvailableRoomsViewStateFilter {

	int getPage();
	IntegerProperty pageProperty();

	int getTotalPages();
	IntegerProperty totalPagesProperty();

	int getRecordsPerPage();
	IntegerProperty recordsPerPageProperty();

	ObjectProperty<ArrayList<AvailableRoomTypes>> getActualRoomTypes();
	ObjectProperty<ArrayList<String>> getAvailableServices();

	ObjectProperty<AvailableRoomTypes> selectedRoomTypeProperty();

	void createRequest();

	void getRecordsOnPage(int page);
	void refresh();
}
