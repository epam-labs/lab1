package com.gitlab.andrewkuryan.vt1.viewmodel.auth.port;

import javafx.beans.property.StringProperty;

public interface RegisterViewState {

	String getLogin();
	StringProperty loginProperty();

	String getLoginError();
	StringProperty loginErrorProperty();

	String getPassword();
	StringProperty passwordProperty();

	String getPasswordError();
	StringProperty passwordErrorProperty();

	String getRepeatedPassword();
	StringProperty repeatedPasswordProperty();

	String getEmail();
	StringProperty emailProperty();

	String getFirstName();
	StringProperty firstNameProperty();

	String getLastName();
	StringProperty lastNameProperty();

	String getPhoneNumber();
	StringProperty phoneNumberProperty();

	void register();
}
