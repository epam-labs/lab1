package com.gitlab.andrewkuryan.vt1.viewmodel.requests.port;

import com.gitlab.andrewkuryan.vt1.entity.request.Request;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;

import java.time.LocalDate;

public interface ClientRequestsViewState extends RequestsViewState {

	String getSelectedRoomTypeName();

	LocalDate getArrivalDate();
	ObjectProperty<LocalDate> arrivalDateProperty();

	LocalDate getDepartureDate();
	ObjectProperty<LocalDate> departureDateProperty();

	int getNumOfPersons();
	IntegerProperty numOfPersonsProperty();

	void cancelRequest(Request request);

	void createRequest();
	void goBack();
	void checkPermissions();
}
