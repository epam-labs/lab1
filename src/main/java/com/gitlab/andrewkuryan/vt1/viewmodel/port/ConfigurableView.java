package com.gitlab.andrewkuryan.vt1.viewmodel.port;

public interface ConfigurableView {

	void configure(String configName);
}
