package com.gitlab.andrewkuryan.vt1.viewmodel.port;

import com.gitlab.andrewkuryan.vt1.view.utils.View;

public interface NavigationManager {

	void navigateTo(String path);

	void navigateWithReplace(String path);

	void navigateToUpperScene();

	void addRoute(String path, View view);

	void addLateinitRoute(String path, LateinitView<? extends View> view);
}
