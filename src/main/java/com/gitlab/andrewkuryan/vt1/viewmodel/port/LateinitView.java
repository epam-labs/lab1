package com.gitlab.andrewkuryan.vt1.viewmodel.port;

public interface LateinitView<T> {

	T init();
}
