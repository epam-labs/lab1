package com.gitlab.andrewkuryan.vt1.viewmodel.account.port;

public interface GuestViewState extends AccountViewState {

	void signIn();
}
