package com.gitlab.andrewkuryan.vt1.viewmodel.main;

import com.gitlab.andrewkuryan.vt1.viewmodel.FXViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.main.port.LeftNavigationViewState;

public class FXLeftNavigationViewModel implements LeftNavigationViewState {

	private FXViewModel appViewModel;

	public FXLeftNavigationViewModel(FXViewModel appViewModel) {
		this.appViewModel = appViewModel;
	}

	@Override
	public void navigateTo(String path) {
		appViewModel.getNavigationManager().navigateWithReplace(path);
	}
}
