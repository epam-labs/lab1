package com.gitlab.andrewkuryan.vt1.viewmodel.availableroomtype;

import com.gitlab.andrewkuryan.vt1.entity.roomtype.AvailableRoomTypes;
import com.gitlab.andrewkuryan.vt1.service.roomtype.AvailableRoomTypesFilter;
import com.gitlab.andrewkuryan.vt1.service.roomtype.AvailableRoomTypesModel;
import com.gitlab.andrewkuryan.vt1.service.roomtype.port.AvailableRoomsViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.FXViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.availableroomtype.port.AvailableRoomsViewState;
import javafx.beans.property.*;
import javafx.collections.FXCollections;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

public class FXAvailableRoomsViewModel implements AvailableRoomsViewModel, AvailableRoomsViewState {

	private IntegerProperty page;
	private IntegerProperty totalPages;
	private IntegerProperty totalCount;
	private IntegerProperty recordsPerPage;
	private ObjectProperty<ArrayList<AvailableRoomTypes>> actualAvailableRoomTypes;
	private ObjectProperty<ArrayList<String>> availableServices;
	private ObjectProperty<AvailableRoomTypes> selectedRoomType;

	private StringProperty typeName;
	private StringProperty numOfPlacesFrom;
	private StringProperty numOfPlacesTo;
	private StringProperty costFrom;
	private StringProperty costTo;
	private StringProperty areaFrom;
	private StringProperty areaTo;
	private MapProperty<String, Boolean> services;
	private StringProperty availablePlacesFrom;
	private StringProperty availablePlacesTo;
	private ObjectProperty<LocalDate> arrivalDate;
	private ObjectProperty<LocalDate> departureDate;

	private FXViewModel appViewModel;
	private AvailableRoomTypesModel model;
	private AvailableRoomTypesFilter filter;

	public FXAvailableRoomsViewModel(FXViewModel appViewModel, AvailableRoomTypesModel model) {
		this.appViewModel = appViewModel;
		this.model = model;
		filter = new AvailableRoomTypesFilter();

		page = new SimpleIntegerProperty(1);
		totalCount = new SimpleIntegerProperty(0);
		totalPages = new SimpleIntegerProperty(0);
		recordsPerPage = new SimpleIntegerProperty(2);
		actualAvailableRoomTypes = new SimpleObjectProperty<>(new ArrayList<>());
		availableServices = new SimpleObjectProperty<>(new ArrayList<>());
		selectedRoomType = new SimpleObjectProperty<>(null);

		typeName = new SimpleStringProperty("");
		numOfPlacesFrom = new SimpleStringProperty("");
		numOfPlacesTo = new SimpleStringProperty("");
		costFrom = new SimpleStringProperty("");
		costTo = new SimpleStringProperty("");
		areaFrom = new SimpleStringProperty("");
		areaTo = new SimpleStringProperty("");
		services = new SimpleMapProperty<>(FXCollections.observableHashMap());
		availablePlacesFrom = new SimpleStringProperty("");
		availablePlacesTo = new SimpleStringProperty("");
		arrivalDate = new SimpleObjectProperty<>(null);
		departureDate = new SimpleObjectProperty<>(null);
	}

	/**
	 * Overrides from AvailableRoomsViewState
	 */

	@Override
	public int getPage() {
		return page.get();
	}

	@Override
	public IntegerProperty pageProperty() {
		return page;
	}

	@Override
	public int getTotalPages() {
		return totalPages.get();
	}

	@Override
	public IntegerProperty totalPagesProperty() {
		return totalPages;
	}

	@Override
	public IntegerProperty recordsPerPageProperty() {
		return recordsPerPage;
	}

	@Override
	public ObjectProperty<ArrayList<AvailableRoomTypes>> getActualRoomTypes() {
		return actualAvailableRoomTypes;
	}

	@Override
	public ObjectProperty<ArrayList<String>> getAvailableServices() {
		return availableServices;
	}

	@Override
	public ObjectProperty<AvailableRoomTypes> selectedRoomTypeProperty() {
		return selectedRoomType;
	}

	@Override
	public void getRecordsOnPage(int page) {
		int actualTotalPages = (int) Math.ceil((double) totalCount.get() / recordsPerPage.get());
		actualTotalPages = actualTotalPages > 0 ? actualTotalPages : 1;
		if (page <= actualTotalPages) {
			model.getRoomTypesOnPage(page, true);
		} else {
			model.getRoomTypesOnPage(actualTotalPages, true);
		}
	}

	@Override
	public void refresh() {
		int actualTotalPages = (int) Math.ceil((double) totalCount.get() / recordsPerPage.get());
		actualTotalPages = actualTotalPages > 0 ? actualTotalPages : 1;
		if (page.get() <= actualTotalPages) {
			model.getRoomTypesOnPage(page.get(), false);
		} else {
			model.getRoomTypesOnPage(actualTotalPages, false);
		}
	}

	@Override
	public void createRequest() {
		appViewModel.getNavigationManager().navigateTo("createrequest");
	}

	/**
	 * Overrides from AvailableRoomsViewModel
	 */

	@Override
	public void setPage(int page) {
		this.page.set(page);
	}

	public void setTotalCount(int totalCount) {
		this.totalCount.set(totalCount);
		totalPages.set((int) Math.ceil((double) totalCount / recordsPerPage.get()));
	}

	@Override
	public Optional<AvailableRoomTypesFilter> getFilter() {
		return Optional.of(filter);
	}

	@Override
	public void setAvailableServices(List<String> services) {
		availableServices.set(new ArrayList<>(services));
		services.forEach(service -> this.services.put(service, false));
	}

	@Override
	public int getRecordsPerPage() {
		return recordsPerPage.get();
	}

	@Override
	public AvailableRoomTypes getSelectedRoomType() {
		return selectedRoomType.get();
	}

	@Override
	public void setActualAvailableRoomTypes(List<AvailableRoomTypes> roomTypes) {
		actualAvailableRoomTypes.set(new ArrayList<>(roomTypes));
	}


	/**
	 * Overrides from AvailableRoomsViewStateFilter
	 */

	@Override
	public String getAvailablePlacesFrom() {
		return availablePlacesFrom.get();
	}

	@Override
	public StringProperty availablePlacesFromProperty() {
		return availablePlacesFrom;
	}

	@Override
	public String getAvailablePlacesTo() {
		return availablePlacesTo.get();
	}

	@Override
	public StringProperty availablePlacesToProperty() {
		return availablePlacesTo;
	}

	@Override
	public LocalDate getArrivalDate() {
		return arrivalDate.get();
	}

	@Override
	public ObjectProperty<LocalDate> arrivalDateProperty() {
		return arrivalDate;
	}

	@Override
	public LocalDate getDepartureDate() {
		return departureDate.get();
	}

	@Override
	public ObjectProperty<LocalDate> departureDateProperty() {
		return departureDate;
	}

	@Override
	public String getTypeName() {
		return typeName.get();
	}

	@Override
	public StringProperty typeNameProperty() {
		return typeName;
	}

	@Override
	public String getNumOfPlacesFrom() {
		return numOfPlacesFrom.get();
	}

	@Override
	public StringProperty numOfPlacesFromProperty() {
		return numOfPlacesFrom;
	}

	@Override
	public String getNumOfPlacesTo() {
		return numOfPlacesTo.get();
	}

	@Override
	public StringProperty numOfPlacesToProperty() {
		return numOfPlacesTo;
	}

	@Override
	public String getCostFrom() {
		return costFrom.get();
	}

	@Override
	public StringProperty costFromProperty() {
		return costFrom;
	}

	@Override
	public String getCostTo() {
		return costTo.get();
	}

	@Override
	public StringProperty costToProperty() {
		return costTo;
	}

	@Override
	public String getAreaFrom() {
		return areaFrom.get();
	}

	@Override
	public StringProperty areaFromProperty() {
		return areaFrom;
	}

	@Override
	public String getAreaTo() {
		return areaTo.get();
	}

	@Override
	public StringProperty areaToProperty() {
		return areaTo;
	}

	@Override
	public Map<String, Boolean> getServices() {
		return services.get();
	}

	@Override
	public MapProperty<String, Boolean> servicesProperty() {
		return services;
	}

	@Override
	public void applyFilter() {
		try {
			filter.setTypeName(typeName.get().isEmpty() ? null : typeName.get());
			filter.setNumOfPlacesFrom(numOfPlacesFrom.get().isEmpty() ? null : Integer.parseInt(numOfPlacesFrom.get()));
			filter.setNumOfPlacesTo(numOfPlacesTo.get().isEmpty() ? null : Integer.parseInt(numOfPlacesTo.get()));
			filter.setCostFrom(costFrom.get().isEmpty() ? null : Integer.parseInt(costFrom.get()));
			filter.setCostTo(costTo.get().isEmpty() ? null : Integer.parseInt(costTo.get()));
			filter.setAreaFrom(areaFrom.get().isEmpty() ? null : Double.parseDouble(areaFrom.get()));
			filter.setAreaTo(areaTo.get().isEmpty() ? null : Double.parseDouble(areaTo.get()));
			filter.setServices(
					services.get().entrySet()
							.stream()
							.filter(Map.Entry::getValue)
							.map(Map.Entry::getKey)
							.collect(Collectors.toList())
			);
			filter.setAvailablePlacesFrom(availablePlacesFrom.get().isEmpty() ? null : Integer.parseInt(availablePlacesFrom.get()));
			filter.setAvailablePlacesTo(availablePlacesTo.get().isEmpty() ? null : Integer.parseInt(availablePlacesTo.get()));
			filter.setArrivalDate(arrivalDate.get() == null ? null : Date.from(arrivalDate.get()
					.atStartOfDay()
					.atZone(ZoneId.systemDefault())
					.toInstant()));
			filter.setDepartureDate(departureDate.get() == null ? null : Date.from(departureDate.get()
					.atStartOfDay()
					.atZone(ZoneId.systemDefault())
					.toInstant()));
			System.out.println(filter);
		} catch (Exception ignored) {}
	}
}
