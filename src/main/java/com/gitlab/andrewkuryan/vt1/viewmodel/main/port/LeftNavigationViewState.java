package com.gitlab.andrewkuryan.vt1.viewmodel.main.port;

public interface LeftNavigationViewState {

	void navigateTo(String path);
}
