package com.gitlab.andrewkuryan.vt1.viewmodel.account.port;

import javafx.beans.property.StringProperty;

public interface ClientViewState extends AccountViewState, AuthorizedViewState {

	String getEmail();
	StringProperty emailProperty();

	String getPhoneNumber();
	StringProperty phoneNumberProperty();

	void goToProfile();
}
