package com.gitlab.andrewkuryan.vt1.viewmodel.requests.port;

import com.gitlab.andrewkuryan.vt1.entity.request.Request;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;

import java.util.ArrayList;

public interface RequestsViewState extends RequestsViewStateFilter {

	int getPage();
	IntegerProperty pageProperty();

	int getTotalPages();
	IntegerProperty totalPagesProperty();

	int getRecordsPerPage();
	IntegerProperty recordsPerPageProperty();

	ObjectProperty<ArrayList<Request>> getActualRequests();

	void getRecordsOnPage(int page);
	void refresh();
}
