package com.gitlab.andrewkuryan.vt1.viewmodel.auth.port;

import javafx.beans.property.StringProperty;

public interface LoginViewState {

	String getLogin();
	StringProperty loginProperty();

	String getPassword();
	StringProperty passwordProperty();

	String getPasswordError();
	StringProperty passwordErrorProperty();

	String getLoginError();
	StringProperty loginErrorProperty();

	void login();
}
