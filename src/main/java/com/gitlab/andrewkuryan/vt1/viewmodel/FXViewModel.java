package com.gitlab.andrewkuryan.vt1.viewmodel;

import com.gitlab.andrewkuryan.vt1.viewmodel.port.AlertManager;
import com.gitlab.andrewkuryan.vt1.viewmodel.port.ConfigurationManager;
import com.gitlab.andrewkuryan.vt1.viewmodel.port.NavigationManager;

public interface FXViewModel {

	NavigationManager getNavigationManager();

	AlertManager getAlertManager();

	ConfigurationManager getConfigurationManager();
}
