package com.gitlab.andrewkuryan.vt1.viewmodel.auth;

import com.gitlab.andrewkuryan.vt1.service.auth.LoginModel;
import com.gitlab.andrewkuryan.vt1.service.auth.port.LoginViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.FXViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.auth.port.LoginViewState;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class FXLoginViewModel implements LoginViewModel, LoginViewState {

	private FXViewModel appViewModel;
	private LoginModel model;

	private StringProperty login;
	private StringProperty password;
	private StringProperty loginError;
	private StringProperty passwordError;

	public FXLoginViewModel(FXViewModel appViewModel, LoginModel model) {
		this.appViewModel = appViewModel;
		this.model = model;

		login = new SimpleStringProperty("");
		password = new SimpleStringProperty("");
		loginError = new SimpleStringProperty("");
		passwordError = new SimpleStringProperty("");
	}

	@Override
	public void setLogin(String login) {
		this.login.set(login);
	}

	@Override
	public void setPassword(String password) {
		this.password.set(password);
	}

	@Override
	public void setLoginError(String error) {
		loginError.set(error);
	}

	@Override
	public void setPasswordError(String error) {
		passwordError.set(error);
	}


	@Override
	public String getLogin() {
		return login.get();
	}

	@Override
	public StringProperty loginProperty() {
		return login;
	}

	@Override
	public String getPassword() {
		return password.get();
	}

	@Override
	public StringProperty passwordProperty() {
		return password;
	}

	@Override
	public String getPasswordError() {
		return passwordError.get();
	}

	@Override
	public StringProperty passwordErrorProperty() {
		return passwordError;
	}

	@Override
	public String getLoginError() {
		return loginError.get();
	}

	@Override
	public StringProperty loginErrorProperty() {
		return loginError;
	}

	@Override
	public void login() {
		// TODO: Validation
		model.login(login.get(), password.get());
	}
}
