package com.gitlab.andrewkuryan.vt1.viewmodel.account;

import com.gitlab.andrewkuryan.vt1.entity.user.User;
import com.gitlab.andrewkuryan.vt1.service.account.port.AccountViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.FXViewModel;
import com.gitlab.andrewkuryan.vt1.viewmodel.account.port.GuestViewState;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.Optional;

public class FXGuestViewModel implements AccountViewModel, GuestViewState {

	private FXViewModel appViewModel;
	private StringProperty login;

	private User guest;
	private User actualUser;

	public FXGuestViewModel(FXViewModel appViewModel) {
		this.appViewModel = appViewModel;
		login = new SimpleStringProperty("");
	}

	@Override
	public void setAccount(User account) {
		actualUser = account;
		if (account.getLogin().equals("Guest")) {
			System.out.println("Guest: " + guest);
			guest = account;
			login.set(guest.getLogin());

			appViewModel.getConfigurationManager().configure("guest");
			appViewModel.getNavigationManager().navigateWithReplace("main");
		}
	}

	@Override
	public Optional<User> getAccount() {
		return Optional.ofNullable(actualUser);
	}

	@Override
	public String getLogin() {
		return login.get();
	}

	@Override
	public StringProperty loginProperty() {
		return login;
	}

	@Override
	public void signIn() {
		appViewModel.getNavigationManager().navigateTo("auth");
	}
}
