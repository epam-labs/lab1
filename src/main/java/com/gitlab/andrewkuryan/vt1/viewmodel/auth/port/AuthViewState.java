package com.gitlab.andrewkuryan.vt1.viewmodel.auth.port;

import javafx.beans.property.BooleanProperty;

public interface AuthViewState {

	Boolean getSignInSelected();
	BooleanProperty signInSelectedProperty();

	Boolean getRegisterSelected();
	BooleanProperty registerSelectedProperty();
}
