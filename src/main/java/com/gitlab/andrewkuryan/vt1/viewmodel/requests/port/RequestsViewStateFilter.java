package com.gitlab.andrewkuryan.vt1.viewmodel.requests.port;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.StringProperty;

import java.time.LocalDate;

public interface RequestsViewStateFilter {

	boolean getConfirmed();
	BooleanProperty confirmedProperty();

	boolean getRejected();
	BooleanProperty rejectedProperty();

	String getRoomTypeName();
	StringProperty roomTypeNameProperty();

	String getCustomerLogin();
	StringProperty customerLoginProperty();

	String getNumOfPersonsFrom();
	StringProperty numOfPersonsFromProperty();

	String getNumOfPersonsTo();
	StringProperty numOfPersonsToProperty();

	LocalDate getArrivalDateFrom();
	ObjectProperty<LocalDate> arrivalDateFromProperty();

	LocalDate getArrivalDateTo();
	ObjectProperty<LocalDate> arrivalDateToProperty();

	LocalDate getDepartureDateFrom();
	ObjectProperty<LocalDate> departureDateFromProperty();

	LocalDate getDepartureDateTo();
	ObjectProperty<LocalDate> departureDateToProperty();

	void applyFilter();
}
