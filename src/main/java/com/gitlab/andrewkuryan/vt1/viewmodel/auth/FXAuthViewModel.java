package com.gitlab.andrewkuryan.vt1.viewmodel.auth;

import com.gitlab.andrewkuryan.vt1.viewmodel.auth.port.AuthViewState;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;

public class FXAuthViewModel implements AuthViewState {

	private BooleanProperty isSignInSelected;
	private BooleanProperty isRegisterSelected;

	public FXAuthViewModel() {
		isSignInSelected = new SimpleBooleanProperty(true);
		isRegisterSelected = new SimpleBooleanProperty(false);
	}

	@Override
	public Boolean getSignInSelected() {
		return isSignInSelected.get();
	}

	@Override
	public BooleanProperty signInSelectedProperty() {
		return isSignInSelected;
	}

	@Override
	public Boolean getRegisterSelected() {
		return isRegisterSelected.get();
	}

	@Override
	public BooleanProperty registerSelectedProperty() {
		return isRegisterSelected;
	}
}
